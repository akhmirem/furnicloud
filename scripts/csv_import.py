import sys
import csv

from django.contrib.auth.models import User
from django.db.models import Q
from django.utils import timezone

from commissions.models import Commission
from customers.models import Customer
from orders.models import Order
from datetime import datetime

from stores.models import Store


def find_existing_customer(first_name, last_name, email=None):
    if not first_name or not last_name:
        return None
    query_filter = Q(first_name__iexact=first_name) & \
                   Q(last_name__iexact=last_name)
    if email:
        query_filter &= Q(email__iexact=email)

    matching_customer = Customer.objects.filter(query_filter).first()
    return matching_customer

# to execute:
# python manage.py shell
# import sys
# sys.path.append('/path/to/furnicloud')
# from scripts import csv_import
# csv_import.bulk_import_orders('filename')
def bulk_import_orders(filename):
    print("Starting the import...")
    # columns in a CSV file
    # Order #,Order Type,Order Date,Phone,First Name,Last Name,Ship To City,Ship To State
    with open(filename, 'r') as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            try:
                print("Row: [{}]".format(row))
                customer = find_existing_customer(row['First Name'], row['Last Name'])
                if not customer:
                    customer = Customer.objects.create(first_name=row['First Name'], last_name=row['Last Name'],
                                                       phone=row['Phone'], ship_addr_city=row['Ship To City'],
                                                       ship_addr_state=row['Ship To State'])
                    customer.save()
                    print("Customer created: {}".format(customer))
                else:
                    print("Found customer: [{}]".format(customer))

                order_date = datetime.strptime(row['Order Date'], "%Y-%m-%d")
                order_date = timezone.make_aware(order_date, timezone.get_current_timezone())
                comments = "Imported from POS"
                sac_store = Store.objects.get(name='Sacramento')
                order = Order.objects.create(customer=customer, order_type=row['Order Type'], status='I',
                                             number='SO-%s' % row['Order #'],
                                             order_date=order_date, comments=comments, store=sac_store)
                order.save()
                print("Order created: [{}]".format(order))

                admin = User.objects.get(pk=1)
                commission = Commission.objects.create(associate=admin, order=order, status='INELIGIBLE', portion_percentage=100.0)
                commission.save()
                print("Commission created: [{}]".format(commission))
                print("-----------------------------------")
            except Exception as e:
                print("***************************************")
                print("****** Error occurred: {}".format(e))
    print("Finished the import.")


# if __name__ == '__main__':
#     filename = sys.argv[0]
#     print("Import file: {}".format(filename))
#     if not filename:
#         raise Exception("CSV Orders file not provided!")
#     bulk_import_orders(filename)
