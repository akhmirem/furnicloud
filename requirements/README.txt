MySQL client installation on MacOS:
=======================================
brew install mysql-client
If you need to have mysql-client first in your PATH, run:
  echo 'export PATH="/opt/homebrew/opt/mysql-client/bin:$PATH"' >> ~/.zshrc

For compilers to find mysql-client you may need to set:
  export LDFLAGS="-L/opt/homebrew/opt/mysql-client/lib"
  export CPPFLAGS="-I/opt/homebrew/opt/mysql-client/include"

It might be required to install openssl (unless it is already available):
brew install openssl
  If you need to have openssl@3 first in your PATH, run:
  echo 'export PATH="/opt/homebrew/opt/openssl@3/bin:$PATH"' >> ~/.zshrc

For compilers to find openssl@3 you may need to set:
  export LDFLAGS="-L/opt/homebrew/opt/openssl@3/lib"
  export CPPFLAGS="-I/opt/homebrew/opt/openssl@3/include"

!!!Prior to installing mysqlclient via pip, do the following:
export LDFLAGS="-L/opt/homebrew/opt/mysql-client/lib -L/opt/homebrew/opt/openssl@3/lib -L/opt/homebrew/opt/zstd/lib"
export CPPFLAGS="-I/opt/homebrew/opt/mysql-client/include -I/opt/homebrew/opt/openssl@3/include -I/opt/homebrew/opt/zstd/include"
then execute
pip install mysqlclient



MySQL client installation on Ubuntu:
=================
run first -
apt-get install python3-dev libmysqlclient-dev


MySQL on Windows:
=================
pip install mysql-connector-python

OTHERS
===============
audit_log
pip install git+https://github.com/teni/django-audit-log

querystring-parser
pip install git+https://github.com/bernii/querystring-parser.git
