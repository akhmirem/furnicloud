# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('claims', '0003_auto_20151119_1100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='claim',
            name='item_origin',
            field=models.CharField(max_length=128, choices=[(b'NTZ', b'Natuzzi Italia'), (b'EDITIONS', b'Natuzzi Editions'), (b'REVIVE', b'Natuzzi Re-Vive'), (b'SOFTALY', b'Softaly')]),
        ),
        migrations.AlterField(
            model_name='vendorclaimrequest',
            name='file',
            field=models.FileField(upload_to=b'claims/%Y/%m', blank=True),
        ),
    ]
