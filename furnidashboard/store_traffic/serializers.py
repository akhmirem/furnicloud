from commissions.models import Commission
from rest_framework import serializers
from django.conf import settings
from store_traffic.models import StoreTrafficEntry
from stores.models import Store


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = 'name'


class StoreTrafficSerializer(serializers.ModelSerializer):
    associate_name = serializers.CharField(source='associate.first_name', required=False)

    class Meta:
        model = StoreTrafficEntry
        fields = ('pk', 'timestamp', 'store', 'count', 'visitor_type', 'referred_by', 'visit_duration',
                  'first_name', 'last_name', 'email', 'comments', 'associate', 'associate_name')
