from django.conf import settings
from django.contrib import auth
from django.db import models
from stores.models import Store
from orders.models import Order

VISITOR_TYPE_CHOICES = (
    ('walkin', 'General Walk-in'),
    ('customer', 'Returning Customer'),
    ('lead', 'In-progress Lead'),
    ('other', 'Other (see comments)'),
)


class StoreTrafficEntry(models.Model):
    timestamp = models.DateTimeField(null=False, blank=False)

    store = models.ForeignKey(Store, default=None, null=True, blank=True,
                              related_name='store_traffic', on_delete=models.SET_NULL)
    count = models.IntegerField(blank=True, default=1)
    visitor_type = models.CharField(blank=True, null=True, max_length=25, choices=VISITOR_TYPE_CHOICES)
    referred_by = models.CharField(blank=True, null=True, max_length=50, choices=Order.REFERRAL_SOURCES)
    visit_duration = models.CharField(blank=True, null=True, max_length=50)
    first_name = models.CharField(blank=True, null=True, max_length=50)
    last_name = models.CharField(blank=True, null=True, max_length=50)
    email = models.EmailField(blank=True, null=True, max_length=50)
    comments = models.TextField(blank=True, null=True, max_length=500)
    associate = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, blank=True,
                                  related_name='store_traffic_entries', on_delete=models.SET_NULL)

    class Meta:
        db_table = "store_traffic"
