from datetime import datetime
from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase, Client
from django.utils import timezone
from rest_framework.test import APIRequestFactory
from rest_framework import status
import json
import random

from .views import StoreTrafficSummaryView
from .models import StoreTrafficEntry
from stores.models import Store

class SimpleTest(TestCase):
    DATE_FORMAT = "%Y-%m-%d"

    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = APIRequestFactory()
        self.client = Client()
        self.user = User.objects.create_user(
            username='test', email='test@localhost', password='test', first_name="test")
        self.sac_store = Store.objects.create(name="Sacramento")
        self.fnt_store = Store.objects.create(name="Roseville")
        self.current_time = timezone.localtime()
        self.valid_traffic_entry = {
            'timestamp': self.current_time.isoformat(),
            'store': self.sac_store.id,
            'count': 3,
            'visitor_type': 'customer',
            'referred_by': 'GOOG',
            'visit_duration': '1:15',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'j.doe@gmail.com',
            'comments': 'In&Out',
            'associate': self.user.id
        }

    def test_get_traffic_summary_no_content(self):
        from_date = timezone.localtime()
        to_date = timezone.localtime() + timezone.timedelta(days=1)

        data = {'from': from_date.strftime(self.DATE_FORMAT), 'to': to_date.strftime(self.DATE_FORMAT)}
        request = self.factory.get('/api/traffic-summary/', data)

        request.user = self.user

        # Or you can simulate an anonymous user by setting request.user to
        # an AnonymousUser instance.
        # request.user = AnonymousUser()

        response = StoreTrafficSummaryView.as_view()(request)
        response.render() 

        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode("utf-8"))
        self.assertEqual(json_response['data']['sac'], [])
        self.assertEqual(json_response['data']['fnt'], [])
        self.assertEqual(json_response['data']['total'], [])
        actual_from_date = datetime.strptime(json_response['data']['from'], "%Y-%m-%dT%H:%M:%S")
        actual_to_date = datetime.strptime(json_response['data']['to'], "%Y-%m-%dT%H:%M:%S")
        self.assertEqual(actual_from_date.strftime(self.DATE_FORMAT), from_date.strftime(self.DATE_FORMAT))
        self.assertEqual(actual_to_date.strftime(self.DATE_FORMAT), to_date.strftime(self.DATE_FORMAT))

    def test_get_traffic_summary(self):
        self.client.login(username="test", password="test")
        expected_counts_sac = [] 
        expected_counts_fnt = [] 
        expected_counts_total = [] 
        for i in range(5):
            timestamp = timezone.now() - timezone.timedelta(days=i)
            self.generate_sample_traffic_entry(i, timestamp, self.sac_store, i)            
            self.generate_sample_traffic_entry(i, timestamp, self.fnt_store, i)
            expected_counts_sac.append({"date":timestamp.strftime(self.DATE_FORMAT), "people_count":i})
            expected_counts_fnt.append({"date":timestamp.strftime(self.DATE_FORMAT), "people_count":i})
            expected_counts_total.append({"date":timestamp.strftime(self.DATE_FORMAT), "people_count":i+i})

        from_date = timezone.localtime() - timezone.timedelta(days=5)
        to_date = timezone.localtime() + timezone.timedelta(days=1)
        data = {'from': from_date.strftime(self.DATE_FORMAT), 'to': to_date.strftime(self.DATE_FORMAT)}
        response = self.client.get(
            '/api/traffic-summary/',
            data=data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # verify number of entries
        json_response = json.loads(response.content.decode("utf-8"))
        self.assertEqual(len(json_response['data']['total']), 5)
        self.assertEqual(len(json_response['data']['sac']), 5)
        self.assertEqual(len(json_response['data']['fnt']), 5)

        # verify individual counts
        for entry in json_response['data']['total']:
            expected_entry = [item for item in expected_counts_total if item["date"] == entry["date"]][0]
            self.assertEqual(entry["people_count"], expected_entry["people_count"])
        for entry in json_response['data']['sac']:
            expected_entry = [item for item in expected_counts_sac if item["date"] == entry["date"]][0]
            self.assertEqual(entry["people_count"], expected_entry["people_count"])
        for entry in json_response['data']['fnt']:
            expected_entry = [item for item in expected_counts_fnt if item["date"] == entry["date"]][0]
            self.assertEqual(entry["people_count"], expected_entry["people_count"])
        
        
    def test_traffic_entry_crud(self):
        self.client.login(username="test", password="test")

        # create
        response = self.client.post(
            '/api/traffic/',
            data=json.dumps(self.valid_traffic_entry),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # read
        json_response = json.loads(response.content.decode("utf-8"))
        id = json_response['pk']
        response = self.client.get(
            '/api/traffic/%d/' % id
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # update
        traffic_entry = json.loads(response.content.decode("utf-8"))
        traffic_entry['count'] = 10
        traffic_entry['comments'] = "UPDATED"
        traffic_entry['visitor_type'] = "walkin"
        del traffic_entry['associate_name']
        response = self.client.put(
            '/api/traffic/%d/' % id,
            data=json.dumps(traffic_entry),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        traffic_entry = json.loads(response.content.decode("utf-8"))
        self.assertEquals(traffic_entry['comments'], "UPDATED")
        self.assertEquals(traffic_entry['count'], 10)
        self.assertEquals(traffic_entry['visitor_type'], "walkin")

        # delete
        response = self.client.delete(
            '/api/traffic/%d/' % id
        )
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(
            '/api/traffic/%d/' % id
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def generate_sample_traffic_entry(self, index, timestamp, store, count):
        new_traffic_entry = {
            'timestamp': timestamp,
            'store': store, #random.choice(self.sac_store.id, self.fnt_store.id),
            'count': count, #random.randint(1, 5),
            'visitor_type': 'customer',
            'referred_by': 'GOOG',
            'visit_duration': '%d:15' % index,
            'first_name': 'John #%d' % index,
            'last_name': 'Doe',
            'email': 'j.doe@gmail.com',
            'comments': 'In&Out #%d' % index,
            'associate': self.user
        }
        StoreTrafficEntry.objects.create(**new_traffic_entry).save()

