import django_filters as filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit
from django import forms
from django.forms.fields import SplitDateTimeField
from django_filters.fields import DateTimeRangeField
from store_traffic.models import StoreTrafficEntry

from datetime import datetime, timedelta


class BootstrapDateRangeWidget(forms.MultiWidget):
    def render(self, name, value, attrs=None):
        return super(BootstrapDateRangeWidget, self).render(name, value, attrs=None, renderer=None)

    def __init__(self, attrs=None):
        widgets = (SplitDateTimeField(),
                   SplitDateTimeField(),)
        super(BootstrapDateRangeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.start, value.stop]
        return [None, None]

    def format_output(self, rendered_widgets):
        return '-'.join(rendered_widgets)


class FurnDateRangeField(forms.MultiValueField):
    widget = BootstrapDateRangeWidget

    def __init__(self, *args, **kwargs):
        fields = (
            forms.DateTimeField(),
            forms.DateTimeField(),
        )
        super(FurnDateRangeField, self).__init__(fields, *args, **kwargs)

    def compress(self, data_list):
        if data_list:
            return slice(*data_list)
        return None


class FurnDateRangeFilter(filters.RangeFilter):
    field_class = DateTimeRangeField

    def __init__(self, *args, **kwargs):
        super(FurnDateRangeFilter, self).__init__(*args, **kwargs)


class StoreTrafficEntryFilter(filters.FilterSet):
    #timestamp_range = FurnDateRangeFilter(
    #    field_name='timestamp',
    #)
    timestamp_range_from = filters.DateTimeFilter(field_name='timestamp', label='Date From', lookup_expr='gte')
    timestamp_range_to = filters.DateTimeFilter(field_name='timestamp', label='Date To', lookup_expr='lte')

    def __init__(self, *args, **kwargs):
        super(StoreTrafficEntryFilter, self).__init__(*args, **kwargs)

        default_from_dt = datetime.now() - timedelta(days=60)
        default_to_dt = datetime.now()
        self.filters['timestamp_range_from'].field.initial = default_from_dt
        self.filters['timestamp_range_from'].field.initial = default_to_dt

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True
        self.helper.layout = Layout(
            Div(
                'timestamp_range_from',
                'timestamp_range_to',
                'store',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )

    class Meta:
        model = StoreTrafficEntry
        fields = ['timestamp', 'store', 'count']
