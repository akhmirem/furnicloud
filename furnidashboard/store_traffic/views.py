from django.contrib.auth import get_user_model
from django.utils import timezone
from django.db.models import Count, Sum, Q
from rest_framework import viewsets, filters, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from store_traffic.filters import StoreTrafficEntryFilter
from store_traffic.models import StoreTrafficEntry
from store_traffic.serializers import StoreTrafficSerializer
from datetime import datetime, timedelta


class StoreTrafficViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Commissions to be viewed or edited.
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    # permission_classes = (AllowAny,)

    queryset = StoreTrafficEntry.objects.all()
    serializer_class = StoreTrafficSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = StoreTrafficEntryFilter
    # filter_fields = ('associate__first_name', 'order__number', 'paid')'


class StoreTrafficSummaryView(APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    # permission_classes = (AllowAny,)


    def get(self, request, *args, **kwargs):
        from_date = request.GET.get('from', '')
        to_date = request.GET.get('to', '')

        if from_date == '' or to_date == '':
            now = datetime.now()
            from_date = now - timedelta(days=60)
            # start = datetime(now.year, now.month, now.day, 0, 0, 0)
            from_date = timezone.make_aware(from_date, timezone.get_current_timezone())

            to_date = now + timedelta(days=1)
            to_date = timezone.make_aware(to_date, timezone.get_current_timezone())
        else:
            from_date = datetime.strptime(from_date, '%Y-%m-%d')
            to_date = datetime.strptime(to_date, '%Y-%m-%d')

        lookup_kwargs = {
            '%s__gte' % 'timestamp': from_date,
            '%s__lt' % 'timestamp': to_date,
        }
        #truncate_date = connection.ops.datetime_trunc_sql('day', 'timestamp', timezone.get_current_timezone())
        truncate_date = u"CAST(DATE_FORMAT(CONVERT_TZ(timestamp, 'UTC', 'America/Los_Angeles'), '%%Y-%%m-%%d') AS DATE)"

        try:
            # replace the aggregation SQL statement for sqlite which is used for testing
            from django.db import connection
            if 'sqlite' in connection.vendor:
                #truncate_date = u"strftime('%%Y-%%m-%%d', datetime(timestamp, 'PDT')) "
                truncate_date = u"strftime('%%Y-%%m-%%d', timestamp) "
        except Exception:
            pass

        qs = StoreTrafficEntry.objects.filter(**lookup_kwargs).extra({'date': truncate_date})
        #qs = StoreTrafficEntry.objects.extra({'date': truncate_date})
        total_counts = qs.values('date') \
            .annotate(party_count=Count('id'), people_count=Sum('count')) \
            .order_by()
        sac_counts = qs.filter(store__pk=1).values('date') \
            .annotate(party_count=Count('id'), people_count=Sum('count')) \
            .order_by()

        fnt_counts = qs.filter(store__pk=2).values('date') \
            .annotate(party_count=Count('id'), people_count=Sum('count')) \
            .order_by()

        result = {'data': {'sac': sac_counts, 'fnt': fnt_counts, 'total': total_counts, 'from': from_date, 'to': to_date}}
        response = Response(result, status=status.HTTP_200_OK)
        return response


class APIClientView(APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    # permission_classes = (AllowAny,)

    def get(self, request):
        user_model = get_user_model()
        associates = user_model.objects.filter(Q(is_active=True) & Q(groups__name__icontains="associates"))
        associate_list = [{'associateId': associate.id,
                           'associateName': associate.first_name} for associate in associates]

        result = {
            'data': {
                'currentAssociateId': request.user.id,
                'currentAssociateName': request.user.first_name,
                'associateList': associate_list
            }
        }
        response = Response(result, status=status.HTTP_200_OK)
        return response
