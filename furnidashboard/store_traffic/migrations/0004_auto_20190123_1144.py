# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store_traffic', '0003_auto_20181010_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='storetrafficentry',
            name='comments',
            field=models.TextField(max_length=500, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='storetrafficentry',
            name='email',
            field=models.EmailField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='storetrafficentry',
            name='first_name',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='storetrafficentry',
            name='last_name',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
