# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store_traffic', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='storetrafficentry',
            name='referred_by',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'NO', b'Not Referred'), (b'RET', b'Returning Customer'), (b'RVL', b'Roseville Store'), (b'CST', b'Referred by existing Furnitalia customer'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'WEB', b'Website'), (b'EBL', b'E-mail (or Eblast)'), (b'MAG', b'Magazine'), (b'SOC', b'Social networks'), (b'NWP', b'Newspaper'), (b'TV', b'TV')]),
        ),
        migrations.AddField(
            model_name='storetrafficentry',
            name='visit_duration',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='storetrafficentry',
            name='visitor_type',
            field=models.CharField(blank=True, max_length=25, null=True, choices=[(b'walkin', b'Walk-in'), (b'customer', b'Returning Customer'), (b'lead', b'In-progress Lead')]),
        ),
    ]
