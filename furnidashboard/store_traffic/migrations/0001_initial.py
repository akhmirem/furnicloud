# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stores', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='StoreTrafficEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('count', models.IntegerField(default=1, blank=True)),
                ('store', models.ForeignKey(related_name='store_traffic', default=None, blank=True, to='stores.Store', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 'store_traffic',
            },
        ),
    ]
