# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store_traffic', '0002_auto_20170428_1038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storetrafficentry',
            name='referred_by',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'RVL', b'Roseville Store'), (b'RET', b'Returning Customer'), (b'GOOG', b'Google/Bing Search Engines'), (b'EBL', b'E-Blast'), (b'WEB', b'Website Direct Visit'), (b'VENDOR', b'Vendor Website Referral'), (b'TV', b'TV'), (b'MAG', b'Magazine'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'SOC', b'Social networks'), (b'EXPO', b'Home & Landscape Show'), (b'CST', b'Referred by existing Furnitalia customer'), (b'NWP', b'Newspaper'), (b'NO', b'Not Referred')]),
        ),
    ]
