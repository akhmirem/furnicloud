# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('store_traffic', '0004_auto_20190123_1144'),
    ]

    operations = [
        migrations.AddField(
            model_name='storetrafficentry',
            name='associate',
            field=models.ForeignKey(related_name='store_traffic_entries', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL),
        ),
        migrations.AlterField(
            model_name='storetrafficentry',
            name='referred_by',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'RVL', b'Roseville Store'), (b'RET', b'Returning Customer'), (b'GOOG', b'Google/Bing Search Engines'), (b'EBL', b'E-Blast'), (b'WEB', b'Website Direct Visit'), (b'VENDOR', b'Vendor Website Referral'), (b'TV', b'TV'), (b'MAG', b'Magazine'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'SOC', b'Social networks'), (b'EXPO', b'Home & Landscape Show'), (b'CST', b'Referred by existing Furnitalia customer'), (b'NWP', b'Newspaper'), (b'NO', b'Not Referred'), (b'UNK', b'Unknown/could not find out'), (b'OTH', b'Other (see comments)')]),
        ),
        migrations.AlterField(
            model_name='storetrafficentry',
            name='visitor_type',
            field=models.CharField(blank=True, max_length=25, null=True, choices=[(b'walkin', b'General Walk-in'), (b'customer', b'Returning Customer'), (b'lead', b'In-progress Lead'), (b'other', b'Other (see comments)')]),
        ),
    ]
