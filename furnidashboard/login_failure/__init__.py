import logging

from django.contrib.auth.signals import user_login_failed

logger = logging.getLogger('furnicloud')


def log_login_failure(sender, credentials, request, **kwargs):
    http_request = request

    msg = "Login failure {}".format(http_request.META['REMOTE_ADDR'])
    logger.error(msg)


user_login_failed.connect(log_login_failure)
