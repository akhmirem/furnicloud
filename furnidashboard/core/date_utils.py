from datetime import timedelta
from django.utils import timezone


def get_prev_month_date():
    today_date = timezone.now()
    month_ago = today_date - timedelta(days=30)
    return month_ago
