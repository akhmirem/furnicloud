import re
from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.db.models import Q, Prefetch, F, Sum
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView
from django.http import QueryDict
from django_tables2 import SingleTableView

from commissions.models import Commission
from core import utils
from customers.models import Customer
from orders.cron_tasks.missed_orders_notification import get_skipped_orders
from orders.managers import apply_cutoff_date_to_qs
from orders.models import Order, OrderItem
from orders.tables import OrderTable, UnplacedOrdersTable
from .mixins import LoginRequiredMixin
from .filters import SpecialOrdersFilter


class AlertsTableView(LoginRequiredMixin, SingleTableView):
    model = Order
    table_class = OrderTable
    template_name = "core/alerts_table.html"
    subtitle = ""
    message = ""

    def get_context_data(self, **kwargs):
        context = super(AlertsTableView, self).get_context_data(**kwargs)
        context['alert_title'] = self.subtitle
        context['message'] = mark_safe(self.message)
        return context


class UnplacedOrderTableView(AlertsTableView):
    subtitle = 'Unplaced Orders'
    table_class = UnplacedOrdersTable

    def get_queryset(self, **kwargs):
        return Order.objects.unplaced_orders() \
            .select_related('store') \
            .select_related('customer') \
            .prefetch_related('payments') \
            .prefetch_related('ordered_items') \
            .prefetch_related(Prefetch('commissions', queryset=Commission.objects.select_related('associate')))


class SpecialOrdersTableView(AlertsTableView):
    subtitle = 'Active Special Orders'
    table_class = OrderTable
    context_filter_name = 'filter'
    filter_class = SpecialOrdersFilter
    filter_form_id = 'special-orders-filter'

    def get_queryset(self, **kwargs):
        qs = Order.objects.special_orders() \
            .select_related('store') \
            .select_related('customer') \
            .prefetch_related('payments') \
            .prefetch_related('ordered_items') \
            .prefetch_related(Prefetch('commissions', queryset=Commission.objects.select_related('associate')))
        self.setup_filter(queryset=qs)
        return self.filter.qs.distinct()

    def setup_filter(self, **kwargs):
        # Create a mutable QueryDict object, default is immutable
        initial = QueryDict(mutable=True)
        # initial['order_date_from'] = core_utils.get_date_n_months_ago(36)
        # initial['order_date_to'] = core_utils.get_month_last_day()

        # if no filters are present, try to get filters stored in session

        initial.update(self.request.GET)
        # if 'order_date_from' not in self.request.GET:
            # restore filters from session
            # initial.update(self.request.session.get('commission_filters', QueryDict()))

        self.filter = self.filter_class(initial, queryset=kwargs['queryset'])

        # save selected filters to session if Filter button was pressed
        # if 'order_date_from' in self.request.GET:
            # self.request.session['commission_filters'] = self.filter.data

        self.filter.helper.form_id = self.filter_form_id
        self.filter.helper.form_method = "get"

    def get_context_data(self, **kwargs):
        context = super(SpecialOrdersTableView, self).get_context_data(**kwargs)
        context[self.context_filter_name] = self.filter 
        return context

class OrderFinancingAlertTableView(AlertsTableView):
    model = Order
    subtitle = "Unactivated Order Financing"

    def get_table_data(self):
        return Order.objects.financing_unactivated() \
            .select_related('store') \
            .select_related('customer') \
            .prefetch_related('payments') \
            .prefetch_related(Prefetch('commissions', queryset=Commission.objects.select_related('associate')))


class UncollectedBalanceAlertTableView(AlertsTableView):
    model = Order
    subtitle = "Orders with Uncollected Balance"

    def get_table_data(self):
        # qs = Order.objects.raw('''
        #         SELECT o.*
        #             , round(o.subtotal_after_discount + ifnull(o.tax,0) + ifnull(o.shipping,0),2) AS order_subtotal
        #             , p.paid_amount AS paid_amount
        #         FROM order_info o
        #         LEFT JOIN (
        #             SELECT order_id, round(sum(ifnull(amount,0)),2) AS paid_amount FROM order_payments GROUP BY order_id
        #             ) p ON p.order_id=o.id
        #         WHERE round(o.subtotal_after_discount + ifnull(o.tax,0) + ifnull(o.shipping,0),2) > paid_amount
        #         LIMIT 1000'''
        # )

        qs = Order.objects.get_qs() \
            .annotate(due=(Sum('payments__amount') - F('subtotal_after_discount') - F('tax') - F('shipping'))) \
            .select_related('store') \
            .select_related('customer') \
            .prefetch_related('payments') \
            .prefetch_related(Prefetch('commissions', queryset=Commission.objects.select_related('associate'))) \
            .filter(due__lt=-0.0001) \
            .filter(~Q(status__in=['E'])) \
            .order_by("-status")
        qs = apply_cutoff_date_to_qs(qs, utils.date_with_tz(datetime(2020, 1, 1)))

        return qs

    def get_table(self, **kwargs):
        kwargs.update({"order_by": "-status"})
        return super(UncollectedBalanceAlertTableView, self).get_table(**kwargs)


class ReadyForDeliveryOrdersAlertTableView(AlertsTableView):
    model = Order
    subtitle = "Orders Ready for Delivery"

    def get_table_data(self):
        qs = Order.objects.get_qs() \
            .annotate(due=(Sum('payments__amount') - F('subtotal_after_discount') - F('tax') - F('shipping'))) \
            .select_related('store') \
            .select_related('customer') \
            .prefetch_related('payments') \
            .prefetch_related(Prefetch('commissions', queryset=Commission.objects.select_related('associate'))) \
            .filter(due__gte=-1) \
            .filter(status__in=['R', 'N', 'PARTIALLY_DELIVERED'])
        qs = apply_cutoff_date_to_qs(qs, utils.date_with_tz(datetime(2020, 1, 1)))
        return qs

    def get_table(self, **kwargs):
        kwargs.update({"order_by": "order_date"})
        return super(ReadyForDeliveryOrdersAlertTableView, self).get_table(**kwargs)


class MissedOrdersAlertTableView(LoginRequiredMixin, TemplateView):
    template_name = "core/missed_orders.html"
    subtitle = "Missed Orders"
    message = ""

    def get_context_data(self, **kwargs):
        context = super(MissedOrdersAlertTableView, self).get_context_data(**kwargs)

        missed_orders = get_skipped_orders()

        context['alert_title'] = self.subtitle
        context['message'] = mark_safe(self.message)
        context['missed_orders'] = missed_orders
        return context


# SEARCH VIEW and RELATED FUNCTIONS
def normalize_query(query_string, findterms=re.compile(r'"([^"]+)"|([^\s]+)').findall,
                    normspaces=re.compile(r'\s{2,}').sub):
    """ Splits the query string in individual keywords, getting rid of unnecessary spaces and grouping quoted words together.
        Example:
        >>> normalize_query(' soem random words "with quotes " and    spaces')
        >>> ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    """

    return [normspaces(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    """ Returns a query that is a combination of Q objects. That combination aims to search keywords within a model by testing the given search fields
    """

    query = None  # Query to search for every search term

    terms = normalize_query(query_string)
    for term in terms:
        or_query = None  # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query |= q
        if query is None:
            query = or_query
        else:
            query &= or_query
    return query


@login_required
def search(request):
    query_string = ''
    found_orders = None
    found_customers = None
    matched_items_by_ack_num = None
    matched_items_by_container_num = None

    if request.GET:
        query_string = request.GET.get('q', '').strip()

        if len(query_string):
            entry_query = get_query(query_string, ['number', ])  # , 'comments',])
            found_orders = Order.objects.filter(entry_query) \
                .select_related('customer') \
                .order_by('-created')

            entry_query = get_query(query_string, ['first_name', 'last_name', 'email', 'phone', 'mobile'])
            found_customers = Customer.objects.filter(entry_query) \
                .order_by('-last_name')

            ack_num_query = get_query(query_string, ['ack_num', ])
            matched_items_by_ack_num = OrderItem.objects.filter(ack_num_query) \
                .select_related('order') \
                .prefetch_related('order__customer') \
                .order_by('order_id')

            container_num_query = get_query(query_string, ['container_num', ])
            matched_items_by_container_num = OrderItem.objects.filter(container_num_query) \
                .select_related('order') \
                .prefetch_related('order__customer') \
                .order_by('order_id')

    return render(request, 'search/search_results.html',
                  {'query_string': query_string,
                   'found_orders': found_orders,
                   'found_customers': found_customers,
                   'found_items': matched_items_by_ack_num,
                   'found_items_container_num': matched_items_by_container_num,
                   },
                  )
