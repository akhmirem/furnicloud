import django_tables2 as tables
from django.conf import settings
from django.utils.safestring import mark_safe
from core import utils


class DollarAmountColumn(tables.Column):
    def render(self, value):
        return utils.dollars(value)


class CustomTextLinkColumn(tables.LinkColumn):
    def __init__(self, viewname, urlconf=None, args=None, kwargs=None, current_app=None, attrs=None, custom_text=None,
                 **extra):
        super(CustomTextLinkColumn, self).__init__(viewname, urlconf=urlconf, args=args, kwargs=kwargs,
                                                   current_app=current_app, attrs=attrs, **extra)
        self.custom_text = custom_text

    def render(self, record, value):
        if self.custom_text:
            value = self.custom_text

        return super(CustomTextLinkColumn, self).render(record, value)


class TruncateTextColumn(tables.Column):
    def __init__(self, trunc_length=30, *args, **kwargs):
        self.trunc_length = trunc_length
        super(TruncateTextColumn, self).__init__(*args, **kwargs)

    def render(self, value):
        return mark_safe(value[:self.trunc_length] + '...' if len(value) > self.trunc_length else value)


class OrderTrackingAlertsTable(tables.Table):
    order = tables.Column()
    associate = tables.Column()
    customer = tables.Column()
    order_date = tables.DateColumn(format=settings.DATE_FORMAT_STANDARD)
    order_status = tables.Column()
    order_subtotal = DollarAmountColumn('Subtotal')
    order_grandtotal = DollarAmountColumn('Grand Total')
    order_balance_due = DollarAmountColumn('Balance Due')
    tracking_alert = tables.Column('Note')

    def render_order(self, value):
        return mark_safe('<a href="{0}">{1}</a>'.format(value.get_absolute_url(), str(value)))

    class Meta:
        order_by = ('associate', '-order_date', 'order')
        # attrs = {"class": "paleblue"}
        #row_attrs = {
        #    'data-commission-status': lambda record: record['commission_status']
        #}
        template_name = 'core/django_tables_responsive.html'
