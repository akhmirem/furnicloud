import django_filters as filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Div, Field
from django.contrib.auth import get_user_model
from django.db.models import Q
from orders.models import Order

EXCLUDED_ORDER_STATUSES = ['H', 'P', 'T', 'X', 'I', 'C']
CHOICES_FOR_STATUS_FILTER = filter(
    lambda c: c[0] not in EXCLUDED_ORDER_STATUSES, list(Order.ORDER_STATUSES))


class SpecialOrdersFilter(filters.FilterSet):
    status = filters.ChoiceFilter(choices=CHOICES_FOR_STATUS_FILTER)

    def __init__(self, *args, **kwargs):
        super(SpecialOrdersFilter, self).__init__(*args, **kwargs)

        user_model = get_user_model()

        self.filters['commissions__associate'].field.queryset = user_model.objects \
            .filter(Q(is_active=True)) \
            .filter(Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers")) \
            .order_by('username') \
            .distinct()
        self.filters['commissions__associate'].field.label = 'Associate'

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            Div(
                Field('status'),
                Field('ordered_items__vendor'),
                Field('commissions__associate'),
                Submit('submit', 'Filter', css_class='btn-default align-bottom'),
                css_class='well'
            )
        )

    class Meta:
        model = Order
        fields = ['status', 'ordered_items__vendor', 'commissions__associate']
