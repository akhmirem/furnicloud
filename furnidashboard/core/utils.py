import calendar
import sys
from datetime import datetime, timedelta

from django.contrib import messages
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils import timezone


def dollars(amount):
    if not isinstance(amount, float):
        try:
            amount = float(amount)
        except TypeError:
            amount = 0.0
    dollar_amount = round(amount, 2)
    return "$ {0}{1}".format(intcomma(int(dollar_amount)), ("%0.2f" % dollar_amount)[-3:])


def is_user_delivery_group(request):
    """
    Determine if currently logged in user belongs to delivery_persons group
    """
    return request.user.groups.filter(name="delivery_persons").exists()


def delivery_form_empty(form_data):
    """
    Check if delivery form is empty to determine if we need to save the instance
    """
    return form_data['delivery_type'] == None


def get_month_date_range(month, year):
    # 1st day of month
    from_date = date_with_tz(datetime(year, month, 1))
    from_date.replace(hour=0, minute=0, second=0)

    # get last day of month
    weekday, num_days = calendar.monthrange(year, month)
    to_date = date_with_tz(from_date + timedelta(days=num_days - 1))
    to_date = to_date.replace(hour=23, minute=59, second=59)

    return from_date, to_date


def get_current_month_date_range():
    to_date = timezone.now()    # cur date
    return get_month_date_range(to_date.month, to_date.year)


def get_date_range_from_string(range_str, request):
    to_date = timezone.now()
    if range_str == 'week':
        from_date = to_date - timedelta(days=to_date.weekday())
        from_date = from_date.replace(hour=0, minute=0, second=0)
    elif range_str == 'last-week':
        to_date = to_date - timedelta(days=to_date.weekday())
        to_date = to_date.replace(hour=0, minute=0, second=0)
        from_date = to_date - timedelta(days=7)
    elif range_str == 'month':
        from_date, to_date = get_month_date_range(to_date.month, to_date.year)
    elif range_str == 'last-month':
        to_date = datetime(to_date.year, to_date.month, 1)
        from_date = to_date - timedelta(days=1)
        from_date = datetime(from_date.year, from_date.month, 1)
    elif range_str == 'year':
        from_date = datetime(to_date.year, 1, 1)
    elif range_str == 'last-year':
        from_date = datetime(to_date.year - 1, 1, 1)
        to_date = datetime(to_date.year - 1, 12, 31)
    elif range_str == 'ytd-last-year':
        from_date = datetime(to_date.year - 1, 1, 1)
        to_date = datetime(to_date.year - 1, to_date.month, to_date.day)
    elif range_str == 'custom':
        try:
            from_date = datetime.strptime(request.GET['range_from'], "%m/%d/%Y")
            from_date = date_with_tz(from_date)
            from_date = from_date.replace(hour=0, minute=0, second=0)
            to_date = datetime.strptime(request.GET['range_to'], "%m/%d/%Y")
            to_date = date_with_tz(to_date)
            to_date = to_date.replace(hour=23, minute=59, second=59)
        except (KeyError, ValueError) as e:
            # raise ValueError("Incorrect date format, should be YYYY-MM-DD")
            messages.add_message(request, messages.ERROR, "Incorrect date format, should be MM/DD/YYYY",
                                 extra_tags="alert alert-danger")
            from_date = to_date - timedelta(days=to_date.weekday())
            from_date = from_date.replace(hour=0, minute=0, second=0)
    else:  # default
        from_date = to_date - timedelta(days=to_date.weekday())
        from_date = from_date.replace(hour=0, minute=0, second=0)

    return from_date, to_date


def get_month_last_day():
    # get last day of month
    cur_date = timezone.now()
    from_date = date_with_tz(datetime(cur_date.year, cur_date.month, 1))
    weekday, num_days = calendar.monthrange(from_date.year, from_date.month)
    last_month_day = from_date + timedelta(days=num_days)
    return last_month_day


def get_date_n_months_ago(n):
    return date_with_tz(timezone.now() - timedelta(days=30 * n))


def date_with_tz(date):
    if timezone.is_naive(date):
        return timezone.make_aware(date, timezone.get_current_timezone())
    return date
