from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(name='comma_to_br')
def comma_to_br(value):
    """Replaces all commas with line break in the given string."""
    return mark_safe(value.replace(",", '<br />'))
