from django.conf import settings


def global_settings(request):
    # return any necessary values
    return {
        'WEB_UI_URL': settings.WEB_UI_URL,
        'STORE_TRAFFIC_URL': settings.STORE_TRAFFIC_URL,
        'WAREHOUSE_URL': settings.WAREHOUSE_LOG_URL,
    }
