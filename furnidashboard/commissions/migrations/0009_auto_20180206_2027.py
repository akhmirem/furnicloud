# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0008_auto_20180119_1959'),
    ]

    operations = [
        migrations.AddField(
            model_name='commission',
            name='manager_commission_flag',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='commissionauditlogentry',
            name='manager_commission_flag',
            field=models.BooleanField(default=False),
        ),
    ]
