# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations
import datetime


def convert_commission_data(apps, schema_editor):
    Commission = apps.get_model("commissions", "Commission")

    for c in Commission.objects.all():
        if c.paid:
            c.status = 'PAID'
        else :
            c.status = 'NEW'
        c.comments="Last updated on %s by conversion" % datetime.datetime.now().strftime('%Y-%m-%d')
        c.save()


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0004_auto_20161018_1418'),
    ]

    operations = [
        migrations.RunPython(convert_commission_data),
    ]
