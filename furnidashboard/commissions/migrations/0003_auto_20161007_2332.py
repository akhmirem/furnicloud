# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0002_auto_20160913_1111'),
    ]

    operations = [
        #migrations.AlterField(
        #    model_name='commission',
        #    name='associate',
        #    field=models.ForeignKey(related_name='order_comissions', default=0, blank=True, to=settings.AUTH_USER_MODEL),
        #),
        migrations.AlterField(
            model_name='commission',
            name='order',
            field=models.ForeignKey(related_name='commissions', to='orders.Order', on_delete=models.CASCADE),
        ),
        #migrations.AlterField(
        #    model_name='commissionauditlogentry',
        #    name='associate',
        #    field=models.ForeignKey(related_name='_auditlog_order_comissions', default=0, blank=True, to=settings.AUTH_USER_MODEL),
        #),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='order',
            field=models.ForeignKey(related_name='_auditlog_commissions', to='orders.Order', on_delete=models.CASCADE),
        ),
    ]
