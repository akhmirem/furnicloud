# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0009_auto_20180206_2027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commission',
            name='status',
            field=models.CharField(default=b'NEW', max_length=50, choices=[(b'NEW', b'Unclaimed'), (b'PENDING', b'Claimed'), (b'INELIGIBLE', b'Ineligible/Rejected'), (b'PAID', b'Paid')]),
        ),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='status',
            field=models.CharField(default=b'NEW', max_length=50, choices=[(b'NEW', b'Unclaimed'), (b'PENDING', b'Claimed'), (b'INELIGIBLE', b'Ineligible/Rejected'), (b'PAID', b'Paid')]),
        ),
    ]
