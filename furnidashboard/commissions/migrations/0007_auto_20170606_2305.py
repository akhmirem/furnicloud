# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from datetime import datetime


def mark_commissions_paid(apps, schema_editor):
    Commission = apps.get_model("commissions", "Commission")

    dt = datetime.strptime('2016-12-31', '%Y-%m-%d')

    for c in Commission.objects.filter(order__order_date__lte=dt):
        if not c.paid:
            c.status = 'PAID'
            c.paid = True
            c.paid_date=dt
            if len(c.comments):
                c.comments += "\n"
            c.comments += "Marked as PAID on %s by conversion" % datetime.now().strftime('%Y-%m-%d')
            c.save()


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0006_auto_20170105_1836'),
    ]

    operations = [
        migrations.RunPython(mark_commissions_paid),
    ]
