# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0010_auto_20180504_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commission',
            name='associate',
            field=models.ForeignKey(related_name='order_commissions', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='associate',
            field=models.ForeignKey(related_name='_auditlog_order_commissions', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
    ]
