# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0003_auto_20161007_2332'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commission',
            options={'permissions': (('update_commissions_payment', 'Can Update Commission Payment Information'), ('view_commissions', 'View all commissions'))},
        ),
        migrations.AddField(
            model_name='commission',
            name='amount',
            field=models.FloatField(default=0.0, blank=True),
        ),
        migrations.AddField(
            model_name='commission',
            name='status',
            field=models.CharField(default=b'NEW', max_length=50, choices=[(b'NEW', b'New'), (b'PENDING', b'Pending'), (b'INELIGIBLE', b'Ineligible/Rejected'), (b'PAID', b'Paid')]),
        ),
        migrations.AddField(
            model_name='commissionauditlogentry',
            name='amount',
            field=models.FloatField(default=0.0, blank=True),
        ),
        migrations.AddField(
            model_name='commissionauditlogentry',
            name='status',
            field=models.CharField(default=b'NEW', max_length=50, choices=[(b'NEW', b'New'), (b'PENDING', b'Pending'), (b'INELIGIBLE', b'Ineligible/Rejected'), (b'PAID', b'Paid')]),
        ),
        #migrations.AlterField(
        #    model_name='commission',
        #    name='associate',
        #    field=models.ForeignKey(related_name='order_commissions', default=0, blank=True, to=settings.AUTH_USER_MODEL),
        #),
        #migrations.AlterField(
        #    model_name='commissionauditlogentry',
        #    name='associate',
        #    field=models.ForeignKey(related_name='_auditlog_order_commissions', default=0, blank=True, to=settings.AUTH_USER_MODEL),
        #),
    ]
