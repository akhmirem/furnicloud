# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0005_convert_commissions'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commission',
            options={'permissions': (('update_commissions_payment', 'Can Update Commission Payment Information'), ('view_commissions', 'View all commissions'), ('edit_commissions', 'Change Commissions'))},
        ),
        migrations.AlterField(
            model_name='commission',
            name='associate',
            # field=models.ForeignKey(related_name='order_commissions', blank=True, to=settings.AUTH_USER_MODEL, null=True, null=True),
            field=models.ForeignKey(related_name='order_commissions', to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='commission',
            name='paid_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commission',
            name='status',
            field=models.CharField(default=b'NEW', max_length=50, choices=[(b'NEW', b'Unclaimed'), (b'PENDING', b'Pending'), (b'INELIGIBLE', b'Ineligible/Rejected'), (b'PAID', b'Paid')]),
        ),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='associate',
            field=models.ForeignKey(related_name='_auditlog_order_commissions', to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='paid_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='status',
            field=models.CharField(default=b'NEW', max_length=50, choices=[(b'NEW', b'Unclaimed'), (b'PENDING', b'Pending'), (b'INELIGIBLE', b'Ineligible/Rejected'), (b'PAID', b'Paid')]),
        ),
    ]
