# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commissions', '0007_auto_20170606_2305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commission',
            name='paid_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='commissionauditlogentry',
            name='paid_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
