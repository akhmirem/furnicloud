import django_filters as filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit
from django.contrib.auth import get_user_model
from django.db.models import Q

from commissions.models import Commission
from orders.models import Order
from orders.forms import CustomDatePickerInput

DATEPICKER_OPTIONS = {"format": "mm/dd/yyyy",
                      "todayHighlight": "true",
                      "todayBtn": 'linked'}
DATE_FORMAT = "%m/%d/%Y"


class CommissionAPIFilter(filters.FilterSet):
    div_attrs = {'class': 'input-group date'}
    attrs = {'class': 'form-control'}
    order_number = filters.CharFilter(
        field_name='order__number', lookup_expr='icontains', label='Order #')
    order_date = filters.DateFilter(field_name="order__order_date")
    associate_name = filters.CharFilter(field_name="associate__username")
    order_date_from = filters.DateFilter(field_name='order__order_date', label='Order Date From', lookup_expr='gte',
                                         widget=CustomDatePickerInput(div_attrs=div_attrs, attrs=attrs,
                                                                      format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    order_date_to = filters.DateFilter(field_name='order__order_date', label='To', lookup_expr='lte',
                                       widget=CustomDatePickerInput(div_attrs=div_attrs, attrs=attrs,
                                                                    format=DATE_FORMAT, options=DATEPICKER_OPTIONS))

    def __init__(self, *args, **kwargs):
        super(CommissionAPIFilter, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True
        self.helper.layout = Layout(
            Div(
                'status',
                'associate',
                'order_number',
                'order_date_from',
                'order_date_to',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )

    class Meta:
        model = Commission
        fields = ['order', 'order_number', 'order_date', 'order_date_from', 'order_date_to', 'associate',
                  'associate_name',
                  'status', 'paid_date']


# CHOICES_FOR_STATUS_FILTER = [('', '--Any--')]
CHOICES_FOR_STATUS_FILTER = []
CHOICES_FOR_STATUS_FILTER.extend(list(Commission.COMMISSION_STATUES))


class CommissionListFilter(filters.FilterSet):
    div_attrs = {'class': 'input-group date'}
    attrs = {'class': 'form-control'}
    status = filters.ChoiceFilter(choices=CHOICES_FOR_STATUS_FILTER)
    order_date_from = filters.DateFilter(field_name='order__order_date', label='Order Date From', lookup_expr='gte',
                                         widget=CustomDatePickerInput(div_attrs=div_attrs,
                                                                      format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    order_date_to = filters.DateFilter(field_name='order__order_date', label='Order Date To', lookup_expr='lte',
                                       widget=CustomDatePickerInput(div_attrs=div_attrs,
                                                                    format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    order_number = filters.CharFilter(
        field_name='order__number', lookup_expr='icontains', label='Order #')
    order_status = filters.ChoiceFilter(label='Order Status', field_name='order__status',
                                         choices=[ (status_name, label) 
                                                  for status_name, label in Order.ORDER_STATUSES 
                                                    if status_name in Order.ORDER_ACTIVE_STATUSES ])

    def __init__(self, *args, **kwargs):
        super(CommissionListFilter, self).__init__(*args, **kwargs)

        user_model = get_user_model()
        self.filters['associate'].field.queryset = user_model.objects.filter(
            Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers")) \
            .filter(is_active=True) \
            .order_by('username') \
            .distinct()

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True

        self.helper.layout = Layout(
            Div(
                'status',
                'associate',
                'order_status',
                'order_number',
                'order_date_from',
                'order_date_to',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )

    class Meta:
        model = Commission
        fields = ['status', 'associate', 'order_number', 'paid_date', 'order__status']


class CommissionPaymentHistoryFilter(filters.FilterSet):
    div_attrs = {'class': 'input-group date'}
    attrs = {'class': 'form-control'}
    order_number = filters.CharFilter(
        field_name='order__number', lookup_expr='icontains', label='Order #')
    commissions_paid_date_from = filters.DateFilter(field_name='paid_date', label='Paid Date From', lookup_expr='gte',
                                                    widget=CustomDatePickerInput(div_attrs=div_attrs, attrs=attrs,
                                                                                 format=DATE_FORMAT,
                                                                                 options=DATEPICKER_OPTIONS))
    commissions_paid_date_to = filters.DateFilter(field_name='paid_date', label='To', lookup_expr='lte',
                                                  widget=CustomDatePickerInput(div_attrs=div_attrs, attrs=attrs,
                                                                               format=DATE_FORMAT,
                                                                               options=DATEPICKER_OPTIONS))

    def __init__(self, *args, **kwargs):
        super(CommissionPaymentHistoryFilter, self).__init__(*args, **kwargs)

        user_model = get_user_model()
        self.filters['associate'].field.queryset = user_model.objects.filter(
            Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers")) \
            .order_by('username') \
            .distinct()

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True
        self.helper.layout = Layout(
            Div(
                'associate',
                'order_number',
                'commissions_paid_date_from',
                'commissions_paid_date_to',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )

    class Meta:
        model = Commission
        fields = ['associate', 'order_number', 'paid_date']
