from commissions.models import Commission
from rest_framework import serializers
from django.conf import settings


class CommissionSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source="pk")
    associate_name = serializers.StringRelatedField(source="associate.first_name")
    order_number = serializers.ReadOnlyField(source="order.number")
    order_date = serializers.ReadOnlyField(source="order.order_date")
    order_status = serializers.ReadOnlyField(source="order.get_status_display")
    delivered_date = serializers.ReadOnlyField(source="order.delivered_date")
    order_subtotal = serializers.ReadOnlyField(source="order.subtotal_after_discount")
    order_grand_total = serializers.ReadOnlyField(source="order.grand_total")
    order_balance_due = serializers.ReadOnlyField(source="order.balance_due")
    eligible_to_claim = serializers.ReadOnlyField(source="is_eligible_for_claim")
    customer = serializers.ReadOnlyField(source="order.customer.full_name")

    class Meta:
        model = Commission
        fields = ('id', 'order', 'order_number', 'order_date', 'order_status', 'order_subtotal',
                  'order_grand_total', 'order_balance_due', 'associate', 'associate_name', 'status',
                  'paid_date', 'amount', 'comments', 'eligible_to_claim', 'delivered_date', 'customer', 
                  'sale_portion')