from django.contrib.auth import get_user_model
from django.db.models import Q
from django.views.generic import TemplateView
from rest_framework import viewsets, filters, generics
from django_filters.rest_framework import DjangoFilterBackend
from commissions.filters import CommissionAPIFilter, CommissionListFilter
from commissions.models import Commission
from commissions.serializers import CommissionSerializer
from core.mixins import LoginRequiredMixin


class CommissionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Commissions to be viewed or edited.
    """
    queryset = Commission.objects.active()
    serializer_class = CommissionSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = CommissionAPIFilter
    # filter_fields = ('associate__first_name', 'order__number', 'paid')'


class CommissionList(generics.ListCreateAPIView):
    """
    API endpoint that allows Commissions to be viewed or created.
    """
    queryset = Commission.objects.regular().order_by('-order__order_date')
    serializer_class = CommissionSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = CommissionAPIFilter  # CommissionFilter


class CommissionDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows Commission objects to be viewed/edited/deleted.
    """
    queryset = Commission.objects.regular()
    serializer_class = CommissionSerializer


class CommissionListApiView(LoginRequiredMixin, TemplateView):
    template_name = "commissions/commissions_list_ajax.html"

    def get_context_data(self, **kwargs):
        context = super(CommissionListApiView, self).get_context_data()
        context['test'] = "TEST"
        user_model = get_user_model()
        context['associates'] = [{'id': associate.pk, 'name': associate.first_name} for associate in
                                 user_model.objects.filter(
                                     Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers"))]
        return context
