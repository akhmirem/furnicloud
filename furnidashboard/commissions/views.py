import logging
import logging
import sys
from collections import OrderedDict
from datetime import datetime, timedelta

import django.forms as forms
from django.conf import settings
from django.contrib import messages
from django.db.models import Q
from django.db.models.aggregates import Sum
from django.db.models.functions import TruncMonth
from django.http import HttpResponseRedirect, QueryDict
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import ListView, UpdateView, TemplateView, DetailView
from django_tables2 import RequestConfig

import commissions.forms as commission_forms
import commissions.utils as commission_utils
import core.forms as core_forms
import core.utils as core_utils
import orders.utils as order_utils
from commissions.filters import CommissionListFilter, CommissionPaymentHistoryFilter
from core.mixins import PermissionRequiredMixin
from orders.models import Order
from orders.tables import SalesByAssociateWithBonusTable, CommissionsTable, SalesByAssocSalesTable, \
    ProtectionPlanBonusTable, ProtectionPlanByAssociateDetailsTable
from .models import Commission

logger = logging.getLogger('furnicloud')


class CommissionsList(PermissionRequiredMixin, TemplateView):
    model = Commission
    template_name = "commissions/commissions_list.html"
    cutoff_dt = datetime(2017, 5, 1)  # last 6 months
    cutoff_dt = timezone.make_aware(cutoff_dt, timezone.get_current_timezone())
    context_filter_name = 'filter'
    filter_class = CommissionListFilter
    filter_form_id = 'commissions-filter'

    required_permissions = (
        'commissions.view_commissions',
    )

    def get_queryset(self):
        qs = Commission.objects.all()
        qs = qs \
            .select_related('order') \
            .select_related('associate') \
            .prefetch_related('order__customer') \
            .prefetch_related('order__ordered_items') \
            .prefetch_related('order__payments') \
            .prefetch_related('order__orderdelivery_set') \
            .prefetch_related('order__commissions') \
            .filter(~Q(order__status__in=('QUOT', 'X', 'I', 'E'))) \
            .filter(~Q(order__order_type__in=('cabinetry', 'cabinetry_quote'))) \
            .distinct()

        # .filter(order__order_date__gt=self.cutoff_dt)
        self.setup_filter(queryset=qs)
        return self.filter.qs

    def setup_filter(self, **kwargs):
        # Create a mutable QueryDict object, default is immutable
        initial = QueryDict(mutable=True)
        initial['order_date_from'] = core_utils.get_date_n_months_ago(36)
        initial['order_date_to'] = core_utils.get_month_last_day()

        # if no filters are present, try to get filters stored in session

        initial.update(self.request.GET)
        if 'order_date_from' not in self.request.GET:
            # restore filters from session
            initial.update(self.request.session.get('commission_filters', QueryDict()))

        self.filter = self.filter_class(initial, queryset=kwargs['queryset'])

        # save selected filters to session if Filter button was pressed
        if 'order_date_from' in self.request.GET:
            self.request.session['commission_filters'] = self.filter.data

        self.filter.helper.form_id = self.filter_form_id
        self.filter.helper.form_method = "get"

    def get_context_data(self, **kwargs):
        context = super(CommissionsList, self).get_context_data(**kwargs)

        is_manager = self.request.user.groups.filter(Q(name="managers")).exists()
        commissions_data = []
        commission_subtotals = {}

        for c in self.get_queryset():
            order = c.order
            sales_amount = c.sale_portion
            if c.manager_commission_flag:
                # manager commission applies to whole order, not partial
                sales_amount = order.subtotal_after_discount

            associate = c.associate.first_name

            claim_action = 'Ineligible (check order)'
            if c.status in ('PAID', 'INELIGIBLE') and not is_manager:
                claim_action = ''
            elif c.status in ('PENDING', 'INELIGIBLE') or is_manager:
                claim_action = reverse("commission_edit", kwargs={"pk": c.pk})
                claim_action = mark_safe('<a href="{0}" class="button">{1}</a>'.format(claim_action, 'Edit'))
            elif c.is_eligible_for_claim:
                if c.associate == self.request.user or self.request.user.username == 'admin':
                    claim_action = reverse("commission_claim", kwargs={"pk": c.pk})
                    claim_action = mark_safe('<a href="{0}" class="button">{1}</a>'.format(claim_action, 'Claim'))
                else:
                    claim_action = 'To be claimed by %s' % associate

            commission_status = c.get_status_display().upper()
            if c.status == 'NEW':
                commission_status = 'UNCLAIMED'

            cur_commission = {
                'order': order,
                'order_date': order.order_date,  # .strftime(settings.DATE_FORMAT_STANDARD),
                'delivered_date': order.delivered_date,
                'order_status': order.get_status_display(),
                'order_subtotal': order.subtotal_after_discount,
                'sale_portion': sales_amount,
                'order_grandtotal': order.grand_total,
                'order_balance_due': order.balance_due,
                'associate': associate,
                # 'estimated_amount': c.amount,
                'amount': c.amount,
                'paid_date': c.paid_date,
                # 'commission_pk': c.pk,
                'commission_status': commission_status,
                'claim_action': claim_action,
                'customer': order.customer,
                'manager_commission_flag': c.manager_commission_flag,
            }
            commissions_data.append(cur_commission)

            # create table for commission subtotals by associate
            if not associate in commission_subtotals:
                commission_subtotals[associate] = {'PENDING': 0.0,
                                                   'PAID': 0.0,
                                                   'INELIGIBLE': 0.0,
                                                   'NEW': 0.0,
                                                   'SALES_SUBTOTAL': 0.0,
                                                   'ESTIMATED_COMMISSIONS': 0.0}

            if c.status.lower() == 'pending' or c.status.lower() == 'paid' or c.manager_commission_flag:
                commission_subtotals[associate][c.status] += c.amount
                commission_subtotals[associate]['SALES_SUBTOTAL'] += sales_amount
                estimated_commission = sales_amount * settings.COMMISSION_PERCENT
                if c.associate.username in 'greg':
                    if c.manager_commission_flag:
                        estimated_commission = order.subtotal_after_discount * 0.01
                    else:
                        estimated_commission = 0.0
                commission_subtotals[associate]['ESTIMATED_COMMISSIONS'] += estimated_commission

        commissions_table = CommissionsTable(commissions_data)
        RequestConfig(self.request).configure(commissions_table)
        context['table'] = commissions_table

        commission_subtotals = OrderedDict(
            sorted(commission_subtotals.items(), key=lambda row: row[1]['SALES_SUBTOTAL']))
        context['subtotals_table'] = commission_subtotals

        context['title'] = 'Commissions'
        context['message'] = '* \'Dummy\' and \'Quotation\' orders are excluded from this list'
        context[self.context_filter_name] = self.filter

        return context


class ManagerCommissionsList(CommissionsList):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.username in ('greg', 'dmitriy', 'admin'):
            redirect(reverse('login'))


class CommissionPaymentHistoryView(PermissionRequiredMixin, TemplateView):
    model = Commission
    template_name = "commissions/commissions_list.html"
    context_filter_name = 'filter'
    filter_class = CommissionPaymentHistoryFilter
    filter_form_id = 'commissions-filter'

    required_permissions = (
        'commissions.view_commissions',
    )

    def get_queryset(self):

        qs = Commission.objects.regular()

        # if self.request and (self.request.user.username in ('greg', 'admin', 'dmitriy')):
        #    qs = Commission.objects.all()

        qs = qs \
            .select_related('order') \
            .select_related('associate') \
            .prefetch_related('order__customer') \
            .prefetch_related('order__ordered_items') \
            .prefetch_related('order__payments') \
            .prefetch_related('order__orderdelivery_set') \
            .prefetch_related('order__commissions') \
            .filter(status='PAID') \
            .filter(~Q(order__status__in=('QUOT', 'X', 'I'))) \
            .filter(~Q(order__order_type__in=('cabinetry', 'cabinetry_quote'))) \
            # .filter(order__order_date__gt=self.cutoff_dt)

        self.setup_filter(queryset=qs)
        return self.filter.qs

    def setup_filter(self, **kwargs):
        # Create a mutable QueryDict object, default is immutable
        initial = QueryDict(mutable=True)
        from_date, to_date = core_utils.get_current_month_date_range()

        from_date = datetime.combine(from_date, datetime.min.time())
        to_date = datetime.combine(to_date, datetime.min.time())

        initial['commissions_paid_date_from'] = core_utils.date_with_tz(from_date)
        initial['commissions_paid_date_to'] = core_utils.date_with_tz(to_date)

        # if no filters are present, try to get filters stored in session
        # if self.request.GET.get('submit', '') != u'Filter':
        #     initial.update(self.request.session.get('commission_filters', QueryDict()))

        initial.update(self.request.GET)

        self.filter = self.filter_class(initial, queryset=kwargs['queryset'])

        # save selected filters to session
        # if self.filter.data.get('submit', '') == u'Filter':
        #     self.request.session['commission_filters'] = self.filter.data

        # self.filter.helper = self.formhelper_class()
        self.filter.helper.form_id = self.filter_form_id
        # self.filter.helper.form_class = "blueForms, well"
        self.filter.helper.form_method = "get"

    def get_context_data(self, **kwargs):
        context = super(CommissionPaymentHistoryView, self).get_context_data(**kwargs)
        # commissions = context[self.context_object_name]

        # is_manager = self.request.user.groups.filter(Q(name="managers")).exists()
        commissions_data = []
        commission_subtotals = {}

        for c in self.get_queryset():
            order = c.order
            sales_amount = c.sale_portion
            if c.manager_commission_flag:
                # manager commission applies to whole order, not partial
                sales_amount = order.subtotal_after_discount

            associate = c.associate.first_name

            claim_action = ''

            commission_status = c.get_status_display().upper()
            if c.status == 'NEW':
                commission_status = 'UNCLAIMED'

            cur_commission = {
                'order': order,
                'order_date': order.order_date,  # .strftime(settings.DATE_FORMAT_STANDARD),
                'delivered_date': order.delivered_date,
                'order_status': order.get_status_display(),
                'order_subtotal': order.subtotal_after_discount,
                'sale_portion': sales_amount,
                'order_grandtotal': order.grand_total,
                'order_balance_due': order.balance_due,
                'associate': associate,
                'amount': c.amount,
                'paid_date': c.paid_date,
                'commission_status': commission_status,
                'claim_action': claim_action,
                'customer': order.customer or 'N/A',
                'manager_commission_flag': c.manager_commission_flag,
            }
            commissions_data.append(cur_commission)

            # create table for commission subtotals by associate
            if not associate in commission_subtotals:
                commission_subtotals[associate] = {'PENDING': 0.0,
                                                   'PAID': 0.0,
                                                   'SALES_SUBTOTAL': 0.0,
                                                   'ESTIMATED_COMMISSIONS': 0.0}

            if c.status.lower() == 'pending' or c.status.lower() == 'paid':
                commission_subtotals[associate][c.status] += c.amount
                commission_subtotals[associate]['SALES_SUBTOTAL'] += sales_amount
                commission_subtotals[associate]['ESTIMATED_COMMISSIONS'] += sales_amount * settings.COMMISSION_PERCENT

        commissions_table = CommissionsTable(commissions_data)
        RequestConfig(self.request).configure(commissions_table)
        context['table'] = commissions_table

        commission_subtotals = OrderedDict(
            sorted(commission_subtotals.items(), key=lambda row: int(row[1]['SALES_SUBTOTAL'])))
        context['subtotals_table'] = commission_subtotals

        context['title'] = 'Commissions Payment History'
        context['message'] = '* \'Dummy\' and \'Quotation\' orders are excluded from this list'
        context[self.context_filter_name] = self.filter

        return context


class CommissionUpdateView(PermissionRequiredMixin, UpdateView):
    """
    Commission Update view.
    """

    model = Commission
    context_object_name = "commission"
    template_name = "commissions/commission_update.html"
    form_class = commission_forms.ClaimCommissionForm
    initial = {'amount': 0}
    action = 'update_action'

    required_permissions = (
        # 'commissions.edit_commissions',
        'commissions.view_commissions',
    )

    def dispatch(self, request, *args, **kwargs):

        try:
            self.action = kwargs.get('action')
            logger.debug('Commission Update Action: %s' % self.action)
            commission = self.get_object()
            if self.action == 'claim_action':
                if not commission.is_eligible_for_claim:
                    logger.debug('Claim %d not eligible to be claimed' % commission.pk)
                    messages.error(
                        request,
                        'Commission for %s, order #%s is not eligible for claim at this time.' % (
                            commission.associate.first_name, commission.order.number),
                        extra_tags="alert alert-danger"
                    )
                    messages.error(
                        request,
                        '1) Order status must be \'Delivered\' or \'Closed\'; '
                        '2) Payment history for order must be entered.',
                        extra_tags="alert alert-danger"
                    )
                    return redirect(reverse('commissions_list'))
                else:

                    extra_tags = "alert alert-danger"
                    proceed = True
                    msg = []

                    # check if everything else is complete for order:
                    # ------------------------------------------
                    # if order has financing plan
                    if commission.order.financing_option:
                        # if financing info detail was not entered, no commission
                        if commission.order.orderfinancing_set.count() == 0:
                            msg.append('FINANCING details are not entered')
                            proceed = False

                    # if guardian protection plan was selected but no info was entered
                    if commission.order.protection_plan:
                        if commission.order.orderitemprotectionplan_set.count() == 0:
                            msg.append('GUARDIAN protection plan info not entered')
                            proceed = False

                    if commission.status != 'NEW':
                        logger.debug('Claim %d cannot be reclaimed, commission status: %s' % (
                            commission.pk, commission.get_status_display()))
                        msg.append('Commission for %s, order #%s cannot be re-claimed, it was already claimed earlier. ' \
                                   'Commission current status: %s.' % (
                                       commission.associate.first_name, commission.order.number,
                                       commission.get_status_display()))
                        proceed = False

                    if not proceed:
                        error_msg = "Commission for %s, order #%s cannot be claimed for the reasons below: " \
                                    % (commission.associate.first_name, commission.order.number)
                        for m in msg:
                            error_msg += "<br/> - " + m
                        messages.add_message(request, messages.ERROR, mark_safe(error_msg), extra_tags)
                        return redirect(reverse('commissions_list'))

            else:
                if not commission_utils.can_edit_commission(request, commission):
                    logger.debug(
                        'User %s doesn\'t have privilege to edit commission %d' % (request.user, commission.pk))
                    messages.error(
                        request,
                        'You don\'t have privilege to edit other associate\'s commission (order %s). Please contact manager.' % (
                            commission.order.number),
                        extra_tags="alert alert-danger"
                    )
                    return redirect(reverse('commissions_list'))
                else:
                    self.form_class = commission_forms.CommissionEditForm
        except KeyError as e:
            logger.debug('Action not available %s' % str(e))

        return super(CommissionUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """
        Handle GET requests and instantiate blank version of the form
        """

        self.object = self.get_object()

        context = self.get_context_data()

        self.initial['amount'] = self.object.amount
        amount_auto_populated = False
        if self.object.amount < 0.1:
            self.initial['amount'] = context['estimated_commission']
            amount_auto_populated = True

        date_auto_populated = False
        # if self.action != 'claim_action':
        #     if self.object.paid_date is None:
        #         self.initial['paid_date'] = datetime.now()
        #         date_auto_populated = True

        form_class = self.get_form_class()
        form = form_class(request=request, **self.get_form_kwargs())

        if self.action == 'claim_action':
            form.fields['status'].widget = forms.HiddenInput()
            form.fields['amount'].widget.attrs['class'] = "autopopulated"
        else:
            if date_auto_populated:
                form.fields['paid_date'].widget.attrs['class'] = "autopopulated"

        extra_forms = {
            'form': form,
        }

        context.update(extra_forms)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiates a form instance with the passed POST variables
        and then checks for validity
        """

        self.object = self.get_object()
        form_class = self.get_form_class()
        form = form_class(request=request, **self.get_form_kwargs())

        if self.action == 'claim_action':
            form.fields['status'].widget = forms.HiddenInput()

        extra_forms = {
            'form': form,
        }

        if form.is_valid():
            return self.form_valid(**extra_forms)
        else:
            messages.add_message(self.request, messages.ERROR, "Error saving Commission information",
                                 extra_tags="alert alert-danger")
            return self.form_invalid(**extra_forms)

    def form_valid(self, *args, **kwargs):
        """
        Called if all forms are valid. Updates a commission instance with associated info
        and then redirects to a success page
        """
        form = kwargs['form']
        self.object = form.save(commit=False)

        # if self.action == 'edit_action':
        #     if form.cleaned_data['paid'] == True:
        #         self.object.status = 'PAID'
        # elif self.action == 'claim_action':
        if self.action == 'claim_action':
            self.object.status = 'PENDING'

        # save commission
        self.object.save()

        status_msg = "Commission status changed to %s" % self.object.get_status_display()
        messages.add_message(self.request, messages.SUCCESS, status_msg,
                             extra_tags="alert alert-success")

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(CommissionUpdateView, self).get_context_data(**kwargs)
        com = context[self.context_object_name]
        comm_data = order_utils.calc_commissions_for_order(com.order)
        estimated_commission = sum(
            [float(c['estimated_commissions']) for c in comm_data if c['associate'] == com.associate.first_name])
        context['estimated_commission'] = round(estimated_commission, 2)
        context['action'] = self.action
        return context

    def get_success_url(self):
        return reverse('commissions_list')

    def form_invalid(self, *args, **kwargs):
        """
        Called if form is invalid. Returns a response with an invalid form in the context
        """
        context = self.get_context_data()
        context.update(kwargs)
        return self.render_to_response(context)

    def get_form_class(self):
        if self.action == 'claim_action':
            return commission_forms.ClaimCommissionForm
        else:
            return commission_forms.CommissionEditForm


class CommissionDetailView(PermissionRequiredMixin, DetailView):
    model = Commission
    context_object_name = "commission"
    template_name = "commissions/commission_detail.html"

    required_permissions = (
        'commissions.view_commissions',
    )


class BonusMonthlyReportView(PermissionRequiredMixin, ListView):
    model = Order
    context_object_name = "bonus_list"
    template_name = "commissions/bonus_report.html"
    from_date = ""
    to_date = ""
    selected_month = datetime.now().month
    selected_year = datetime.now().year
    cutoff_dt = timezone.make_aware(datetime(2015, 2, 1))

    required_permissions = (
        'orders.view_sales',
    )

    def get_queryset(self, **kwargs):
        try:
            month = int(self.request.GET['month'])
            year = int(self.request.GET['year'])
            self.from_date, self.to_date = core_utils.get_month_date_range(month, year)
            self.selected_month = month
            self.selected_year = year
        except (KeyError, TypeError) as e:
            self.from_date, self.to_date = core_utils.get_current_month_date_range()

        lookup_kwargs = {
            '%s__gte' % 'bonus_elig_date': self.from_date,
            '%s__lte' % 'bonus_elig_date': self.to_date,
        }
        qs = Order.objects.get_qs().filter(**lookup_kwargs)

        return qs

    def get_context_data(self, **kwargs):
        context = super(BonusMonthlyReportView, self).get_context_data(**kwargs)

        initial = {'month': self.selected_month, 'year': self.selected_year}
        # params = initial.update(self.request.GET)
        context['date_range_filter'] = core_forms.MonthYearFilterForm(initial)

        orders = context[self.context_object_name]

        attrs = {'old_bonus': False}
        if self.from_date < self.cutoff_dt:  # new bonus start date - 02/01/2015!!!
            attrs['old_bonus'] = True

        # Get Sales and Bonus information for orders
        sales_by_assoc_data, sales_by_assoc_expanded_data = order_utils.get_sales_data_from_orders(orders, **attrs)
        sales_by_assoc = SalesByAssociateWithBonusTable(sales_by_assoc_data)

        RequestConfig(self.request, paginate=False).configure(sales_by_assoc)
        context['sales_by_associate'] = sales_by_assoc

        # sales eligible for Bonus in selected date range
        bonus_elig_orders_by_assoc = {}
        count = 1
        for associate_name, sales in sales_by_assoc_expanded_data.items():
            bonus_elig_orders_by_assoc[associate_name] = SalesByAssocSalesTable(sales, prefix="tbl" + str(count))
            bonus_elig_orders_by_assoc[associate_name].exclude = (
            'estimated_commissions', 'claimed_commissions', 'commission_status')

            RequestConfig(self.request).configure(bonus_elig_orders_by_assoc[associate_name])
            count += 1
        context['bonus_elig_orders_by_associate'] = bonus_elig_orders_by_assoc

        # summary table for Guardian Protection Plan bonus
        sales_by_assoc_data, sales_by_assoc_expanded_data = order_utils.get_protection_plan_bonus(self.from_date,
                                                                                                  self.to_date)
        protection_plan_table = ProtectionPlanBonusTable(sales_by_assoc_data)
        RequestConfig(self.request, paginate=False).configure(protection_plan_table)
        context['protection_plan_bonus_summary_table'] = protection_plan_table

        # prepare expanded sales tables per each associate
        protection_plan_breakdown_by_associate = {}
        count = 1
        for associate_name, sales in sales_by_assoc_expanded_data.items():
            # include only sold orders that have protection plan
            sales = filter(lambda rec: rec.get('protection_plan_bonus', 0.0) > 0.0, sales)
            protection_plan_breakdown_by_associate[associate_name] = ProtectionPlanByAssociateDetailsTable(sales,
                                                                                                           prefix="tbl" + str(
                                                                                                               count))
            RequestConfig(self.request).configure(protection_plan_breakdown_by_associate[associate_name])
            count += 1
        context['protection_plan_breakdown_by_associate'] = protection_plan_breakdown_by_associate

        return context


def get_sales_data_for_period(from_date, to_date):
    lookup_kwargs = {
        '%s__gte' % 'order_date': from_date,
        '%s__lt' % 'order_date': to_date,
        '%s__in' % 'order_type': ['stock', 'special_order'],
    }
    # month_portion = u"CAST(DATE_FORMAT(CONVERT_TZ(order_date, 'UTC', 'America/Los_Angeles'), '%%m') AS CHAR)"

    qs = Order.objects \
        .exclude(status__in=('X', 'QUOT', 'E')) \
        .exclude(order_type__in=('cabinetry', 'cabinetry_quote')) \
        .filter(**lookup_kwargs) \
        .annotate(date=TruncMonth('order_date')) \
        .values('date') \
        .annotate(sales=Sum('subtotal_after_discount'))

    sac_sales = qs.filter(store__pk=1) \
        .order_by()
    fnt_sales = qs.filter(store__pk=2) \
        .order_by()

    sales_subtotals = {}
    for row in sac_sales:
        month_num = row['date'].month - 1
        sales_subtotals[month_num] = {'date': month_num, 'sac': row['sales'], 'fnt': 0.0}
    for row in fnt_sales:
        month_num = row['date'].month - 1
        try:
            sales_subtotals[month_num]['fnt'] = row['sales']
        except KeyError:
            sales_subtotals[month_num] = {'date': month_num, 'fnt': row['sales'], 'sac': 0.0}

    # sales_data = [('Month', 'Sac Store Sales', 'Fnt Store Sales')]
    sales_data = []
    for month in sales_subtotals.keys():
        row = sales_subtotals[month]
        total = row['sac'] + row['fnt']
        row['total'] = total
        sales_data.append(row)

    sales_data = sorted(sales_data, key=lambda k: k['date'])

    return sales_data


class SalesStatsView(PermissionRequiredMixin, TemplateView):
    required_permissions = (
        'commissions.view_commissions',
    )
    template_name = "commissions/stats.html"
    context_filter_name = 'filter'

    def get_context_data(self, **kwargs):
        context = super(SalesStatsView, self).get_context_data(**kwargs)

        tz = timezone.get_current_timezone()
        
        # Get the current year
        now = timezone.now()
        current_year = now.year

        sales_data = {}

        start_year = 2015
        for year in range(start_year, current_year):
            from_date = datetime(year, 1, 1)
            from_date = timezone.make_aware(from_date, tz)
            to_date = datetime(year, 12, 31)
            to_date = timezone.make_aware(to_date, tz)
            
            # Fetch sales data for the year
            sales_data[year] = get_sales_data_for_period(from_date, to_date)

        # Fetch data for the current year up to now + 1 day
        from_date = datetime(current_year, 1, 1)
        from_date = timezone.make_aware(from_date, tz)
        to_date = now + timedelta(days=1)
        sales_data[current_year] = get_sales_data_for_period(from_date, to_date)

        # Add the sales data to the context
        context['sales_data'] = sales_data

        return context
