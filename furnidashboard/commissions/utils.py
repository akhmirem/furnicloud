from django.contrib.auth import get_user_model


def can_edit_commission(request, commission):
    return request.user.has_perm('commissions.edit_commissions') or \
           (request.user == commission.associate and commission.status != 'PAID')


def get_manager_account():
    user_model = get_user_model()
    greg = user_model.objects.get(username='greg')
    return greg


def get_manager_commission_for_order(order):
    return order.commissions.all().filter(manager_commission_flag=True)

