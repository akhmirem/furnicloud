from bootstrap3_datepicker.widgets import DatePickerInput
# from bootstrap3_datetime.widgets import DateTimePicker
from django import forms
from django.forms import ModelForm

from orders.forms import CustomDatePickerInput
from .models import Commission

# DATEPICKER_OPTIONS = {"format": "YYYY-MM-DD", "pickTime": False}
DATEPICKER_OPTIONS = {"format": "%m/%d/%Y"}


class ClaimCommissionForm(ModelForm):

    # name = forms.CharField(
    #     max_length=Author._meta.get_field('name').max_length
    # )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ClaimCommissionForm, self).__init__(*args, **kwargs)
        self.fields['comments'].widget.attrs['rows'] = 3
        self.fields['status'].label = 'Commission Status'

    def save(self, commit=True):

        instance = super(ClaimCommissionForm, self).save(commit=False)

        if commit:
            # save
            instance.save(update_fields=['amount', 'comments'])

        return instance

    class Meta:
        model = Commission
        fields = ('amount', 'comments', 'status')
        widgets = {'associate': forms.HiddenInput()}


class CommissionEditForm(forms.ModelForm):

    class Meta:
        model = Commission
        fields = ('associate', 'paid', 'status', 'amount', 'paid_date', 'paid_date', 'comments') #"__all__"
        widgets = {'associate': forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(CommissionEditForm, self).__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            #self.fields['associate'].disabled = True #Django 1.9
            self.fields['associate'].required = True
            #self.fields['associate'].widget = forms.TextInput()
            self.fields['associate'].widget.attrs['readonly'] = True

        self.fields['associate'].label = 'Associate'

        self.fields['amount'].label = 'Commission Amount'

        self.fields['paid_date'].label = 'Commission Paid Date'
        # self.fields['paid_date'].widget = DateTimePicker(options=DATEPICKER_OPTIONS)
        # self.fields['paid_date'].widget = DatePickerInput(**DATEPICKER_OPTIONS)
        self.fields['paid_date'].widget = CustomDatePickerInput(**DATEPICKER_OPTIONS)
        #self.fields['paid_date'].widget.attrs['layout'] = 'inline'


        #user_model = get_user_model()
        #self.fields['associate'].queryset = user_model.objects.filter(
        #    Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers"))

        self.fields['comments'].widget.attrs['rows'] = 3

        if self.request and not self.request.user.has_perm('commissions.update_commissions_payment'):
            # person can modify only certain delivery info data
            self.fields['paid_date'].widget.attrs['readonly'] = 'true'
            self.fields['paid'].widget.attrs['readonly'] = 'true'
            self.fields['comments'].widget.attrs['readonly'] = 'true'
            self.fields['status'].widget.attrs['readonly'] = 'true'

        self.fields['status'].label = 'Commission Status'

    def save(self, commit=True):

        instance = super(CommissionEditForm, self).save(commit=False)

        if commit:
            # save
            instance.save() #update_fields=['associate', 'paid', 'paid_date', 'comments'])

        return instance

    def clean(self):
        self.cleaned_data = super(CommissionEditForm, self).clean()

        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            if self.request and not self.request.user.has_perm('commissions.update_commissions_payment'):
                self.cleaned_data['paid'] = instance.paid
                self.cleaned_data['paid_date'] = instance.paid_date
                self.cleaned_data['comments'] = instance.comments
                self.cleaned_data['status'] = instance.status
                self.cleaned_data['associate'] = instance.associate
        return self.cleaned_data

