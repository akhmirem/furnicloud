from audit_log.models.managers import AuditLog
from django.db.models import Q
from django.db import models
from django.conf import settings
from django_extensions.db.models import TimeStampedModel
from audit_log.models import AuthStampedModel

import orders
from orders.models import Order
import logging


logger = logging.getLogger('furnicloud')


class CommissionManager(models.Manager):

    def regular(self):
        return self.get_queryset()\
            .filter(manager_commission_flag=False)\
            .select_related('order')\
            .filter(~Q(order__order_type__in=('cabinetry', 'cabinetry_quote')))

    def active(self):
        return self.regular()\
            .select_related('order')\
            .filter(order__status__in=orders.models.Order.ORDER_ACTIVE_STATUSES)

    def all_commissions(self):
        return super(CommissionManager, self).get_queryset()


class Commission(TimeStampedModel, AuthStampedModel):
    COMMISSION_STATUES = (
        ('NEW', 'Unclaimed'),
        ('PENDING', 'Claimed'),
        ('INELIGIBLE', 'Ineligible/Rejected'),
        ('PAID', 'Paid'),
    )

    associate = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, blank=True,
                                  related_name='order_commissions', on_delete=models.CASCADE)
    order = models.ForeignKey(Order, related_name='commissions', on_delete=models.CASCADE)
    paid = models.BooleanField(default=False, blank=True)
    status = models.CharField(max_length=50, choices=COMMISSION_STATUES, default='NEW')
    amount = models.FloatField(blank=True, default=0.0)
    paid_date = models.DateTimeField(null=True, blank=True)
    comments = models.TextField(blank=True)
    manager_commission_flag = models.BooleanField(default=False, blank=True)
    portion_percentage = models.FloatField()

    def __str__(self):
        s = 'Commission: pk={0}, amount={1}, orderId={2}'.format(self.pk, self.amount, self.order_id)
        return s

    def __unicode__(self):
        return str(self)

    # track change history
    audit_log = AuditLog()

    objects = CommissionManager()

    @property
    def sale_portion(self):
        sales_amount = self.order.subtotal_after_discount * self.portion_percentage / 100.0
        return sales_amount

    @property
    def is_eligible_for_claim(self):
        """
        order is eligible for claim if
        - order is delivered or closed OR
        - order has in stock items and at least 1 delivery was entered
        """

        if self.order.status in ('D', 'C'):
            return True

        # get ordered items list
        # order_items = self.order.ordered_items.all()
        # # check if there is any "In Stock" item
        # if len(order_items) > 1 and any([i for i in order_items if i.status == 'S']):
        #     # check if Delivery has been entered for order
        #     if self.order.orderdelivery_set.count() > 0:
        #         return True

        return False

    class Meta:
        db_table = "commissions"
        permissions = (
            ("update_commissions_payment", "Can Update Commission Payment Information"),
            ("view_commissions", "View all commissions"),
            ("edit_commissions", "Change Commissions"),
        )


# @receiver(models.signals.post_save, sender=Order)
# def execute_after_save(sender, instance, created, *args, **kwargs):
#     manager = com_utils.get_manager_account()
#     com_amount = instance.subtotal_after_discount * 0.01
#
#     dt = datetime.strptime('2018-01-01', '%Y-%m-%d')
#     dt = timezone.make_aware(dt, timezone.get_current_timezone())
#     if instance.order_date >= dt:
#         if created or len(com_utils.get_manager_commission_for_order(instance)) == 0:
#             # upon order creation, need to create a commission for manager
#             logger.debug('Creating a manager commission for order ' + str(instance))
#             c = Commission.objects.create(order=instance, status='NEW', associate=manager,
#                                           manager_commission_flag=True,
#                                           amount=com_amount)
#             c.save()
#         else:
#             # when order is saved, update the manager's commission amount
#             logger.debug('Updating manager\'s commission for order %s' % instance)
#             c = Commission.objects.all().filter(order=instance, manager_commission_flag=True, associate=manager)
#             for cur_com in c:
#                 cur_com.amount = com_amount
#                 cur_com.save()
#
#
# @receiver(models.signals.pre_delete, sender=Commission)
# def execute_pre_delete(sender, instance, *args, **kwargs):
#     logger.debug("Commission Pre Delete")
#     instance.audit_log.disable_tracking()
#
#
# @receiver(models.signals.post_save, sender=Commission)
# def execute_after_com_save(sender, instance, created, *args, **kwargs):
#     if instance.manager_commission_flag:
#         return
#
#     manager = com_utils.get_manager_account()
#     com_amount = instance.order.subtotal_after_discount * 0.01
#
#     dt = datetime.strptime('2018-01-01', '%Y-%m-%d')
#     dt = timezone.make_aware(dt, timezone.get_current_timezone())
#     if instance.order.order_date >= dt:
#         if not created and \
#                 len([com for com in com_utils.get_commissions_for_order(instance.order) if com.status == 'NEW']) == 0:
#             # when order is saved, update the manager's commission amount
#             logger.debug('Updating manager\'s commission for order %s' % instance.order)
#             c = Commission.objects.all().filter(order=instance.order, manager_commission_flag=True, associate=manager)
#             for cur_com in c:
#                 cur_com.amount = com_amount
#                 cur_com.status = 'PENDING'
#                 cur_com.save()
