from django.conf import settings
from django.db import models
from associates.models import Associate
from orders.models import Order, OrderItem

VENDORS_LIST = (
    ('Natuzzi Italia', 'Natuzzi Italia'),
    ('Natuzzi Editions', 'Natuzzi Editions'),
    ('Natuzzi Re-Vive', 'Natuzzi Re-Vive'),
    ('ALF Italia', 'ALF Italia'),
    ('Bontempi', 'Bontempi'),
    ('Stressless', 'Stressless'),
    ('Cattelan', 'Cattelan'),
    ('Bauformat', 'Bauformat'),
    ('Miele', 'Miele'),
)


class IncomingShipment(models.Model):
    date = models.DateTimeField(null=False, blank=False)
    vendor = models.CharField(blank=True, null=True, max_length=50, choices=VENDORS_LIST)

    received_by = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, blank=True,
                                    related_name='incoming_shipments', on_delete=models.SET_NULL)
    num_boxes = models.IntegerField(blank=True, default=1)
    notes = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "incoming_shipments"


# class IncomingShipmentOrder(models.Model):
#     shipment = models.ForeignKey(IncomingShipment, related_name="orders")
#     po_num = models.CharField(max_length=25)
#     so_num = models.CharField(blank=True, null=True, max_length=25)
#
#     class Meta:
#         db_table = "incoming_shipment_orders"


class IncomingShipmentItem(models.Model):
    # 'pk', 'shipment', 'po_num', 'so_num', 'description', 'order_id', 'item_id'
    shipment = models.ForeignKey(IncomingShipment, related_name="items", on_delete=models.CASCADE)
    description = models.CharField(max_length=100)
    po_num = models.CharField(max_length=25)
    so_num = models.CharField(blank=True, null=True, max_length=25)
    order = models.ForeignKey(Order, related_name='incoming_shipment_items', blank=True, null=True, default=None, on_delete=models.SET_NULL)
    item = models.ForeignKey(OrderItem, related_name='incoming_shipments', blank=True, null=True, default=None, on_delete=models.SET_NULL)

    class Meta:
        db_table = "incoming_shipment_items"


class IncomingShipmentFile(models.Model):
    shipment = models.ForeignKey(IncomingShipment, related_name="files", on_delete=models.CASCADE)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, blank=True,
                              related_name='incoming_shipment_files', on_delete=models.SET_NULL)
    file = models.FileField(upload_to='warehouse/%Y')
    created = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=100, blank=True, null=True)

    @property
    def filename(self):
        import os
        return os.path.basename(self.file.name)

    def __unicode__(self):
        return self.description[:30]

    class Meta:
        db_table = "incoming_shipment_files"
