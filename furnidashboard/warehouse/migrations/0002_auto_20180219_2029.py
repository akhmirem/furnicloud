# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('warehouse', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='IncomingShipmentFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'warehouse/%Y')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('description', models.CharField(max_length=100)),
                ('owner', models.ForeignKey(related_name='incoming_shipment_files', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 'incoming_shipment_files',
            },
        ),
        migrations.RenameField(
            model_name='incomingshipment',
            old_name='description',
            new_name='notes',
        ),
        migrations.AddField(
            model_name='incomingshipmentfile',
            name='shipment',
            field=models.ForeignKey(related_name='files', to='warehouse.IncomingShipment', on_delete=models.CASCADE),
        ),
    ]
