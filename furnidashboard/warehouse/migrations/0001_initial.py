# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0013_auto_20171007_2248'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='IncomingShipment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField()),
                ('vendor', models.CharField(blank=True, max_length=50, null=True, choices=[(b'Natuzzi Italia', b'Natuzzi Italia'), (b'Natuzzi Editions', b'Natuzzi Editions'), (b'Natuzzi Re-Vive', b'Natuzzi Re-Vive'), (b'ALF Italia', b'ALF Italia'), (b'Bontempi', b'Bontempi'), (b'Stressless', b'Stressless'), (b'Cattelan', b'Cattelan'), (b'Bauformat', b'Bauformat'), (b'Miele', b'Miele')])),
                ('num_boxes', models.IntegerField(default=1, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('received_by', models.ForeignKey(related_name='incoming_shipments', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 'incoming_shipments',
            },
        ),
        migrations.CreateModel(
            name='IncomingShipmentItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=100)),
                ('po_num', models.CharField(max_length=25)),
                ('so_num', models.CharField(max_length=25, null=True, blank=True)),
                ('item', models.ForeignKey(related_name='incoming_shipments', default=None, blank=True, to='orders.OrderItem', null=True, on_delete=models.CASCADE)),
                ('order', models.ForeignKey(related_name='incoming_shipment_items', default=None, blank=True, to='orders.Order', null=True, on_delete=models.CASCADE)),
                ('shipment', models.ForeignKey(related_name='items', to='warehouse.IncomingShipment', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'incoming_shipment_items',
            },
        ),
    ]
