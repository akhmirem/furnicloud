# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('warehouse', '0002_auto_20180219_2029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='incomingshipmentfile',
            name='description',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
