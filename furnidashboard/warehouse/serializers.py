from orders.models import Order, OrderItem
from rest_framework.relations import PrimaryKeyRelatedField, StringRelatedField
from rest_framework import serializers
from warehouse.models import IncomingShipment, IncomingShipmentItem, IncomingShipmentFile
import logging

logger = logging.getLogger('furnicloud')


class WarehouseOrderItemSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='get_status_display')
    so_num = serializers.CharField(source='order.number', read_only=True)
    order_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = OrderItem
        fields = ('pk', 'order_id', 'so_num', 'status', 'description', 'po_num', 'po_date', 'ack_num', 'ack_date')


class WarehouseOrderSerializer(serializers.ModelSerializer):
    customer = StringRelatedField(read_only=True)
    store = StringRelatedField(read_only=True)
    status = serializers.CharField(source='get_status_display')

    class Meta:
        model = Order
        fields = ('pk', 'number', 'order_date', 'customer', 'status', 'store')

    def get_status(self, obj):
        return obj.get_status_display()


class IncomingShipmentItemSerializer(serializers.ModelSerializer):
    shipment = PrimaryKeyRelatedField(many=False, queryset=IncomingShipment.objects.all())
    order = WarehouseOrderSerializer(many=False, read_only=True)
    order_id = PrimaryKeyRelatedField(queryset=Order.objects.all(), source='order', required=False, allow_null=True)
    item = WarehouseOrderItemSerializer(many=False, read_only=True)
    item_id = PrimaryKeyRelatedField(queryset=OrderItem.objects.all(), source='item', required=False, allow_null=True)

    class Meta:
        model = IncomingShipmentItem
        fields = ('pk', 'shipment', 'po_num', 'so_num', 'description', 'order', 'order_id', 'item', 'item_id')


# class IncomingShipmentOrderSerializer(serializers.ModelSerializer):
#
#     shipment = PrimaryKeyRelatedField(many=False, queryset=IncomingShipment.objects.all())
#     items = IncomingShipmentItemSerializer(many=True, read_only=True, required=False)
#
#     class Meta:
#         model = IncomingShipmentOrder
#         fields = ('pk', 'shipment', 'po_num', 'so_num', 'items')


class IncomingShipmentFileSerializer(serializers.ModelSerializer):
    shipment = PrimaryKeyRelatedField(many=False,
                                      queryset=IncomingShipment.objects.all(),
                                      required=False)
    file = serializers.FileField(required=True)

    def save(self, **kwargs):
        shipment_pk = self.context['view'].kwargs['shipment_pk']
        logger.debug("In save method. Shipment_pk=" + shipment_pk)
        kwargs['shipment'] = IncomingShipment.objects.get(pk=shipment_pk)
        return super(IncomingShipmentFileSerializer, self).save(**kwargs)

    class Meta:
        model = IncomingShipmentFile
        # fields = ('pk', 'shipment', 'file', 'description', 'owner')
        read_only_fields = ('created', 'file', 'owner')


class IncomingShipmentSerializer(serializers.ModelSerializer):

    #received_by = PrimaryKeyRelatedField(many=False, read_only=True)
    items = IncomingShipmentItemSerializer(many=True, read_only=True)
    files = IncomingShipmentFileSerializer(many=True, read_only=True)

    # def create(self, validated_data):
    #     orders_data = validated_data.pop('orders')
    #     shipment = IncomingShipment.objects.create(**validated_data)
    #     for order_data in orders_data:
    #         items_data = order_data.pop('items')
    #         order = IncomingShipmentOrder.objects.create(shipment=shipment, **order_data)
    #         for item_data in items_data:
    #             IncomingShipmentItem.objects.create(order=order, **item_data)
    #     return shipment
    #
    # def update(self, instance, validated_data):
    #
    #     for attr, value in validated_data.items():
    #         setattr(instance, attr, value)
    #     instance.save()
    #
    #     return instance

    class Meta:
        model = IncomingShipment
        fields = ('pk', 'date', 'vendor', 'num_boxes', 'received_by', 'notes', 'items', 'files')
