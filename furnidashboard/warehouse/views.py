from rest_framework import viewsets, filters, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from orders.models import OrderItem
from warehouse.models import IncomingShipment, IncomingShipmentItem, IncomingShipmentFile
from warehouse.serializers import IncomingShipmentSerializer, \
    IncomingShipmentItemSerializer, WarehouseOrderItemSerializer, IncomingShipmentFileSerializer
import logging

logger = logging.getLogger('furnicloud')


class IncomingShipmentItemViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    queryset = IncomingShipmentItem.objects.all()
    serializer_class = IncomingShipmentItemSerializer


class IncomingShipmentViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    queryset = IncomingShipment.objects.all()
    serializer_class = IncomingShipmentSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    #filter_class = StoreTrafficEntryFilter
    # filter_fields = ('associate__first_name', 'order__number', 'paid')'


class SOItemsAPIView(APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        so_num = self.request.query_params.get('so_num', '')

        lookup_kwargs = {
            '%s__contains' % 'order__number': so_num,
            '%s__startswith' % 'order__number': 'SO-',
        }

        qs = OrderItem.objects.filter(**lookup_kwargs)\
            .select_related('order')\
            .order_by('-order__order_date')\
            .all()[:15]

        serializer = WarehouseOrderItemSerializer(qs, many=True)
        data = {
            'so_num': so_num,
            'results': serializer.data
        }
        response = Response(data, status=status.HTTP_200_OK)
        return response


class IncomingShipmentFileViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = IncomingShipmentFile.objects.all()
    serializer_class = IncomingShipmentFileSerializer
    parser_classes = (MultiPartParser, FormParser,FileUploadParser)

    def get_queryset(self):
        return IncomingShipmentFile.objects.filter(shipment=self.kwargs['shipment_pk'])

    def perform_create(self, serializer):
        logger.debug('Shipment API file upload -- current user: %s' % str(self.request.user))
        serializer.save(owner=self.request.user,
                        shipment=self.kwargs['shipment_pk'],
                        file=self.request.data.get('file'))
