(function (G, U) {

    let orderDate = new Date('2006-01-01');
    const orderType = $("#id_order_type").val().toLowerCase();

    const applyItemFormRules = function (form_container) {

        const today = new Date();

        form_container.find(".items-form:not(.processed)").each(function (index, f) {

            const $form = $(f);
            const $status = $form.find('select.order-item-status');
            const $po_num = $form.find("input.order-item-po");
            const $po_date = $form.find("input.order-item-po-date");
            const $ack_num = $form.find("input.order-item-ack-num");
            const $ack_date = $form.find("input.order-item-ack-date");
            const $eta = $form.find("input.order-item-eta");
            const $cost = $form.find("input.order-item-cost");
            const $price = $form.find("input.order-item-price");
            const $container_num = $form.find("input.order-item-container-num");
            const $tracking_date = $form.find("input.order-item-last-checked-date");
            const $tracking_comment = $form.find("textarea.order-item-tracking-desc");


            $("#id_ordered_items-" + index + "-po_date_picker")
                .data('datepicker')
                .setEndDate(today);
            $("#id_ordered_items-" + index + "-ack_date_picker")
                .data('datepicker')
                .setEndDate(today);
            $("#id_ordered_items-" + index + "-order_last_checked_date_picker")
                .data('datepicker')
                .setEndDate(today);

            function resetSpecialOrderControls(visible) {
                [$po_num, $po_date, $ack_num, $ack_date, $eta, $container_num, $tracking_date, $tracking_comment].forEach(function (control) {
                    if (visible) {
                        control.parents('.form-group').show();
                    }
                    else {
                        control.val("");
                        control.parents('.form-group').hide();
                    }
                });
            }

            $form.addClass('processed');

            if ($status.val() === "S") {
                resetSpecialOrderControls(false);
            } else {
                resetSpecialOrderControls(true);
            }

            /*$in_stock.change(function () {
                try {
                    if (this.checked) {
                        console.log("Item in stock");
                        resetSpecialOrderControls(false);
                        if ($status.val() !== "S" && $status.val() !== 'D') {
                            $status.val("S");                                 // select "In Stock"
                        }
                    } else {
                        $status.val("P");                                     // select "Unordered"
                        resetSpecialOrderControls(true);
                    }
                } catch (e) {
                    console.log("Failed to change 'In stock' checkbox value.")
                }
            });*/

            //processing of dependent fields once item status is changed
            $status.change(function () {
                if ($(this).val() === "S") {
                    resetSpecialOrderControls(false);             // hide special order related fields
                    /*try {
                        $in_stock.prop("checked", "checked");
                    } catch (e) {
                        $in_stock.val(true);
                    }*/
                } else {
                    if ($(this).val() !== "D") {
                        // $in_stock.removeAttr("checked");
                        resetSpecialOrderControls(true);
                    }
                }
            });

            if (orderType === "cabinetry") {

                console.log("Processing cabinetry order item fields");

                $form.find(".item-pricing").show();

                // for cabinetry order types, keep track of subtotal by adding up item prices
                $cost.change(function () {
                    console.log("updating cabinetry order subtotal cost");
                    refreshCabinetrySubtotals();
                    displayProfitForItem($form);
                });
                $price.change(function () {
                    console.log("updating cabinetry order subtotal price");
                    refreshCabinetrySubtotals();
                    displayProfitForItem($form);
                });

                $("#profit-container").show();

                displayProfitForItem($form);
            } else {
                // Profit is visible only for Cabinetry Orders
                $("#profit-container").hide();
                $form.find(".item-pricing").hide();
            }

        });
    };

    const displayProfitForItem = function ($form) {

        const profitAmount = getProfitForItem($form);
        const $profitContainer = $form.find(".profit");
        $profitContainer.text("Profit: $" + profitAmount.toFixed(2));

    };

    const getProfitForItem = function($itemForm) {
        // Update profit made for an item
        let price = $itemForm.find("input.order-item-price").val();
        let cost = $itemForm.find("input.order-item-cost").val();
        if (price) {
            price = parseFloat(price.replace(',', ''));
        }
        if (cost) {
            cost = parseFloat(cost.replace(',', ''));
        }
        let profitAmount = 0.0;

        if (!isNaN(price) && !isNaN(cost)) {
            profitAmount = price - cost;
        }

        return profitAmount;
    };

    const refreshCabinetrySubtotals = function () {
        const cabinetryOrderSubtotal = calculateCabinetryItemPriceSubtotal();
        $("#id_subtotal_after_discount").val(cabinetryOrderSubtotal);
        recalcOrderTotals();
    };

    //calc order total and balance due
    const recalcOrderTotals = function () {
        let $total = $("#order-total");
        let $balance_due = $("#balance-due");
        let $profit = $("#total-profit");

        let grand_total_amount = calculateGrandTotalAmount();
        console.log("Grand total " + grand_total_amount.toString());

        let deposit_balance = calculateDepositBalance();

        let due = grand_total_amount - deposit_balance;
        if (Math.abs(grand_total_amount - deposit_balance) < 0.01) {
            due = 0.0;
        }

        const profitAmount = calculateCabinetryItemTotalProfit();

        $total.text("$" + grand_total_amount.toFixed(2));
        $balance_due.text("$" + due.toFixed(2));
        $profit.text("$" + profitAmount.toFixed(2));

        return due.toFixed(2);

    };

    const calculateGrandTotalAmount = function () {
        let subtotal = $("#id_subtotal_after_discount").val();
        subtotal = parseFloat(subtotal.replace(',', ''));
        if (isNaN(subtotal)) {
            subtotal = 0.0;
        }

        let tax = $("#id_tax").val();
        tax = parseFloat(tax.replace(',', ''));
        if (isNaN(tax)) {
            tax = 0.0;
        }

        let shipping = $("#id_shipping").val();
        shipping = parseFloat(shipping.replace(',', ''));
        if (isNaN(shipping)) {
            shipping = 0.0;
        }
        return subtotal + tax + shipping;
    };

    const calculateCabinetryItemPriceSubtotal = function () {
        let itemPriceSubtotal = 0.0;
        $("#orderItemsTable").find('tr.items-form').each(function (index, elem) {
            const itemPriceElem = $(elem).find('#id_ordered_items-' + index + '-price');
            if (itemPriceElem.length > 0 && $(elem).css('display') !== 'none') {
                let itemPrice = itemPriceElem.val();
                itemPrice = parseFloat(itemPrice.replace(',', ''));
                if (!isNaN(itemPrice)) {
                    itemPriceSubtotal += itemPrice;
                }
            }
        });
        return itemPriceSubtotal;
    };

    const calculateCabinetryItemTotalProfit = function () {
        let profitSubtotal = 0.0;
        $("#orderItemsTable").find('tr.items-form').each(function (index, $elem) {
            profitSubtotal += getProfitForItem($($elem));
        });
        return profitSubtotal;
    };

    const calculateDepositBalance = function () {
        var deposit_balance = 0.0;
        $("#orderPaymentsTable").find('tr.payments_form').each(function (index, elem) {
            var paymentAmount = $(elem).find('#id_payments-' + index + '-amount');
            if (paymentAmount.length > 0 && $(elem).css('display') !== 'none') {
                var curDeposit = paymentAmount.val();
                curDeposit = parseFloat(curDeposit.replace(',', ''));
                if (!isNaN(curDeposit)) {
                    deposit_balance += curDeposit;
                }
            }
        });
        return deposit_balance;
    };

    const formOnSubmitCallback = function ($form, event) {

        var $errMsg = $('<div class="alert bg-danger"></div>');
        var $tabs = $('#tabs');

        var cur_order_status = $("#id_status").val();
        if (cur_order_status === 'QUOT' || cur_order_status === 'X') {
            //var grandTotal = calculateGrandTotalAmount();
            var deposit_balance = calculateDepositBalance();
            //if (grandTotal > 0 || deposit_balance > 0) {
            if (deposit_balance > 0) {

                event.preventDefault();
                $tabs.find('a[href="#payment-info"]').tab('show');
                $errMsg.text("This is a Quote/Dummy order, so please remove payment information!")
                    .prependTo($tabs);
                $tabs.find('a[href="#payment-info"]').tab('show');
                window.setTimeout(function () {
                    $errMsg.fadeOut('slow');
                }, 10000);
                window.setTimeout(function () {
                    var offset = $tabs.offset();
                    $('body,html').animate({scrollTop: (offset.top - 100)}, 300);
                }, 500);

                return false;
            }
        }

        var payment_bad = false;
        var totalDue = recalcOrderTotals();
        if ($.isNumeric(totalDue) && totalDue < 0) {
            console.log('Total Payment amount cannot be greater than Total Total!');
            payment_bad = true;
            err_msg = "Total payment amount cannot be greater than Order Total!";
        }

        if (!payment_bad) {
            var payment_balance = calculateDepositBalance();
            if ($.isNumeric(payment_balance) && payment_balance < 0) {
                console.log('Total Payment amount cannot be negative!');
                payment_bad = true;
                err_msg = "Total payment amount cannot be negative!";
            }
        }

        if (payment_bad) {

            event.preventDefault();

            $tabs.find('a[href="#payment-info"]').tab('show');

            $errMsg.text(err_msg)
                .prependTo($("#orderPaymentsTable_wrapper"));
            window.setTimeout(function () {
                $errMsg.fadeOut('slow');
            }, 10000);

            window.setTimeout(function () {
                var target = $("#id_payments-0-payment_date", $form).parents('.row');
                var offset = target.offset();
                $('body,html').animate({scrollTop: (offset.top - 100)}, 300);
            }, 500);

            return false;
        }

        /*var commissionPortionsTotal = calculateCommissionPortionsTotal();
        if (Math.abs(100.0 - commissionPortionsTotal) > 1) {
            event.preventDefault();

            $tabs.find('a[href="#order-commissions-fieldset"]').tab('show');

            $errMsg.text("Please enter valid sale portion percentage (for a total to be 100%).")
                .prependTo($("#orderCommissionsTable_wrapper"));
            window.setTimeout(function () {
                $errMsg.fadeOut('slow');
            }, 10000);

            window.setTimeout(function () {
                var target = $("#id_commissions-0-portion_percentage", $form).parents('.form-row');
                var offset = target.offset();
                $('body,html').animate({scrollTop: (offset.top - 100)}, 300);
            }, 500);

            return false;
        }*/

        $("#id_subtotal_after_discount :disabled").removeAttr('readonly');


        return true;
    };


    const calculateCommissionPortionsTotal = function () {
        var portionTotals = 0.0;
        $("#orderCommissionsTable").find('tr.commissions_form').each(function (index, elem) {
            let curPortionPercentageElem = $(elem).find('#id_commissions-' + index + '-portion_percentage');
            if (curPortionPercentageElem.length > 0 && $(elem).css('display') !== 'none') {
                let curPortionPercentage = parseFloat(curPortionPercentageElem.val());
                if (!isNaN(curPortionPercentage)) {
                    portionTotals += curPortionPercentage;
                }
            }
        });
        return portionTotals;
    };

    function resetFormValidator(formId) {
        try {
            // var validator = $(formId).validate();
            // validator.resetForm();
            $(formId).removeData('validator');
            $(formId).removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse(formId);
        } catch (err) {}
    }

    function applyOrderFormValidation($form) {
        try {
            if ($form.length < 1) {
                return;
            }
        } catch (err) {
            console.error("FurnFormHelp > applyOrderFormValidation -> $form object param is invalid");
        }

        resetFormValidator($form);

        $.validator.addMethod('totalPercentageCheck', function(value, element, params) {
            var commissionPortionsTotal = calculateCommissionPortionsTotal();
            return Math.abs(100.0 - commissionPortionsTotal) <= 1;
        }, "Total sales portion must add up to 100%.");

        const validationRules = {
            number: {
                required: true,
                regex: /^(SO|SR|DR)-([0-9]{5})$/
            },
            subtotal_after_discount: {positive_num: true},
            tax: {positive_num: true},
            shipping: {positive_num: true}
        };

        $("#orderCommissionsTable").find('tr.commissions_form').each(function (index, elem) {
            let curPortionPercentageElem = $(elem).find('#id_commissions-' + index + '-portion_percentage');
            let curCommissionAssociateElem = $(elem).find('#id_commissions-' + index + '-associate');
            validationRules[curPortionPercentageElem.attr('name')] = {
                required: true,
                totalPercentageCheck: [],
                min: 10,
            }
            validationRules[curCommissionAssociateElem.attr('name')] = {
                required: true
            }
        });


        $form.validate({
            rules: validationRules,
            messages: {
                number: {
                    regex: 'Please enter order number is the format: <"SO" or "SR" or "DR">-<5 digits>. Examples: SO-10009 or SR-31000 or DR-10005',
                    required: 'Order number is required'
                }
            },
            ignore: '#orderPaymentsTable .form-control',
            errorClass: "has-error help-block",
            errorPlacement: function (error, element) {
                error.appendTo(element.parents(".form-group"));
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (element) {
                element
                    .addClass('has-success control-label')
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
            }
        });

        const picker = $("#id_order_date_picker").data('datepicker');
        picker.setEndDate(new Date());

        $("#id_order_date").on('change', function () {
            if (Date.parse($(this).val())) {
                orderDate = new Date($(this).val().valueOf());
            }
        });

        const $orderStatus = $("#id_status")
        $orderStatus.on("change", function () {
            refreshPaymentFields();
        });

        $("#id_number").on('change', function (e) {
            const $store = $("#id_store");
            const number = $(this).val();
            const sac_pattern = /^(SO|SR|DR)-(1[0-9]{4})$/;
            const fnt_pattern = /^(SO|SR|DR)-(3[0-9]{4})$/;
            if (sac_pattern.test(number)) {
                $store.val("1");
            } else if (fnt_pattern.test(number)) {
                $store.val("2");
            } else {
                $store.val("");
            }

        });

        const $deliveredDate = $("#id_deliveries-0-delivered_date");
        $deliveredDate.on('blur', function () {
            if (Date.parse($(this).val())) {
                const text = "Mark this order as 'Delivered'?";
                if (confirm(text) === true &&  $orderStatus.val() !== "D") {
                    $orderStatus.val("D")
                }
            }
        })

    }

    function refreshPaymentFields() {
        var subtotalFields = ['id_subtotal_after_discount', 'id_tax', 'id_shipping'];
        var money_fields = [].concat(subtotalFields);

        var cur_order_status = $("#id_status").val();

        $("#orderPaymentsTable").find("tr.payments_form .form-control").each(function (index, elem) {

            var picker = $("#id_payments-" + index + "-payment_date_picker").data('datepicker');
            if (picker) {
                picker.setEndDate(new Date());
            }

            //var paymentAmountId = '#id_payments-' + index + '-amount';
            money_fields.push($(elem).attr("id"));
            var payment_fields_regex = /^(id_payments)-([0-9])-(payment_date|amount|payment_type)$/;
            if (payment_fields_regex.test($(elem).attr("id"))) {
                console.log($(elem).attr("id") + " is a required field");

                if (cur_order_status === 'QUOT' || cur_order_status === 'X') {
                    $(elem).removeAttr("required");
                } else {
                    $(elem).attr("required", "true");
                }
            }
        });


        $.each(money_fields, function (index, field) {

            const $paymentField = $("#" + field);
            // set payment info to "0.0" if empty
            $paymentField.off('.orderValidation');
            $paymentField.on('blur.orderValidation', function (e) {

                if (subtotalFields.indexOf($(this).attr('id')) > -1 && !$.isNumeric($(this).val())) {
                    $(this).val(0.0);
                }

                FurnFormHelper.recalcOrderTotals();
            });

        });

        if (orderType === "cabinetry") {
            $("#id_subtotal_after_discount").attr("readonly", "readonly");
        }

        FurnFormHelper.recalcOrderTotals();
    }


    function initCommissionForm($form) {
        $form.find('input').each(function(index, elem) {
            let elemId = $(elem).attr('id');
            if (/id_commissions-\d-status/.test(elemId)) {
                $(elem).val("NEW");
            }
            if (/id_commissions-\d-portion_percentage/.test(elemId)) {
                $(elem).on('change', function() {
                    $("#orderCommissionsTable").find("tr.commissions_form").each(function (index, elem) {
                        if (elemId !== "id_commissions-" + index + "-portion_percentage") {
                            $("#id_commissions-" + index + "-portion_percentage").valid();
                        }
                    });
                });
            }
        });
    }

    function initNotesForm() {
        $("#orderNotesTable").find("tr.notes_form").each(function (index, row) {

            let noteType = 'GENERAL';
            const noteContent = $(row).find('textarea[name="notes-' + index + '-content"]');
            $(row).find('input[name="notes-' + index + '-type"]').each((index, radio) => {
                if ($(radio).prop('checked')) {
                    noteType = $(radio).val();
                }
                $(radio).off('click').on('click', function () {
                    if ($(this).is(':checked') ) {
                        $(noteContent)
                            .css('background', $(this).val() === 'ALERT' ? 'yellow' : 'none');
                    }
                })
            })
            $(noteContent).css('background', noteType === 'ALERT' ? 'yellow' : 'none');
        });
    }

    // routine that runs once new formset is added to the form
    const initFormset = function ($form, prefix, caption) {
        const accordions = $form.parent().find('div.form-row');
        const form_ind = accordions.length - 1;
        $form.find("div.panel-heading .panel-title a").attr("href", "#collapse" + prefix + "-" + form_ind).text(caption + " " + (form_ind + 1));
        $form.find("div.panel-collapse").attr("id", "collapse" + prefix + "-" + form_ind);
        $form.parents('fieldset.accordion').find("div.panel-collapse").removeClass("in");
        $form.find("div.panel-heading a").click();
    };

    function setUpFormSets() {

        // items formset
        const itemsTable = $("#orderItemsTable");
        itemsTable.find('tr.form-row').formset({
            prefix: 'ordered_items',
            formCssClass: 'items-form',
            // formTemplate: $(trackingTable).find('tr.form-row').first(),
            addCssClass: 'btn btn-primary add-row float-right',
            addText: 'Add <span class="glyphicon glyphicon-plus"></span>',
            deleteCssClass: 'delete-row-tr',
            added: function ($form) {
                $form.removeClass("processed");

                // stripes
                if ($form[0].rowIndex % 2 === 0) {
                    $form.removeClass("odd").addClass('even');
                } else {
                    $form.removeClass("even").addClass('odd');
                }

                // Unbind event handlers
                $('input', $form).off('change');
                $('select', $form).off('change');
                $('.input-group-addon', $form).off('change');
                $('.input-group-addon', $form).off('click');
                $('.date', $form).each(function () {
                    $(this).find('span.input-group-addon').unbind('click');
                    $(this).removeData('datepicker');
                    $(this).datepicker(dateTimePickerOptions);
                });

                $('select', $form).val("");

                // set item as "special order" by default
                // var $general_fields = $form.find(".item-general-fields");
                // var $in_stock = $form.find('input.order-item-in-stock');
                var $status = $form.find('select.order-item-status');
                // $in_stock.removeAttr("checked");
                var $orderTypeElem = $("#id_order_type");
                var orderType = "";
                var defaultItemStatus;
                if ($orderTypeElem.length) {
                    orderType = $orderTypeElem.val();
                    if (orderType === 'stock') {
                        defaultItemStatus = "S";
                    } else {
                        defaultItemStatus = "P";
                    }
                }
                $status.val(defaultItemStatus);


                applyItemFormRules(itemsTable);

                console.log("Added item...");
            },
            removed: function ($form) {
                console.log("Removed item...");
            }
        });
        itemsTable.DataTable(dataTableOptions);

        // order delivery formset
        $("#deliveries").find("div.panel-collapse").eq(0).addClass("in");               //expand first item
        $('#deliveries div.delivery-form').formset({
            prefix: 'deliveries',
            added: function ($form) {
                initFormset($form, 'deliveries', 'Delivery');

                $('.input-group-addon', $form).off('change');
                $('.input-group-addon', $form).off('click');

                $('.date', $form).each(function () {
                    $(this).removeData('datepicker')
                    $(this).datepicker(dateTimePickerOptions);
                });
            }
        });

        // commissions formset
        var commissionsTable = $('#orderCommissionsTable');
        commissionsTable.find('tr.form-row').formset({
            prefix: 'commissions',
            formCssClass: 'commissions_form',
            addCssClass: 'btn btn-primary add-row float-right',
            addText: 'Add <span class="glyphicon glyphicon-plus"></span>',
            deleteCssClass: 'delete-row-tr',
            added: function ($form) {
                $('select', $form).val("");
                // refreshPaymentFields();
                initCommissionForm($form);
                applyOrderFormValidation($('#orderForm'));
            },
            removed: function ($form) {
                // refreshPaymentFields();
                // initCommissionForm();
                applyOrderFormValidation($('#orderForm'));
            }
        });
        commissionsTable.DataTable(dataTableOptions);

        // attachments formset
        $("#attachment-fieldset").find("div.panel-collapse:first").addClass("in"); //expand first item
        $('#attachment-fieldset div.attachment-form').formset({
            prefix: 'attachments',
            added: function ($form) {
                initFormset($form, 'attachments', 'Attachment');
            }
        });

        // issues formset
        $("#issues-fieldset").find("div.panel-collapse:first").addClass("in"); //expand first item
        $('#orderIssuesTable tr').formset({
            prefix: 'issues',
            addCssClass: 'btn btn-primary add-row',
            addText: 'Add <span class="glyphicon glyphicon-plus"></span>',
            deleteCssClass: 'delete-row-tr'
        });
        $('#orderIssuesTable').DataTable(dataTableOptions);


        //order payments table
        var paymentsTable = $('#orderPaymentsTable');
        paymentsTable.find('tr.form-row').formset({
            prefix: 'payments',
            formCssClass: 'payments_form',
            //formTemplate: $(paymentsTable).find('tr.form-row').first(),
            addCssClass: 'btn btn-primary add-row float-right',
            addText: 'Add <span class="glyphicon glyphicon-plus"></span>',
            deleteCssClass: 'delete-row-tr',
            added: function ($form) {
                //FurnFormHelper.initFormset($form, 'payments', 'Payment');
                $('.date', $form).each(function () {
                    $(this).find('span.input-group-addon').unbind('click');
                    $(this).removeData('datepicker');
                    $(this).datepicker(dateTimePickerOptions);
                });
                $('select', $form).val("");
                refreshPaymentFields();
            },
            removed: function ($form) {
                refreshPaymentFields();
            }
        });
        paymentsTable.DataTable(dataTableOptions);

        //order notes table
        $(".notes-fieldset").find("div.panel-collapse:first").addClass("in"); //expand first item
        $('.notes-fieldset tr').formset({
            prefix: 'notes',
            formCssClass: 'notes_form',
            addCssClass: 'btn btn-primary add-row',
            addText: 'Add new note <span class="glyphicon glyphicon-plus"></span>',
            deleteCssClass: 'delete-row-tr',
            added: function ($form) {
                $form.find('input[type="radio"]').each(function(index, elem) {
                    let elemId = $(elem).attr('id');
                    if (/id_notes-\d-type_\d/.test(elemId) && $(elem).val() === 'GENERAL') {
                        $(elem).prop("checked", "checked");
                    }
                });
                $("span.note-date-author", $form).remove();
                initNotesForm();
            },
        });
        $('#orderNotesTable').DataTable(dataTableOptions);
    }


    const dateTimePickerOptions = {
        format: "mm/dd/yyyy",
        todayHighlight: true,
        todayBtn: 'linked'
    };
    const dataTableOptions = {
        "paging": false,
        "ordering": false,
        "info": false,
        "bSort": false,
        "searching": false
        // ,"scrollX": true
    };


    G.FurnFormHelper = {
        //'initFormset' : initFormset,
        'calculateDepositBalance': calculateDepositBalance,
        'applyItemFormRules': applyItemFormRules,
        'recalcOrderTotals': recalcOrderTotals,
        'onSubmitCallback': formOnSubmitCallback,
        'applyOrderFormValidation': applyOrderFormValidation,
        'refreshPaymentFields': refreshPaymentFields,
        'setUpFormSets': setUpFormSets,
        'dateTimePickerOptions': dateTimePickerOptions,
        'initNotesForm': initNotesForm

    };

}(this, undefined));
