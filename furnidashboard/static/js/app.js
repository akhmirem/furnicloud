console.log("In App.JS...");

var from_date = '2016-10-01',
    to_date = '2016-12-31';

var settings = {
    'commission_statuses': [
        { id: 'NEW', status: 'Unclaimed' },
        { id: 'PENDING', status: 'Pending' },
        { id: 'INELIGIBLE', status: 'Ineligible/Rejected' },
        { id: 'PAID', status: 'Paid' }
    ]
};

var app = angular.module('app', ['ngResource', 'ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.cellNav', 'ui.grid.validate', 'ui.grid.pinning', 'addressFormatter']);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
}]);
app.config(['$resourceProvider', function($resourceProvider) {
    // Don't strip trailing slashes from calculated URLs
    $resourceProvider.defaults.stripTrailingSlashes = false;
}]);


angular.module('addressFormatter', []).filter('address', function () {
    return function (input) {
        return input.street + ', ' + input.city + ', ' + input.state + ', ' + input.zip;
    };
});

app.filter('commissionStatus', function() {
    var commissionStatuses = {
        'NEW': 'Unclaimed',
        'PENDING': 'Pending',
        'INELIGIBLE': 'Ineligible/Rejected',
        'PAID': 'Paid'
    };

    return function(input) {
        if (!input){
            return '';
        } else {
            return commissionStatuses[input];
        }
    };
});

app.factory('Commission', ['$resource', function ($resource) {
    return $resource('/api/commissions/:id/', {}, {
        'query': {method: 'GET', isArray: false},
        'update': {method: 'PUT'}
    });
}]);

app.controller('MainCtrl', ['$scope', '$http', '$q', '$interval', 'Commission', 'uiGridValidateService', function ($scope, $http, $q, $interval, Commission, uiGridValidateService) {

    $scope.msg = {
        type: '',
        text: ""
    };

    $scope.gridOptions = {};
    $scope.columns = [
        {name: 'id', displayName: 'ID', enableCellEdit: false, visible:false},
        {
            name: 'status',
            displayName: 'Status',
            cellFilter:'commissionStatus',
            editableCellTemplate: 'ui-grid/dropdownEditor',
            editDropdownValueLabel:'status',
            editDropdownOptionsArray: settings.commission_statuses,
            width:70
        },
        {
            name: 'associate_name',
            enableCellEdit: false,
            displayName: 'Associate',
            width:90
        },
        {name: 'associate', enableCellEdit: false, displayName: 'AssociateID', visible:false},
        {
            name: 'order_number',
            enableCellEdit: false,
            displayName: 'Order #',
            width:80
        },
        {
            name: 'order_date',
            enableCellEdit: false,
            displayName: 'Order Date',
            type: 'date',
            cellFilter: 'date:"yyyy-MM-dd"',
            width:95
        },
        {
            name: 'order_status',
            enableCellEdit: false,
            displayName: 'Order Status',
            width:110
        },
        {
            name: 'order_subtotal',
            enableCellEdit: false,
            displayName: 'Subtotal',
            cellTemplate: '<div>${{ row.entity.order_subtotal | number:2 }}</div>',
            width:85
        },
        {
            name: 'order_grand_total',
            enableCellEdit: false,
            displayName: 'Grand Total',
            cellTemplate: '<div>${{ row.entity.order_grand_total | number:2 }}</div>',
            width:110
        },
        {
            name: 'order_balance_due',
            enableCellEdit: false,
            displayName: 'Balance Due',
            cellTemplate: '<div>${{ row.entity.order_balance_due | number:2 }}</div>',
            width:110
        },
        {
            name: 'amount',
            displayName: 'Commission Amount',
            type: 'number',
            cellTemplate: '<div class="ui-grid-cell-contents" ng-class="{invalid:grid.validate.isInvalid(row.entity,col.colDef)}">${{ row.entity.amount | number:2 }}</div>',
            validators: {numericPositiveAmount:0},
            //cellTemplate: 'ui-grid/cellTitleValidator'
            /*cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
              if (grid.validate.isInvalid(row.entity,col.colDef)) {
                return 'invalid';
              }
            }*/
            width:100
        },
        {
            name: 'paid_date',
            displayName: 'Paid Date',
            type: 'date',
            cellFilter: 'date:"yyyy-MM-dd":"-0800"',
            validators: {paidDateRequired: 'abracadabra'},
            cellTemplate: 'ui-grid/cellTitleValidator',
            width:90
        },
        {
            name: 'comments',
            displayName: 'Comments',
            cellEditableCondition: function ($scope) {
                return true;//$scope.rowRenderIndex%2
            },
            width:300
        },
        {
            name: 'actions',
            displayName:'Actions',
            cellTemplate: '<button type="button" class="btn btn-small btn-success" ' +
            'ng-click="grid.appScope.markCommissionPending(row.entity, col.colDef)" ' +
            'ng-class="{hidden:row.entity.status != \'NEW\'}">' +
            'Claim</button>',
            pinnedRight:true,
            enableCellEdit:false,
            width: 110
        }
    ];
    $scope.gridOptions.columnDefs = $scope.columns;
    //$scope.gridOptions.rowEditWaitInterval = -1; // disable timer-based saving

    uiGridValidateService.setValidator('paidDateRequired',
        function (argument) {
            return function (oldValue, newValue, rowEntity, colDef) {
                /*if (!newValue) {
                    return true; // We should not test for existence here
                } else {
                    return newValue.startsWith(argument);
                }*/
                if (!newValue && rowEntity.status === 'PAID') {
                    console.log('failed');
                    return false; // paid date is missing
                } else {
                    console.log('pass');
                    return true;
                }
            };
        },
        function (argument) {
            return 'Validation failed ' + argument + '';
        }
    );
    uiGridValidateService.setValidator('numericPositiveAmount',
        function (argument) {
            return function (oldValue, newValue, rowEntity, colDef) {
                console.log('validating amount... new value=' + newValue);
                if (newValue == null) {
                    newValue = 0;
                    rowEntity.amount = 0.0;
                }
                return parseFloat(newValue) >= 0.00;
            };
        },
        function (argument) {
            return 'Amount field Validation failed ' + argument + '';
        }
    );



    $scope.saveRow = function (rowEntity) {

        var invalidRow = $scope.gridApi.validate.isInvalid(rowEntity, $scope.columns[10]);
        if (!invalidRow) invalidRow = $scope.gridApi.validate.isInvalid(rowEntity, $scope.columns[11]);
        if (invalidRow) {
            console.log('Validation failed. Abort Saving...');
            $scope.msg.type="error";
            $scope.msg.text="Invalid data entered: commission for Order " + rowEntity.order_number;

            var promise = $q.defer();
            $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );

            // fake a delay of 1 second and return error
            $interval( function() {
                promise.reject();
            }, 500, 1);

            return;
        }

        console.log("Saving...");


        /*var updatedRow = angular.copy(rowEntity);
        var paidDate = updatedRow.paid_date;
        if (paidDate != null) {
            console.log(typeof paidDate);
            var d = updatedRow.paid_date.getDate();
            var m = updatedRow.paid_date.getMonth();
            m += 1;  // JavaScript months are 0-11
            var y = updatedRow.paid_date.getFullYear();

            updatedRow.paid_date = y + '-' + m + '-' + d;
        }

        console.debug(updatedRow);*/

        console.debug(rowEntity);

        $scope.msg.type="warning";
        $scope.msg.text="Saving commission...";

        var $promise = Commission.update({id: rowEntity.id}, rowEntity).$promise;
        $scope.gridApi.rowEdit.setSavePromise(rowEntity, $promise);
        $promise.then(function(commission) {
            $scope.msg.type="success";
            $scope.msg.text="Commission for Order " + rowEntity.order_number + " was saved";
            $interval( function() {
                $scope.msg.text = '';
            }, 3000, 1);

        }, function(reason) {
            $scope.msg.type="error";
            $scope.msg.text="Failed to save commission for Order " + rowEntity.order_number + ". Reason:" + reason.toString();
        });
    };

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);

        gridApi.validate.on.validationFailed($scope,function(rowEntity, colDef, newValue, oldValue){
            console.error('rowEntity: '+ rowEntity + '\n' +
                          'colDef: ' + colDef + '\n' +
                          'newValue: ' + newValue + '\n' +
                          'oldValue: ' + oldValue);
        });
    };

    console.log("Starting...");

    $scope.filter = {
        'status': '',      // any
        'associate': '10', //Jenn
        'order_number': '',
        'date_range_from': from_date,
        'date_range_to': to_date
    };
    $scope.loadData = function() {
        $scope.msg.type="warning";
        $scope.msg.text="Loading commission data...";
        $scope.gridOptions.data = [];
        Commission.query({
            'associate': $scope.filter.associate,
            'status': $scope.filter.status,
            'order_number': $scope.filter.order_number,
            'order_date_range_0': $scope.filter.date_range_from,
            'order_date_range_1': $scope.filter.date_range_to
        }).$promise.then(function (res) {

            console.log("Raw data: ");
            console.log(res.results);
            data = res.results;
            for (i = 0; i < data.length; i++) {
                if (data[i].paid_date != null)
                    data[i].paid_date = new Date(data[i].paid_date);
                if (data[i].order_date != null)
                    data[i].order_date = new Date(data[i].order_date);
            }

            $interval( function() {
                $scope.gridOptions.data = data;
                $scope.msg.type="success";
                $scope.msg.text="";
            }, 3000, 1);

            console.log("Formatted Data:");
            console.log($scope.gridOptions.data);
        });
    };

    $scope.settings = settings;
    $scope.currentFocused = "";
    $scope.markCommissionPending = function(rowEntity, colDef){
        /*var rowCol = $scope.gridApi.cellNav.getFocusedCell();
        console.log('Selected row:');
        console.debug(rowCol);
        if(rowCol !== null) {
            $scope.currentFocused = 'Row Id:' + rowCol.row.entity.id + ' col:' + rowCol.col.colDef.name;
            console.log('Marking commission as Pending');
            rowCol.row.entity.status = 'PENDING';
            $interval( function() {
              $scope.gridApi.rowEdit.setRowsDirty([rowCol.row.entity]);
            }, 0, 1);
        }*/
        $scope.currentFocused = 'Row Id:' + rowEntity.id + ' col:' + colDef.name;
        console.log('Marking commission as Pending');
        rowEntity.status = 'PENDING';
        $interval( function() {
            $scope.msg.type="success";
            $scope.msg.text="";
            $scope.gridApi.rowEdit.setRowsDirty([rowEntity]);
        }, 0, 1);
    };

    $scope.loadData();

}])
;
