"""Development settings and globals."""

from __future__ import absolute_import

from os.path import join, normpath
from os import environ

from .base import *


# DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
#TEMPLATE_DEBUG = DEBUG
# END DEBUG CONFIGURATION


# EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# END EMAIL CONFIGURATION

ALLOWED_HOSTS = [
    'localhost',
    '127.0.0.1',
    'cloud-staging.furnitalia.com',
]

LOGGING['handlers']['applogfile']['filename'] = normpath(
    join(SITE_ROOT, 'logs/furnicloud.log'))


# DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': environ['dbname'],
        'HOST': environ['dbhost'] or 'localhost',
        'USER': environ['username'],
        'PASSWORD': environ['password'],
        'PORT': environ['port'],
        'TEST': {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": ":memory:",
            "USER": "",
            "PASSWORD": "",
            "HOST": "",
            "PORT": "",
        },
    }

}
# END DATABASE CONFIGURATION


# CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
# END CACHE CONFIGURATION


# TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
    'debug_toolbar',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TEMPLATE_CONTEXT': True,
}
# END TOOLBAR CONFIGURATION

# CORS CONFIGURATION
CORS_ORIGIN_ALLOW_ALL = True

# CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
)
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'sessionid',
)
CSRF_TRUSTED_ORIGINS = (
    'localhost',
)
# END CORS CONFIGURATION

STORE_TRAFFIC_URL = 'http://localhost:8080/store-traffic/'
WAREHOUSE_LOG_URL = 'http://localhost:4200/'
COMMISSIONS_URL = 'http://cloud-staging.furnitalia.com/app/'
SECRET_KEY = r"@)jv)^k=1f055u(0b@t$efrdmu(z0#+_z&dp&wkmsu*4*w2xkb"
