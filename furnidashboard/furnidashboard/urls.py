from datetime import date

import django
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include, path
from django.views.generic import TemplateView
from rest_framework_nested import routers

import claims.views as claims_views
import commissions.views as commissions_views
import commissions.views_rest as commissions_rest
import core
import core.views as core_views
import orders.views.non_furniture_orders_views as non_furniture_views
import orders.views.deliveries as deliveries_views
import orders.views.general as general_views
import orders.views.order_crud as order_crud_views
from commissions.views import BonusMonthlyReportView
from customers.views import CustomerCreateView, CustomerUpdateView, CustomerDetailView, CustomerTableView, \
    CustomerViewSet
from furnileads.views import webhook
from orders.views.api import OrderApiViewSet
from orders.views.reports import DeliveredOrdersTableView
from schedule.views import ScheduleViewSet
from store_traffic.views import StoreTrafficViewSet, StoreTrafficSummaryView, APIClientView
from warehouse.views import IncomingShipmentViewSet, IncomingShipmentItemViewSet, SOItemsAPIView, \
    IncomingShipmentFileViewSet

admin.autodiscover()

# router = routers.DefaultRouter()
# router.register(r'commissions', commssions_rest.CommissionViewSet)

urlpatterns = [

    # home page template
    url(
        regex=r'^$',
        view=general_views.HomePageRedirectView.as_view(permanent=False),
        name="home",
    ),
    url(
        regex=r"^dashboard/$",
        view=general_views.DashBoardView.as_view(),
        name="dashboard_url",
    ),
    # url(r'^$', TemplateView.as_view(template_name='base.html')),
    # url(r'^$', FormView.as_view(form_class=OrderForm, template_name = 'orders/form.html')),

    url(
        regex=r'^orders/$',
        view=general_views.OrderMonthArchiveTableView.as_view(month_format='%m',
                                                              year=str(
                                                                  date.today().year),
                                                              month=str(date.today().month)),
        name="order_list",
    ),
    # url(
    #     regex=r'^all-orders/$',
    #     view=general_views.AllOrdersTableView.as_view(),
    #     name="all_orders",
    # ),
    url(
        regex=r'^active-orders/$',
        view=general_views.ActiveOrdersTableView.as_view(),
        name="active_orders",
    ),
    # url(
    #    regex=r'^my-orders/$',
    #    view=general_views.MyOrderListView.as_view(),
    #    name="my_order_list",
    # ),
    url(
        regex=r'^orders/(?P<pk>\d+)/$',
        view=order_crud_views.OrderDetailView.as_view(),
        name="order_detail",
    ),
    url(
        regex=r'^orders/(?P<pk>\d+)/edit/$',
        view=order_crud_views.OrderUpdateView.as_view(),
        name="order_edit",
    ),
    url(
        regex=r'^orders/(?P<pk>\d+)/delete/$',
        view=login_required(order_crud_views.OrderDeleteView.as_view()),
        name="order_delete",
    ),
    url(r'orders/add/$', login_required(order_crud_views.OrderCreateView.as_view()),
        name="order_add"),
    url(r'orders/(?P<pk>\d+)/delete/$', login_required(order_crud_views.OrderDeleteView.as_view()),
        name="order_delete"),
    url(
        regex=r'^orders/(?P<pk>\d+)/history$',
        view=order_crud_views.OrderHistoryView.as_view(),
        name="order_history",
    ),
    url(
        regex=r'^orders/(?P<pk>\d+)/convert/$',
        view=order_crud_views.ConvertOrderView.as_view(),
        name="order_convert",
    ),

    # Archive Views
    # Month:
    url(
        # /2014/mar/
        regex=r'^orders/(?P<year>\d{4})/(?P<month>[a-zA-z]+)/$',
        view=general_views.OrderMonthArchiveTableView.as_view(),
        name="archive_month",
    ),
    url(
        # /2014/03/
        regex=r'^orders/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=general_views.OrderMonthArchiveTableView.as_view(
            month_format='%m'),
        name="archive_month_numeric",
    ),

    # Cabinetry Orders
    url(
        regex=r'^orders/cabinetry/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            year=str(date.today().year),
            month=str(date.today().month),
            order_types = ('cabinetry', 'cabinetry_quote'),
            title = "Kitchen & Bath Orders"),
        name="cabinetry_orders_list",
    ),
    url(
        # /2018/09/
        regex=r'^orders/cabinetry/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            order_types = ('cabinetry', 'cabinetry_quote'),
            title = "Kitchen & Bath Orders"),
        name="cabinetry_orders_month_list",
    ),

    # Appliances Orders
    url(
        regex=r'^orders/appliances/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            year=str(date.today().year),
            month=str(date.today().month),
            order_types = ['appliances'],
            title = "Appliances Orders"),
        name="appliances_orders_list",
    ),
    url(
        # /2018/09/
        regex=r'^orders/appliances/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            order_types = ['appliances'],
            title = "Appliances Orders"),
        name="appliances_orders_month_list",
    ),

    # Hunter Douglas Orders
    url(
        regex=r'^orders/windows/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            year=str(date.today().year),
            month=str(date.today().month),
            order_types = ['windows'],
            title = "Hunter Douglas Orders"),
        name="windows_orders_list",
    ),
    url(
        # /2018/09/
        regex=r'^orders/windows/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            order_types = ['windows'],
            title = "Hunter Douglas Orders"),
        name="windows_orders_month_list",
    ),

    # Flooring Orders
    url(
        regex=r'^orders/flooring/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            year=str(date.today().year),
            month=str(date.today().month),
            order_types = ['flooring'],
            title = "Flooring Orders"),
        name="flooring_orders_list",
    ),
    url(
        # /2018/09/
        regex=r'^orders/flooring/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            order_types = ['flooring'],
            title = "Flooring Orders"),
        name="flooring_orders_month_list",
    ),
    # Design Orders
    url(
        regex=r'^orders/design/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            year=str(date.today().year),
            month=str(date.today().month),
            order_types = ['design'],
            title = "Interior Design Orders"),
        name="design_orders_list",
    ),
    url(
        # /2018/09/
        regex=r'^orders/design/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=non_furniture_views
        .NonFurnitureOrderMonthArchiveTableView.as_view(
            month_format='%m',
            order_types = ['design'],
            title = "Interior Design Orders"),
        name="design_orders_month_list",
    ),


    # Delivered Orders report
    url(
        regex=r'^reports/delivered-orders/$',
        view=DeliveredOrdersTableView.as_view(),
        name="delivered_orders_report",
    ),
    url(
        # /2014/03/
        regex=r'^reports/delivered-orders/(?P<year>\d{4})/(?P<month>\d+)/$',
        view=DeliveredOrdersTableView.as_view(),
        name="delivered_orders_report_month",
    ),

    # Alerts pages
    url(
        regex=r'^alerts/$',
        view=core_views.UnplacedOrderTableView.as_view(),
        name="alerts_main",
    ),
    url(
        regex=r'^alerts/order-financing/$',
        view=core_views.OrderFinancingAlertTableView.as_view(),
        name="alerts_order_financing",
    ),
    url(
        regex=r'^alerts/uncollected-balance-orders/$',
        view=core_views.UncollectedBalanceAlertTableView.as_view(),
        name="open_balance_orders",
    ),
    url(
        regex=r'^alerts/missing-orders/$',
        view=core_views.MissedOrdersAlertTableView.as_view(),
        name="alerts_missing_orders",
    ),
    url(
        regex=r'^alerts/ready-for-delivery/$',
        view=core_views.ReadyForDeliveryOrdersAlertTableView.as_view(),
        name="alerts_orders_ready_for_delivery",
    ),
    url(
        regex=r'^alerts/special-orders/$',
        view=core_views.SpecialOrdersTableView.as_view(),
        name="alerts_special_orders",
    ),

    # Customer links
    url(
        regex=r'customers/$',
        view=CustomerTableView.as_view(),
        name="customer_list",
    ),
    url(
        regex=r'^customers/(?P<pk>\d+)/edit/$',
        view=CustomerUpdateView.as_view(),
        name="customer_edit",
    ),
    url(
        regex=r'^customers/add/$',
        view=CustomerCreateView.as_view(),
        name="customer_add",
    ),
    url(
        regex=r'^customers/(?P<pk>\d+)/$',
        view=CustomerDetailView.as_view(),
        name="customer_detail",
    ),

    # Sales Standings Report
    url(
        # /sales-standings/
        regex=r'^sales-standings/$',
        view=general_views.SalesStandingsMonthTableView.as_view(),
        name="sales_standings_cur",
    ),
    url(
        regex=r'^bonus-report/$',
        view=BonusMonthlyReportView.as_view(),
        name="bonus_report"
    ),
    url(
        regex=r'^sales-standings/cabinetry/$',
        view=general_views.CabinetrySalesStandingsMonthTableView.as_view(),
        name="sales_standings_cabinetry",
    ),


    url(r'^accounts/', include('django.contrib.auth.urls')),

    # search
    url(
        regex=r'^search/$',
        view=core.views.search,
        name="search",
    ),

    # deliveries
    url(
        regex=r'^deliveries/$',
        view=deliveries_views.DeliveriesTableView.as_view(),
        name="delivery_list",
    ),
    url(
        regex=r'^deliveries/(?P<pk>\d+)/$',
        view=deliveries_views.DeliveryDetailView.as_view(),
        name="delivery_detail",
    ),
    url(
        regex=r'^deliveries/(?P<pk>\d+)/edit/$',
        view=deliveries_views.DeliveryUpdateView.as_view(),
        name="delivery_edit",
    ),
    url(
        regex=r'^deliveries(?P<pk>\d+)/delete/$',
        view=deliveries_views.DeliveryDeleteView.as_view(),
        name="delivery_delete",
    ),

    # claims
    url(
        regex=r'^claims/$',
        view=claims_views.ClaimsTableView.as_view(),
        name="claim_list",
    ),
    url(
        regex=r'^claims/(?P<pk>\d+)/$',
        view=claims_views.ClaimDetailView.as_view(),
        name="claim_detail",
    ),
    url(
        regex=r'^claims/(?P<pk>\d+)/edit/$',
        view=claims_views.ClaimUpdateView.as_view(),
        name="claim_edit",
    ),
    url(
        regex=r'^claims(?P<pk>\d+)/delete/$',
        view=claims_views.ClaimDeleteView.as_view(),
        name="claim_delete",
    ),
    url(
        regex=r'claims/add/$',
        view=claims_views.ClaimCreateView.as_view(),
        name="claim_add"
    ),
    url(
        regex=r'^claims/(?P<claim_pk>\d+)/vendor-form/new/$',
        view=claims_views.VendorClaimRequestCreateView.as_view(),
        name="claim_vendor_form_add",
    ),
    url(
        regex=r'^claims/(?P<claim_pk>\d+)/vendor-form/(?P<pk>\d+)/delete/$',
        view=claims_views.VendorClaimRequestDeleteView.as_view(),
        name="claim_request_delete",
    ),
    url(
        regex=r'claims/print/$',
        view=claims_views.claim_print,
        name="claim_print"
    ),

    # Docs page
    url(
        regex=r'docs/$',
        view=login_required(TemplateView.as_view(
            template_name="misc/schedule.html",
            get_context_data=lambda: {'SCHEDULE_DOC_URL': settings.SCHEDULE_DOC_URL})),

        name="docs"
    ),
    url(
        regex=r'docs/numbers/$',
        view=login_required(TemplateView.as_view(
            template_name="misc/numbers.html",
            get_context_data=lambda: {'IMPORTANT_NUMBERS_URL': settings.IMPORTANT_NUMBERS_URL})),
        name="numbers"
    ),
    url(
        regex=r'docs/instructions/$',
        view=login_required(TemplateView.as_view(
            template_name="misc/instructions.html",
            get_context_data=lambda: {'INSTRUCTIONS_DOC_URL': settings.INSTRUCTIONS_DOC_URL})),
        name="instructions"
    ),

    url(
        regex=r'^stats/$',
        view=commissions_views.SalesStatsView.as_view(),
        name="stats",
    ),

    url(r'^ajax_select/', include('ajax_select.urls')),

    url(
        # unused
        regex=r'warehouse-log/$',
        view=login_required(TemplateView.as_view(
            template_name="misc/warehouse.html",
        )),
        name="warehouse"
    ),

    # administration URLs
    url(r'^admin/', admin.site.urls),

    # commissions
    url(
        regex=r'^commissions/$',
        view=commissions_views.CommissionsList.as_view(),
        name='commissions_list',
    ),
    url(
        regex=r'^commissions/paid',
        view=commissions_views.CommissionPaymentHistoryView.as_view(),
        name='commissions_paid',
    ),
    url(
        regex=r'^commissions-v2/$',
        view=TemplateView.as_view(
            template_name='commissions/commissions-v2.html',
            get_context_data=lambda: {
                'COMMISSIONS_URL': settings.COMMISSIONS_URL}
        ),
        name='commissions_list_v2',
    ),
    url(
        regex=r'^commissions/(?P<pk>\d+)/edit/$',
        view=commissions_views.CommissionUpdateView.as_view(),
        name="commission_edit",
        kwargs={'action': 'edit_action'},
    ),
    url(
        regex=r'^commissions/(?P<pk>\d+)/claim/$',
        view=commissions_views.CommissionUpdateView.as_view(),
        name="commission_claim",
        kwargs={'action': 'claim_action'},
    ),
    url(
        regex=r'^commissions/(?P<pk>\d+)/$',
        view=commissions_views.CommissionDetailView.as_view(),
        name="commission_detail",
    ),

    # Webhooks
    url(
        regex=r'^webhooks/furnitalia/$',
        view=webhook,
        name="furnitalia_webhook",
    ),

    # delivery fee
    url(
        regex=r'^delivery-fee/$',
        view=TemplateView.as_view(
            template_name='core/ui_iframe.html',
            get_context_data=lambda: {
                'WEB_UI_URL': settings.WEB_UI_URL + "/delivery-fee"}
        ),
        name='delivery_fee',
    ),
    # stock status
    url(
        regex=r'^stock-status/$',
        view=TemplateView.as_view(
            template_name='core/ui_iframe.html',
            get_context_data=lambda: {
                'WEB_UI_URL': settings.WEB_UI_URL + "/stock-status"}
        ),
        name='stock_status',
    ),
]


# router = DefaultRouter()
router = routers.SimpleRouter()
router.register(r'traffic', StoreTrafficViewSet)
router.register(r'schedule', ScheduleViewSet)
router.register(r'warehouse', IncomingShipmentViewSet)
# router.register(r'incoming-orders', IncomingShipmentOrderViewSet)
router.register(r'incoming-items', IncomingShipmentItemViewSet)
router.register(r'orders', OrderApiViewSet)
router.register(r'commissions', commissions_rest.CommissionViewSet)
router.register(r'customers', CustomerViewSet)

shipments_router = routers.NestedSimpleRouter(
    router, r'warehouse', lookup='shipment')
shipments_router.register(r'files', IncomingShipmentFileViewSet)

urlpatterns += [
    url(r'^api/', include(router.urls)),
    url(r'^api/', include(shipments_router.urls)),
    url(r'^api/traffic-summary/', StoreTrafficSummaryView.as_view()),
    url(r'^api/so_items/', SOItemsAPIView.as_view()),
    url(r'^api/client/', APIClientView.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

# rest_urls = [
#     url(r'^api/commissions/$', commssions_rest.CommissionList.as_view()),
#     url(r'^api/commissions/(?P<pk>[0-9]+)/$', commssions_rest.CommissionDetail.as_view()),
# ]
# rest_urls = format_suffix_patterns(rest_urls)
#
# urlpatterns += rest_urls
#
# urlpatterns += patterns('',
#                         (r'^commissions/ajax2/(?P<path>.*)$', 'django.views.static.serve',
#                          {'document_root': normpath(join(settings.SITE_ROOT, 'commissions_app'))}),
#                         (r'^app/(?P<path>.*)$', 'django.views.static.serve',
#                          {'document_root': normpath(join(settings.SITE_ROOT, 'commissions_app', 'app'))}),
#                         (r'^node_modules/(?P<path>.*)$', 'django.views.static.serve',
#                          {'document_root': normpath(join(settings.SITE_ROOT, 'commissions_app', 'node_modules'))}),
#                         )

if settings.DEBUG:
    # development serve static files
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', django.views.static.serve,
            {'document_root': settings.MEDIA_ROOT}),
    ]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
