import json

from django.test import TestCase, Client
from django.utils import timezone

from .models import WebhookTransaction


class WebhookTransactionTestCase(TestCase):
    date_generated = timezone.now() - timezone.timedelta(minutes=5)
    body = {'a': 1, 'b':2, 'c': []}
    body_json = json.dumps(body)
    meta = {"setting1": True}
    meta_json = json.dumps(meta)

    def setUp(self) -> None:
        webhook_tran = WebhookTransaction.objects.create(date_generated=self.date_generated, body=self.body_json, request_meta=self.meta_json)
        webhook_tran.save()

    def test_crud(self):
        webhook_tran = WebhookTransaction.objects.all()[0]
        self.assertEqual(webhook_tran.date_generated, self.date_generated)
        self.assertIsNotNone(webhook_tran.date_received)
        self.assertEqual(json.loads(webhook_tran.body), self.body)
        self.assertEqual(json.loads(webhook_tran.request_meta), self.meta)

    def tearDown(self) -> None:
        transactions = WebhookTransaction.objects.all()
        for t in transactions:
            t.delete()


class WebhookEndpointTestCase(TestCase):
    sample_data = "form%5Bid%5D=35f2857&form%5Bname%5D=Request+Form&fields%5Bname%5D%5Bid%5D=name&fields%5Bname%5D%5Btype%5D=text&fields%5Bname%5D%5Btitle%5D=&fields%5Bname%5D%5Bvalue%5D=Emil+Akhmirov&fields%5Bname%5D%5Braw_value%5D=Emil+Akhmirov&fields%5Bname%5D%5Brequired%5D=0&fields%5Bemail%5D%5Bid%5D=email&fields%5Bemail%5D%5Btype%5D=email&fields%5Bemail%5D%5Btitle%5D=&fields%5Bemail%5D%5Bvalue%5D=emil%40furnitalia.com&fields%5Bemail%5D%5Braw_value%5D=emil%40furnitalia.com&fields%5Bemail%5D%5Brequired%5D=1&fields%5BrequestFormItemName%5D%5Bid%5D=requestFormItemName&fields%5BrequestFormItemName%5D%5Btype%5D=text&fields%5BrequestFormItemName%5D%5Btitle%5D=&fields%5BrequestFormItemName%5D%5Bvalue%5D=Product%3A+Alf+Italia+Accademia+9PC+Dining+Set&fields%5BrequestFormItemName%5D%5Braw_value%5D=Product%3A+Alf+Italia+Accademia+9PC+Dining+Set&fields%5BrequestFormItemName%5D%5Brequired%5D=1&fields%5Bmessage%5D%5Bid%5D=message&fields%5Bmessage%5D%5Btype%5D=textarea&fields%5Bmessage%5D%5Btitle%5D=&fields%5Bmessage%5D%5Bvalue%5D=This+is+another+test+message&fields%5Bmessage%5D%5Braw_value%5D=This+is+another+test+message&fields%5Bmessage%5D%5Brequired%5D=0&fields%5Busource%5D%5Bid%5D=usource&fields%5Busource%5D%5Btype%5D=hidden&fields%5Busource%5D%5Btitle%5D=usource&fields%5Busource%5D%5Bvalue%5D=&fields%5Busource%5D%5Braw_value%5D=&fields%5Busource%5D%5Brequired%5D=0&fields%5Bumedium%5D%5Bid%5D=umedium&fields%5Bumedium%5D%5Btype%5D=hidden&fields%5Bumedium%5D%5Btitle%5D=umedium&fields%5Bumedium%5D%5Bvalue%5D=&fields%5Bumedium%5D%5Braw_value%5D=&fields%5Bumedium%5D%5Brequired%5D=0&fields%5Bucampaign%5D%5Bid%5D=ucampaign&fields%5Bucampaign%5D%5Btype%5D=hidden&fields%5Bucampaign%5D%5Btitle%5D=ucampaign&fields%5Bucampaign%5D%5Bvalue%5D=&fields%5Bucampaign%5D%5Braw_value%5D=&fields%5Bucampaign%5D%5Brequired%5D=0&fields%5Bucontent%5D%5Bid%5D=ucontent&fields%5Bucontent%5D%5Btype%5D=hidden&fields%5Bucontent%5D%5Btitle%5D=ucontent&fields%5Bucontent%5D%5Bvalue%5D=&fields%5Bucontent%5D%5Braw_value%5D=&fields%5Bucontent%5D%5Brequired%5D=0&fields%5Buterm%5D%5Bid%5D=uterm&fields%5Buterm%5D%5Btype%5D=hidden&fields%5Buterm%5D%5Btitle%5D=uterm&fields%5Buterm%5D%5Bvalue%5D=&fields%5Buterm%5D%5Braw_value%5D=&fields%5Buterm%5D%5Brequired%5D=0&fields%5Bireferrer%5D%5Bid%5D=ireferrer&fields%5Bireferrer%5D%5Btype%5D=hidden&fields%5Bireferrer%5D%5Btitle%5D=ireferrer&fields%5Bireferrer%5D%5Bvalue%5D=direct&fields%5Bireferrer%5D%5Braw_value%5D=direct&fields%5Bireferrer%5D%5Brequired%5D=0&fields%5Blreferrer%5D%5Bid%5D=lreferrer&fields%5Blreferrer%5D%5Btype%5D=hidden&fields%5Blreferrer%5D%5Btitle%5D=lreferrer&fields%5Blreferrer%5D%5Bvalue%5D=direct&fields%5Blreferrer%5D%5Braw_value%5D=direct&fields%5Blreferrer%5D%5Brequired%5D=0&fields%5Bilandpage%5D%5Bid%5D=ilandpage&fields%5Bilandpage%5D%5Btype%5D=hidden&fields%5Bilandpage%5D%5Btitle%5D=ilandpage&fields%5Bilandpage%5D%5Bvalue%5D=http%2F%2Fnew.furnitalia.com%2Fshop%2F&fields%5Bilandpage%5D%5Braw_value%5D=http%253A%2F%2Fnew.furnitalia.com%2Fshop%2F&fields%5Bilandpage%5D%5Brequired%5D=0&fields%5Bvisits%5D%5Bid%5D=visits&fields%5Bvisits%5D%5Btype%5D=hidden&fields%5Bvisits%5D%5Btitle%5D=visits&fields%5Bvisits%5D%5Bvalue%5D=44&fields%5Bvisits%5D%5Braw_value%5D=44&fields%5Bvisits%5D%5Brequired%5D=0&meta%5Bdate%5D%5Btitle%5D=Date&meta%5Bdate%5D%5Bvalue%5D=August+28%2C+2020&meta%5Btime%5D%5Btitle%5D=Time&meta%5Btime%5D%5Bvalue%5D=3%3A42+pm&meta%5Bpage_url%5D%5Btitle%5D=Page+URL&meta%5Bpage_url%5D%5Bvalue%5D=https%3A%2F%2Fwww.furnitalia.com%2Fproduct%2Faccademia-9pc-dining-set%2F&meta%5Buser_agent%5D%5Btitle%5D=User+Agent&meta%5Buser_agent%5D%5Bvalue%5D=Mozilla%2F5.0+%28Macintosh%3B+Intel+Mac+OS+X+10_15_5%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F84.0.4147.89+Safari%2F537.36&meta%5Bremote_ip%5D%5Btitle%5D=Remote+IP&meta%5Bremote_ip%5D%5Bvalue%5D=108.210.19.30&meta%5Bcredit%5D%5Btitle%5D=Powered+by&meta%5Bcredit%5D%5Bvalue%5D=Elementor"
    content_type = "application/x-www-form-urlencoded"

    @classmethod
    def setUpClass(cls):
        cls.cls_atomics = cls._enter_atomics()
        cls.client = Client()

    def test_webhook(self):
        response = self.client.post('/webhooks/furnitalia/', data=self.sample_data, content_type=self.content_type)

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, 200)

        tran = WebhookTransaction.objects.all()
        self.assertTrue(len(tran), 1)
        self.assertEqual(tran[0].status, str(WebhookTransaction.UNPROCESSED))
        self.assertTrue(len(tran[0].body) > 0)
        self.assertTrue(len(tran[0].request_meta) > 0)
