import json
import logging
from django.conf import settings

logger = logging.getLogger('furnicloud')

_AMO_LOGIN_PATH = '/private/api/auth.php?type=json'
_AMO_PATH_CONTACTS = '/api/v2/contacts'
_AMO_PATH_LEADS = '/api/v2/leads'
_AMO_PATH_NOTES = '/api/v2/notes'

LOGIN_DATA = {
    'USER_LOGIN': settings.AMOCRM_LOGIN,
    'USER_HASH': settings.AMOCRM_TOKEN,
}


def save_contact(session, contact_info):
    op = 'update' if "id" in contact_info else 'add'
    logger.debug("Saving contact info, op={}, contact_info={}".format(op, contact_info))
    request_body = {op: [contact_info]}
    headers = {'Content-Type': 'application/json'}
    response = session.post(_url(_AMO_PATH_CONTACTS), data=json.dumps(request_body), headers=headers)
    logger.debug("Saved contact info, status={}, content={}".format(response.status_code, response.content))
    if response.status_code not in (200, 204):
        raise Exception("Failed to update a contact in AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))


def update_contacts(session, contacts_info):
    logger.debug("Saving contacts, contacts_info={}".format(contacts_info))
    request_body = {"update": contacts_info}
    headers = {'Content-Type': 'application/json'}
    response = session.post(_url(_AMO_PATH_CONTACTS), data=json.dumps(request_body), headers=headers)
    logger.debug("Updated contact info, status={}, content={}".format(response.status_code, response.content))
    if response.status_code not in (200, 204):
        raise Exception("Failed to update contacts in AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))


def _search_contacts(_session, limit=500, limit_offset=None, query=None, id=None, user=None, modified_since=None, **kwargs):
    request = {}
    if query:
        request['query'] = query
    if id:
        request['id'] = id
    headers = {}
    # request.update({'type': 'contact'})
    if limit is not None:
        request['limit_rows'] = limit
    if limit_offset is not None:
        request['limit_offset'] = limit_offset
    if modified_since is not None:
        headers['if-modified-since'] = modified_since

    logger.debug("Searching for a contact info, params={}, headers={}".format(request, headers))
    response = _session.get(_url(_AMO_PATH_CONTACTS),
                            params=request, headers=headers)
    logger.debug("Contact search completed, status_code={}, res={}".format(response.status_code, response.content))

    if response.status_code not in (200, 204):
        raise Exception("Failed to return data from AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))

    if response.status_code == 204:
        return []

    try:
        r = response.json()
        return r['_embedded']['items']
    except ValueError:
        return response.content


def _search_leads(_session, limit=500, limit_offset=None, query=None, status=None, modified_since=None, **kwargs):
    request = {}
    if query:
        request['query'] = query
    if status:
        request['status'] = status

    headers = {}
    if limit is not None:
        request['limit_rows'] = limit
    if limit_offset is not None:
        request['limit_offset'] = limit_offset
    if modified_since is not None:
        headers['if-modified-since'] = modified_since

    logger.debug("Searching for existing leads, params={}, headers={}".format(request, headers))
    response = _session.get(_url(_AMO_PATH_LEADS),
                            params=request, headers=headers)
    logger.debug("Leads search completed, status_code={}, res={}".format(response.status_code, response.content))

    if response.status_code not in (200, 204):
        raise Exception("Failed to return data from AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))

    if response.status_code == 204:
        return []

    try:
        r = response.json()
        return r['_embedded']['items']
    except ValueError:
        return response.content


def create_lead(session, lead_info):
    lead_info = {'add': [lead_info]}
    headers = {'Content-Type': 'application/json'}
    logger.debug("Creating a lead, lead_info={}".format(lead_info))
    response = session.post(_url(_AMO_PATH_LEADS), data=json.dumps(lead_info), headers=headers)
    logger.debug("Lead creation result: status_code={}, resp={}".format(response.status_code, response.content))

    if response.status_code not in (200, 204):
        raise Exception("Failed to create a lead in AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))

    try:
        result = response.json()
    except ValueError as e:
        raise Exception("Failed to parse lead creation response from AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))

    try:
        lead_id = result['_embedded']['items'][0]["id"]
    except KeyError as e:
        raise Exception("Failed to get the created AmoCRM lead ID. Status=%d content=%s".format(
            response.status_code, response.content))

    return lead_id


def add_note_to_lead(session, note_data):
    request_body = {'add': [note_data]}
    headers = {'Content-Type': 'application/json'}
    logger.debug("Creating a note: note_data={}".format(note_data))
    response = session.post(_url(_AMO_PATH_NOTES), data=json.dumps(request_body), headers=headers)
    logger.debug("Note creation result: status_code={}, resp={}".format(response.status_code, response.content))

    if response.status_code not in (200, 204):
        raise Exception("Failed to create a note in AmoCRM. Status=%d content=%s".format(
            response.status_code, response.content))


def get_notes(session, request):
    response = session.get(_url(_AMO_PATH_NOTES), params=request)
    if response.status_code != 200:
        logger.error("Failed to load notes; Status={}, request={}".format(response.status_code, request))
        return []

    try:
        r = response.json()
        return r['_embedded']['items']
    except ValueError:
        return response.content


def amo_login(session):
    response = session.post(_url(_AMO_LOGIN_PATH), data=json.dumps(LOGIN_DATA))
    if not response.ok:
        logger.error('Something went wrong while logging in to AmoCRM')
        raise Exception("Failed to login to AmoCRM")


def search_contacts(_session, query, limit=1, modified_since=None, **kwargs):
    logger.info("Searching for existing contact. Query={}".format(query))
    results = _search_contacts(_session, query=query, limit=limit,
                               modified_since=modified_since, **kwargs)
    return list(results) if results else []


def get_contact(_session, id):
    logger.info("Loading contact info. ID={}".format(id))
    results = _search_contacts(_session, id=id)
    return list(results)[0] if results else None


def search_leads(_session, query=None, status=None, limit=1, modified_since=None, **kwargs):
    logger.info("Searching for existing contact. Query={}, status={}".format(query, status))
    results = _search_leads(_session, query=query, status=status, limit=limit,
                            modified_since=modified_since, **kwargs)
    return list(results) if results else []


def _url(path):
    return 'https://%(domain)s.amocrm.com%(path)s' % {'domain': 'furnitalia', 'path': path}
