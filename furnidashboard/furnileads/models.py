from django.db import models
from django.utils import timezone


class WebhookTransaction(models.Model):
    UNPROCESSED = 1
    PROCESSED = 2
    ERROR = 3

    STATUSES = (
        (UNPROCESSED, 'Unprocessed'),
        (PROCESSED, 'Processed'),
        (ERROR, 'Error'),
    )

    date_generated = models.DateTimeField()
    date_received = models.DateTimeField(default=timezone.now)
    body = models.TextField(max_length=32000)
    request_meta = models.TextField(max_length=32000)
    status = models.CharField(max_length=250, choices=STATUSES, default=UNPROCESSED)

    def __str__(self):
        return u'Webhook tran id={0}: generated={1} | Status={2}'.format(self.pk, self.date_generated,
                                                                         self.get_status_display())
