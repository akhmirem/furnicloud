import copy
import json
import logging

from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from querystring_parser import parser

from .models import WebhookTransaction

logger = logging.getLogger("furnicloud")


@csrf_exempt
@require_POST
def webhook(request):
    jsondata = parser.parse(request.POST.urlencode())
    logger.info("Received webhook request:")
    logger.debug(jsondata)
    data = json.dumps(jsondata)
    logger.debug("Webhook json={}".format(data))
    meta = copy.copy(request.META)
    for k in list(meta):
        if not isinstance(meta[k], str):
            del meta[k]

    WebhookTransaction.objects.create(
        date_generated=timezone.now(),
        body=data,
        request_meta=meta
    )

    return HttpResponse(status=200)
