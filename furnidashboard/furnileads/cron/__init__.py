import kronos

from furnileads.cron.cron_process_leads import process_leads
from furnileads.cron.cron_unprocessed_leads import process_leads_report
from furnileads.cron.cron_update_contacts import amocrm_fix_contact_names


@kronos.register('10 * * * *')
def amocrm_fix_contact_names_cron_task():
    amocrm_fix_contact_names()


@kronos.register('5 * * * *')
def process_message():
    process_leads()


@kronos.register('5 * * * *')
def process_new_leads_report():
    process_leads_report()