import logging
from datetime import datetime, timedelta

import requests

import furnileads.amocrm_api as amo

logger = logging.getLogger('furnicloud')


# CALL_STATUS_CONVERSATION = "4"
# CALL_STATUS_LEFT_VM = "1"
# CALL_STATUS_DIDNT_GO_THROUGH = "6"
EVENT_NOTE_TYPES = {
    'LEAD_CREATED': 1,
    'CONTACT_CREATED': 2,
    'LEAD_STATUS_CHANGED': 3,
    'COMMON': 4,  # Normal note
    'CALL_IN': 10,  # Incoming call
    'CALL_OUT': 11,  # Outgoing call
    'COMPANY_CREATED': 12,
    'TASK_RESULT': 13,
    'SYSTEM': 25,  # System message
    'SMS_IN': 102,  # Incoming SMS
    'SMS_OUT': 103,  # Outgoing SMS
}
NOTE_TYPES_FOR_ACTIVE_CONTACT = (
    EVENT_NOTE_TYPES['CALL_OUT'], EVENT_NOTE_TYPES['COMMON'], EVENT_NOTE_TYPES['LEAD_CREATED'])
CALL_DURATION_THRESHOLD1 = 15  # duration in seconds

IF_MODIFIED_SINCE_DATE_FORMAT = "%a, %d %b %Y %H:%M:%S UTC"
CYRILLIC_NAMES = (u"Пропущенный",
                  u"Входящий",
                  u"Исходящий",
                  u"Клиент не ответил")


def amocrm_fix_contact_names():
    session = requests.Session()
    amo.amo_login(session)

    query = ""
    recent_contacts = amo.search_contacts(session, query, limit=50,
                                          modified_since=get_modified_since())
    contacts_to_update = []
    for contact in recent_contacts:
        #logger.debug("Processing contact {}".format(contact))
        if contact['name']:
            if is_rename_required(contact):
                # contact doesn't have a name
                # delete the contact if there are only calls made without a voice message
                # rename and keep otherwise
                contact_to_update = update_contact_info(contact)
                # if should_mark_for_delete(session, contact):
                #     contact_to_update['name'] = 'TO_DELETE: ' + contact_to_update['name']
                contacts_to_update.append(contact_to_update)
                #logger.debug(
                #    "Contact will be updated: {}".format(contact_to_update))
        # print("----------------")

    if len(contacts_to_update) > 0:
        amocrm_update_contacts(session, contacts_to_update)


def is_rename_required(contact):
    return any(cyr_name in contact['name'] for cyr_name in CYRILLIC_NAMES) or '19163329000' in contact['name'] or '19164840333' in contact['name']


def should_mark_for_delete(_session, contact):
    request = {
        'type': 'contact',
        'element_id': contact['id']
    }
    notes = amo.get_notes(_session, request)
    for note in notes:

        # if there is outgoing call, lead, or a regular note, keep the contact
        if note['note_type'] in NOTE_TYPES_FOR_ACTIVE_CONTACT:
            return False

        # if call went through or voice message was left, keep the contact
        if note['note_type'] == EVENT_NOTE_TYPES['CALL_IN'] and \
                note['params']['DURATION'] > CALL_DURATION_THRESHOLD1:
            return False

    # if contact was updated within the last 15 min, call info might not yet be available
    # so skip this contact for now
    if from_ts(contact['updated_at']) + timedelta(minutes=3) > datetime.now():
        return False

    #logger.debug("Contact {} can be deleted".format(contact['id']))
    return True


def amocrm_update_contacts(_session, contacts_to_update):
    #logger.info("Updating contact names, list={}".format(contacts_to_update))
    amo.update_contacts(_session, contacts_to_update)


def update_contact_info(contact):
    new_name = contact['name']
    new_name = new_name.replace(u"(+19163329000 - Furnitalia Sacramento)", "")
    new_name = new_name.replace("(+19164840333)", "")
    new_name = new_name.replace(u"Пропущенный", "Missed")
    new_name = new_name.replace(u"Исходящий", "Outgoing")
    new_name = new_name.replace(u"Входящий", "Incoming")
    new_name = new_name.replace(u"Клиент не ответил", "Outgoing")
    return {
        'id': contact['id'],
        'updated_at': get_updated_at(),
        'name': new_name.strip()
    }


def get_modified_since():
    # 'IF-MODIFIED-SINCE: Mon, 01 Aug 2017 07:07:23 UTC'
    ts = datetime.utcnow() - timedelta(hours=48)
    modified_since = ts.strftime(IF_MODIFIED_SINCE_DATE_FORMAT)
    return modified_since


def get_updated_at():
    ts = datetime.now()
    return ts.strftime("%s")


def from_ts(timestamp):
    return datetime.fromtimestamp(timestamp)
