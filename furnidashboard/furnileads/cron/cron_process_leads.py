import json
import logging
from datetime import datetime

import requests

import furnileads.amocrm_api as amo
from furnileads.models import WebhookTransaction

logger = logging.getLogger('furnicloud')

LEAD_STATUS_UNPROCESSED = '10739429'
NOTE_TYPE_NORMAL = '4'
ELEMENT_TYPE_LEAD = '2'
CUSTOM_FIELD_REFERRED_BY_ID = '1522440'
CUSTOM_FIELD_EMAIL = "1511782"
CUSTOM_FIELD_ADDRESS = "1523106"

UTM_FIELDS = ('usource', 'umedium', 'ucampaign', 'ucontent',
              'uterm', 'ireferrer', 'lreferrer', 'ilandpage', 'visits')


def process_leads():
    unprocessed_trans = get_transactions_to_process()
    logger.info("Processing incoming leads, count={}".format(
        len(unprocessed_trans)))
    for trans in unprocessed_trans:
        try:
            logger.info("Processing lead={}".format(trans))
            process_trans(trans)
            trans.status = WebhookTransaction.PROCESSED
            trans.save()

        except Exception as e:
            logger.error(
                "Error processing lead, tran={}, error={}".format(trans, e))
            trans.status = WebhookTransaction.ERROR
            trans.save()
            raise e
    logger.info("Finished processing new leads")


def get_transactions_to_process():
    return WebhookTransaction.objects.filter(
        status__in=(WebhookTransaction.UNPROCESSED, WebhookTransaction.ERROR)
    )


def process_trans(trans):
    session = requests.Session()
    amo.amo_login(session)
    data = json.loads(trans.body)
    logger.debug("Lead data: {}".format(trans.body))
    logger.debug("Lead meta: {}".format(trans.request_meta))
    create_lead(session, data, trans)


def create_lead(session, data, trans_info):
    logger.info("Creating a lead for {}".format(trans_info))
    ip = data['meta']['remote_ip']['value']
    ip_geolocation = get_ip_address_geo_location(ip)
    logger.debug("Lead IP geolocation: {}".format(str(ip_geolocation)))

    # create lead
    lead_id = create_lead_in_crm(session, data, trans_info)

    # add note to lead
    add_note_to_lead(session, data, lead_id, trans_info, ip_geolocation)

    # create/update contact for lead
    create_or_update_contact(session, data, lead_id,
                             trans_info, ip_geolocation)


def create_lead_in_crm(session, data, trans_info) -> str:
    name = data['fields']['name']['value']
    item = data['fields']['requestFormItemName']['value']
    lead_name = "Web Request"
    if name:
        lead_name = name
    if len(item):
        lead_name += " - {item}".format(item=item.replace("Product: ", ""))
    amocrm_lead = {
        'name': lead_name,
        'created_at': ts(trans_info.date_generated),
        'updated_at': ts_now(),
        'status_id': LEAD_STATUS_UNPROCESSED,
        'tags': 'request',
    }
    lead_id = amo.create_lead(session, amocrm_lead)
    return lead_id


def add_note_to_lead(session, data, lead_id, trans_info, ip_geocoded_addr=None):
    location_full = "{city}, {region} {postal}, {country}".format(
        **ip_geocoded_addr)
    utm = ["{field}: {value}".format(field=field, value=data['fields'][field]['value'])
           for field in UTM_FIELDS if field in data['fields'] and data['fields'][field]['value']]
    message = data['fields']['message']['value']
    item = data['fields']['requestFormItemName']['value']
    note_body = "IP Address Estimated Location: {loc}\r\n" \
                "================================\r\n" \
                "{item}\r\n{message}\r\n" \
                "--------------------------------\r\n" \
                "{utm}".format(loc=location_full, item=item,
                               message=message, utm="  |  ".join(utm))
    note_info = {
        "element_id": lead_id,
        "element_type": ELEMENT_TYPE_LEAD,
        "note_type": NOTE_TYPE_NORMAL,
        "created_at": ts(trans_info.date_generated),
        "updated_at": ts_now(),
        "text": note_body
    }
    amo.add_note_to_lead(session, note_info)


def create_or_update_contact(session, data, lead_id, trans_info, ip_geoloc):
    email = data['fields']['email']['value']
    name = data['fields']['name']['value']
    matched_contacts = amo.search_contacts(session, email) if email else []
    if len(matched_contacts):
        contact = matched_contacts[0]
        logger.info("Found existing contact: {}".format(contact))
        update_contact(session, contact, data, lead_id, trans_info, ip_geoloc)
    else:
        contact_info = {
            "name": name,
            "created_at": ts(trans_info.date_generated),
            "updated_at": ts_now(),
            "leads_id": lead_id,
            "custom_fields": [
                email_field(email),
                address_field_from_ip_geocode(ip_geoloc)
            ]
        }
        amo.save_contact(session, contact_info)


def update_contact(session, contact, data, lead_id, trans_info, ip_geoloc):
    leads = [lead_id]
    if contact["leads"] and "id" in contact["leads"]:
        leads += list(contact["leads"]["id"])

    contact_info = {
        "id": contact["id"],
        "updated_at": ts_now(),
        "leads_id": leads
    }
    if not has_contact_address(contact):
        contact_info["custom_fields"] = [
            f for f in contact["custom_fields"] if f["name"] != "Address"]
        contact_info["custom_fields"].append(
            address_field_from_ip_geocode(ip_geoloc))
    logger.debug("Updating fields on contact: {}".format(contact_info))
    amo.save_contact(session, contact_info)


def has_contact_address(contact) -> bool:
    try:
        contact_address_custom_field = [
            f for f in contact['custom_fields'] if f['name'] == 'Address'][0]
        for address_field in contact_address_custom_field["values"]:
            if address_field['subtype'] in ('city', 'state', 'zip') and address_field['value']:
                return True
        return False
    except (KeyError, IndexError):
        return False


def get_ip_address_geo_location(ip: str) -> dict:
    r = requests.get('https://ipinfo.io/{ip}'.format(ip=ip))
    geo_data = {
        'city': '',
        'region': '',
        'country': '',
        'postal': ''
    }
    geo_data.update(r.json())
    return geo_data


def ts_now():
    return datetime.now().strftime("%s")


def ts(dt):
    return dt.strftime("%s")


def address_field_from_ip_geocode(ip_geocoded_addr):
    return {
        "id": CUSTOM_FIELD_ADDRESS,
        "values": [
            {
                "value": ip_geocoded_addr["city"],
                "subtype": "city"
            },
            {
                "value": ip_geocoded_addr["region"],
                "subtype": "state"
            },
            {
                "value": ip_geocoded_addr["postal"],
                "subtype": "zip"
            }
        ]
    }


def email_field(email):
    return {
        "id": CUSTOM_FIELD_EMAIL,
        "values": [
            {
                "value": email,
                "enum": "PRIV"
            }
        ]
    }
