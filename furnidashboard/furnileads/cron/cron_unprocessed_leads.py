import logging
from datetime import datetime

import requests
from django.conf import settings
from django.template.loader import render_to_string
from django.utils import timezone

import furnileads.amocrm_api as amo
from orders.cron_tasks.utils import send_emails

logger = logging.getLogger('furnicloud')

LEAD_STATUS_UNPROCESSED = '10739429'
NOTE_TYPE_NORMAL = '4'
ELEMENT_TYPE_LEAD = '2'
CUSTOM_FIELD_REFERRED_BY_ID = '1522440'
CUSTOM_FIELD_EMAIL = "1511782"
CUSTOM_FIELD_ADDRESS = "1523106"

USERS = {
    726812: 'Admin',
    2113132: 'Becca',
    6035879: 'Marie',
    6828860: 'Omar',
}


class LeadInfo(object):
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.contact = kwargs.get('contact')
        self.created_date = kwargs.get('created_date')
        self.owner = kwargs.get('owner')
        self.id = kwargs.get('id')

    def __str__(self):
        return 'Lead [name={}, contact={}, date={}, owner={}, id={}]'.format(self.name, self.contact, self.created_date,
                                                                             self.owner, self.id)

    @property
    def days_unprocessed(self):
        return (timezone.now() - timezone.make_aware(datetime.strptime(self.created_date, '%m/%d/%Y'))).days


def process_leads_report():
    logger.info("Starting unprocessed leads report")
    try:
        session = requests.Session()
        amo.amo_login(session)
        leads = amo.search_leads(session, status=LEAD_STATUS_UNPROCESSED, limit=30)
        #logger.info(leads)
        new_leads = []
        if leads:
            for lead in leads:
                contact_name = ""
                if 'main_contact' in lead:
                    contact = amo.get_contact(session, lead['main_contact']['id'])
                    #logger.info('Found contact: {}'.format(contact))
                    contact_name = contact['name']
                try:
                    owner = USERS[lead['responsible_user_id']]
                except KeyError:
                    owner = "N/A"

                new_leads.append(LeadInfo(**{
                    'name': lead['name'],
                    'contact': contact_name,
                    'created_date': format_date(lead['created_at']),
                    'owner': owner,
                    'id': lead['id']
                }))
            if len(new_leads) > 0:
                #logger.info(new_leads)
                msg = render_to_string("core/furnileads_unprocessed_list_email.html",
                                       {'leads': new_leads, 'current_date': timezone.now()})
                send_emails(
                    to=settings.LEAD_EMAIL_NOTIFICATION_LIST,
                    subject='Furnitalia - {} Unprocessed Leads in AmoCRM'.format(len(new_leads)),
                    message=msg
                )
    except Exception as e:
        logger.error("Error during preparing unprocessed leads report, error={}".format(e))
        raise e
    logger.info("Finished unprocessed leads report")


def format_date(timestamp):
    return timezone.make_aware(datetime.fromtimestamp(timestamp)).strftime('%m/%d/%Y')


def ts_now():
    return timezone.now().strftime("%s")


def ts(dt):
    return dt.strftime("%s")
