from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase, Client, RequestFactory

from datetime import datetime

from django.utils import timezone

from commissions.models import Commission
from commissions.views import BonusMonthlyReportView
from customers.models import Customer
from orders.views.non_furniture_orders_views import NonFurnitureOrderMonthArchiveTableView
from orders.models import Order, OrderPayment
from orders.views.general import OrderMonthArchiveTableView, SalesStandingsMonthTableView
from stores.models import Store


class OrderMonthArchiveTableViewTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cls_atomics = cls._enter_atomics()

    def setUp(self) -> None:
        self.factory = RequestFactory()

        associates_group = Group.objects.create(name="associates")
        self.set_up_associates_group(associates_group)

        self.user = User.objects.create_user(
            username='admin', email='admin@furnitalia.com', password='top_secret')
        self.user.groups.add(associates_group)
        self.user.first_name = "Admin"
        self.user.last_name = "L"
        self.user.save()

        self.user2 = User.objects.create_user(
            username='user2', email='user2@furnitalia.com', password='top_secret')
        self.user2.groups.add(associates_group)
        self.user2.first_name = "User2"
        self.user2.last_name = "L"
        self.user2.save()

        self.user3 = User.objects.create_user(
            username='user3', email='user3@furnitalia.com', password='top_secret')
        self.user3.groups.add(associates_group)
        self.user3.first_name = "User3"
        self.user3.last_name = "L"
        self.user3.save()

        self.cust = Customer.objects.create(first_name="John", last_name="Doe")
        self.cust.save()
        self.sac_store = Store.objects.create(name="Sacramento")
        self.sac_store.save()

        self.create_orders()

    def test_archive_view(self):
        request = self.factory.get('/orders/')
        request.user = self.user

        response = OrderMonthArchiveTableView.as_view(month_format='%m')(request, year=str(datetime.today().year),
                                                                         month=str(datetime.today().month))

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales totals by store
        month_totals_table_data = response.context_data['month_totals_table'].data
        self.assertEqual(month_totals_table_data[0]['hq'], '$ 6,100.00')
        self.assertEqual(month_totals_table_data[0]['total'], '$ 6,100.00')
        self.assertEqual(month_totals_table_data[1]['hq'], '$ 6,100.00')
        self.assertEqual(month_totals_table_data[1]['total'], '$ 6,100.00')
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales totals by associate
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 3000)
        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 2200)
        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 900)

        # view = OrderMonthArchiveTableView(month_format='%m')
        # view.setup(request, year=str(datetime.today().year), month=str(datetime.today().month))

        # context = view.get_context_data()
        # self.assertIn('month_totals_table', context)

    def test_sales_view(self):
        request = self.factory.get('/sales-standings/')
        request.user = self.user

        response = SalesStandingsMonthTableView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales by associate table
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 3000)
        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 2200)
        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 900)

        # verify expanded sales by associate list table
        sales_by_associate_expanded_data = response.context_data['sales_by_assoc_expanded_tables']
        admin_user_sales = sorted(sales_by_associate_expanded_data['Admin'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(admin_user_sales[0]['amount'], 100)
        self.assertAlmostEqual(admin_user_sales[1]['amount'], 1400)
        self.assertAlmostEqual(admin_user_sales[2]['amount'], 1500)
        self.assertAlmostEqual(admin_user_sales[3]['amount'], 0)
        self.assertAlmostEqual(admin_user_sales[4]['amount'], 0)

        user2_sales = sorted(sales_by_associate_expanded_data['User2'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user2_sales[0]['amount'], 600)
        self.assertAlmostEqual(user2_sales[1]['amount'], 1500)
        self.assertAlmostEqual(user2_sales[2]['amount'], 100)
        self.assertAlmostEqual(user2_sales[3]['amount'], 0)

        user3_sales = sorted(sales_by_associate_expanded_data['User3'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user3_sales[0]['amount'], 900)
        self.assertAlmostEqual(user3_sales[1]['amount'], 0)

        # verify sales by store
        month_totals_table_data = response.context_data['store_totals_table'].data
        self.assertEqual(month_totals_table_data[0]['hq'], '$ 6,100.00')
        self.assertEqual(month_totals_table_data[0]['total'], '$ 6,100.00')
        self.assertEqual(month_totals_table_data[1]['hq'], '$ 6,100.00')
        self.assertEqual(month_totals_table_data[1]['total'], '$ 6,100.00')

    def test_bonus_view(self):
        request = self.factory.get('/bonus-report/')
        request.user = self.user

        response = BonusMonthlyReportView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context_data['sales_by_associate'].data is not None)

        # verify sales by associate table
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 1600)
        self.assertAlmostEqual(sales_by_associate_data[0]['eligible_amount'], 1600)
        self.assertAlmostEqual(sales_by_associate_data[0]['bonus'], 0)

        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 1600)
        self.assertAlmostEqual(sales_by_associate_data[1]['eligible_amount'], 1600)
        self.assertAlmostEqual(sales_by_associate_data[1]['bonus'], 0)

        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 900)
        self.assertAlmostEqual(sales_by_associate_data[2]['eligible_amount'], 900)
        self.assertAlmostEqual(sales_by_associate_data[2]['bonus'], 0)

        # verify expanded sales by associate list table
        sales_by_associate_expanded_data = response.context_data['bonus_elig_orders_by_associate']
        admin_user_sales = sorted(sales_by_associate_expanded_data['Admin'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(admin_user_sales[0]['amount'], 100)
        self.assertAlmostEqual(admin_user_sales[0]['order_number'], "SO-1001")
        self.assertAlmostEqual(admin_user_sales[1]['amount'], 1500)
        self.assertAlmostEqual(admin_user_sales[1]['order_number'], "SO-1003")


        user2_sales = sorted(sales_by_associate_expanded_data['User2'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user2_sales[0]['amount'], 1500)
        self.assertAlmostEqual(user2_sales[0]['order_number'], "SO-1003")
        self.assertAlmostEqual(user2_sales[1]['amount'], 100)
        self.assertAlmostEqual(user2_sales[1]['order_number'], "SO-1004")

        user3_sales = sorted(sales_by_associate_expanded_data['User3'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user3_sales[0]['amount'], 900)
        self.assertAlmostEqual(user3_sales[0]['order_number'], "SO-1004")

    def test_archive_view_cabinetry(self):
        request = self.factory.get('/orders/cabinetry/')
        request.user = self.user

        response = NonFurnitureOrderMonthArchiveTableView.as_view(month_format='%m')(request,
            year=str(datetime.today().year), month=str(datetime.today().month))

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        orders = response.context_data['table'].data
        orders = sorted(orders, key=lambda a: a.number)
        self.assertTrue(len(orders) == 3)

        order_numbers = [o.number for o in orders]
        self.assertIn("SO-1009", order_numbers)
        self.assertIn("SO-10091", order_numbers)
        self.assertIn("SO-10092", order_numbers)

    def test_sales_view_cabinetry(self):
        request = self.factory.get(
            '/sales-standings/', data={'department': 'kitchen-and-bath'})
        request.user = self.user

        response = SalesStandingsMonthTableView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales by associate table
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 500)
        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 1000)
        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 500)

        # verify expanded sales by associate list table
        sales_by_associate_expanded_data = response.context_data['sales_by_assoc_expanded_tables']
        admin_user_sales = sorted(sales_by_associate_expanded_data['Admin'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(admin_user_sales[0]['amount'], 500)

        user2_sales = sorted(sales_by_associate_expanded_data['User2'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user2_sales[0]['amount'], 1000)
        self.assertAlmostEqual(user2_sales[1]['amount'], 0)

        user3_sales = sorted(sales_by_associate_expanded_data['User3'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user3_sales[0]['amount'], 500)

        # verify sales by store
        month_totals_table_data = response.context_data['store_totals_table'].data
        self.assertEqual(month_totals_table_data[0]['hq'], '$ 2,000.00')
        self.assertEqual(month_totals_table_data[0]['total'], '$ 2,000.00')
        self.assertEqual(month_totals_table_data[1]['hq'], '$ 2,000.00')
        self.assertEqual(month_totals_table_data[1]['total'], '$ 2,000.00')

    def test_archive_view_appliances(self):
        request = self.factory.get('/orders/appliances/')
        request.user = self.user

        response = NonFurnitureOrderMonthArchiveTableView.as_view(month_format='%m',
            order_types = ['appliances'])(request, year=str(datetime.today().year),
                month=str(datetime.today().month))

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        orders = response.context_data['table'].data
        orders = sorted(orders, key=lambda a: a.number)
        self.assertTrue(len(orders) == 3)

        order_numbers = [o.number for o in orders]
        self.assertIn("SO-1007", order_numbers)
        self.assertIn("SO-10071", order_numbers)
        self.assertIn("SO-10072", order_numbers)

    def test_sales_view_appliances(self):
        request = self.factory.get(
            '/sales-standings/', data={'department': 'appliances'})
        request.user = self.user

        response = SalesStandingsMonthTableView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales by associate table
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 1000)
        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 250)
        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 250)

        # verify expanded sales by associate list table
        sales_by_associate_expanded_data = response.context_data['sales_by_assoc_expanded_tables']
        admin_user_sales = sorted(sales_by_associate_expanded_data['Admin'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(admin_user_sales[0]['amount'], 1000)
        self.assertAlmostEqual(admin_user_sales[1]['amount'], 0)

        user2_sales = sorted(sales_by_associate_expanded_data['User2'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user2_sales[0]['amount'], 250)

        user3_sales = sorted(sales_by_associate_expanded_data['User3'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user3_sales[0]['amount'], 250)

        # verify sales by store
        month_totals_table_data = response.context_data['store_totals_table'].data
        self.assertEqual(month_totals_table_data[0]['hq'], '$ 1,500.00')
        self.assertEqual(month_totals_table_data[0]['total'], '$ 1,500.00')
        self.assertEqual(month_totals_table_data[1]['hq'], '$ 1,500.00')
        self.assertEqual(month_totals_table_data[1]['total'], '$ 1,500.00')

    def test_archive_view_hunter_douglas(self):
        request = self.factory.get('/orders/windows/')
        request.user = self.user

        response = NonFurnitureOrderMonthArchiveTableView.as_view(month_format='%m',
            order_types = ['windows'])(request, year=str(datetime.today().year),
                month=str(datetime.today().month))

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        orders = response.context_data['table'].data
        orders = sorted(orders, key=lambda a: a.number)
        self.assertTrue(len(orders) == 3)

        order_numbers = [o.number for o in orders]
        self.assertIn("SO-1008", order_numbers)
        self.assertIn("SO-10081", order_numbers)
        self.assertIn("SO-10083", order_numbers)

    def test_sales_view_hunter_douglas(self):
        request = self.factory.get(
            '/sales-standings/', data={'department': 'windows'})
        request.user = self.user

        response = SalesStandingsMonthTableView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales by associate table
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 1000)
        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 1000)
        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 1000)

        # verify expanded sales by associate list table
        sales_by_associate_expanded_data = response.context_data['sales_by_assoc_expanded_tables']
        admin_user_sales = sorted(sales_by_associate_expanded_data['Admin'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(admin_user_sales[0]['amount'], 1000)

        user2_sales = sorted(sales_by_associate_expanded_data['User2'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user2_sales[0]['amount'], 1000)
        self.assertAlmostEqual(user2_sales[1]['amount'], 0)

        user3_sales = sorted(sales_by_associate_expanded_data['User3'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user3_sales[0]['amount'], 1000)

        # verify sales by store
        month_totals_table_data = response.context_data['store_totals_table'].data
        self.assertEqual(month_totals_table_data[0]['hq'], '$ 3,000.00')
        self.assertEqual(month_totals_table_data[0]['total'], '$ 3,000.00')
        self.assertEqual(month_totals_table_data[1]['hq'], '$ 3,000.00')
        self.assertEqual(month_totals_table_data[1]['total'], '$ 3,000.00')


    def test_archive_view_flooring(self):
        request = self.factory.get('/orders/flooring/')
        request.user = self.user

        response = NonFurnitureOrderMonthArchiveTableView.as_view(month_format='%m',
            order_types = ['flooring'])(request, year=str(datetime.today().year),
                month=str(datetime.today().month))

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        orders = response.context_data['table'].data
        orders = sorted(orders, key=lambda a: a.number)
        self.assertTrue(len(orders) == 3)

        order_numbers = [o.number for o in orders]
        self.assertIn("SO-1010", order_numbers)
        self.assertIn("SO-1011", order_numbers)
        self.assertIn("SO-1012", order_numbers)

    def test_sales_view_flooring(self):
        request = self.factory.get(
            '/sales-standings/', data={'department': 'flooring'})
        request.user = self.user

        response = SalesStandingsMonthTableView.as_view()(request)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.context_data['object_list']) > 0)

        # verify sales by associate table
        sales_by_associate_data = response.context_data['sales_by_associate'].data
        sales_by_associate_data = sorted(sales_by_associate_data, key=lambda a: a['associate'])
        self.assertAlmostEqual(sales_by_associate_data[0]['sales'], 500)
        self.assertAlmostEqual(sales_by_associate_data[1]['sales'], 1000)
        self.assertAlmostEqual(sales_by_associate_data[2]['sales'], 500)

        # verify expanded sales by associate list table
        sales_by_associate_expanded_data = response.context_data['sales_by_assoc_expanded_tables']
        admin_user_sales = sorted(sales_by_associate_expanded_data['Admin'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(admin_user_sales[0]['amount'], 500)
        self.assertAlmostEqual(admin_user_sales[1]['amount'], 0)

        user2_sales = sorted(sales_by_associate_expanded_data['User2'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user2_sales[0]['amount'], 1000)

        user3_sales = sorted(sales_by_associate_expanded_data['User3'].data, key=lambda row: row['order_number'])
        self.assertAlmostEqual(user3_sales[0]['amount'], 500)

        # verify sales by store
        month_totals_table_data = response.context_data['store_totals_table'].data
        self.assertEqual(month_totals_table_data[0]['hq'], '$ 2,000.00')
        self.assertEqual(month_totals_table_data[0]['total'], '$ 2,000.00')
        self.assertEqual(month_totals_table_data[1]['hq'], '$ 2,000.00')
        self.assertEqual(month_totals_table_data[1]['total'], '$ 2,000.00')

    def set_up_associates_group(self, associates_group):
        self.add_perms_to_group(associates_group, 'commissions', 'commission')
        self.add_perms_to_group(associates_group, 'customers', 'customer')
        self.add_perms_to_group(associates_group, 'orders', 'order')
        self.add_perms_to_group(associates_group, 'orders', 'orderitem')
        self.add_perms_to_group(associates_group, 'orders', 'orderpayment')

    def add_perms_to_group(self, group, app_label, model):
        content_type = ContentType.objects.get(app_label=app_label, model=model)
        perms = Permission.objects.filter(content_type=content_type)
        for p in perms:
            group.permissions.add(p)

    def create_orders(self):
        # Furniture
        o1 = Order.objects.create(number="SO-1001", order_date=tznow(), customer=self.cust, status='N',
                                  subtotal_after_discount=100, store=self.sac_store, order_type="stock",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o1, payment_date=tznow(), payment_type="CASH", amount=50)
        Commission.objects.create(associate=self.user, order=o1, portion_percentage=100)

        # Furniture
        o2 = Order.objects.create(number="SO-1002", order_date=tznow(), customer=self.cust, status='N',
                                  subtotal_after_discount=2000, store=self.sac_store, order_type="stock")
        Commission.objects.create(associate=self.user, order=o2, portion_percentage=70)
        Commission.objects.create(associate=self.user2, order=o2, portion_percentage=30)

        # Furniture
        o3 = Order.objects.create(number="SO-1003", order_date=tznow(), customer=self.cust, status='N',
                                  subtotal_after_discount=3000, store=self.sac_store, order_type="stock",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o3, payment_date=tznow(), payment_type="CASH", amount=1500)
        Commission.objects.create(associate=self.user, order=o3, portion_percentage=50)
        Commission.objects.create(associate=self.user2, order=o3, portion_percentage=50)

        # Furniture
        o4 = Order.objects.create(number="SO-1004", order_date=tznow(), customer=self.cust, status='N',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="stock",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o1, payment_date=tznow(), payment_type="CASH", amount=100)
        Commission.objects.create(associate=self.user3, order=o4, portion_percentage=90)
        Commission.objects.create(associate=self.user2, order=o4, portion_percentage=10)

        # Canceled Furniture
        o5 = Order.objects.create(number="SO-1005", order_date=tznow(), customer=self.cust, status='E',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="stock")
        o5.save()
        Commission.objects.create(associate=self.user, order=o5, portion_percentage=15)
        Commission.objects.create(associate=self.user2, order=o5, portion_percentage=15)
        Commission.objects.create(associate=self.user3, order=o5, portion_percentage=70)

        # Furniture Quote
        o6 = Order.objects.create(number="SO-1006", order_date=tznow(), customer=self.cust, status='QUOT',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="stock", is_quote=True)
        o6.save()
        Commission.objects.create(associate=self.user, order=o6, portion_percentage=100)

        # Appliances #1
        o7 = Order.objects.create(number="SO-1007", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="appliances")
        OrderPayment.objects.create(order=o7, payment_date=tznow(), payment_type="CASH", amount=500)
        Commission.objects.create(associate=self.user, order=o7, portion_percentage=100)

        # Appliances #2
        o10071 = Order.objects.create(number="SO-10071", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=500, store=self.sac_store, order_type="appliances")
        OrderPayment.objects.create(order=o10071, payment_date=tznow(), payment_type="CASH", amount=500)
        Commission.objects.create(associate=self.user2, order=o10071, portion_percentage=50)
        Commission.objects.create(associate=self.user3, order=o10071, portion_percentage=50)

        # Appliances Quote
        o10072 = Order.objects.create(number="SO-10072", order_date=tznow(), customer=self.cust, status='QUOT',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="appliances", is_quote=True)
        Commission.objects.create(associate=self.user, order=o10072, portion_percentage=100)

        # Hunter Douglas #1
        o8 = Order.objects.create(number="SO-1008", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="windows",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o8, payment_date=tznow(), payment_type="CASH", amount=500)
        Commission.objects.create(associate=self.user, order=o8, portion_percentage=100)

        # Hunter Douglas #2
        o10081 = Order.objects.create(number="SO-10081", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=2000, store=self.sac_store, order_type="windows",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o10081, payment_date=tznow(), payment_type="CASH", amount=1000)
        Commission.objects.create(associate=self.user2, order=o10081, portion_percentage=50)
        Commission.objects.create(associate=self.user3, order=o10081, portion_percentage=50)

        # Hunter Douglas Quote
        o10083 = Order.objects.create(number="SO-10083", order_date=tznow(), customer=self.cust, status='QUOT',
                                  subtotal_after_discount=2000, store=self.sac_store, order_type="windows",
                                  bonus_elig_date=tznow(), is_quote=True)
        Commission.objects.create(associate=self.user2, order=o10083, portion_percentage=50)

        # Cabinetry #1
        o9 = Order.objects.create(number="SO-1009", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="cabinetry",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o9, payment_date=tznow(), payment_type="CASH", amount=1000)
        Commission.objects.create(associate=self.user2, order=o9, portion_percentage=100)

        # Cabinetry #2
        o10091 = Order.objects.create(number="SO-10091", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="cabinetry",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o10091, payment_date=tznow(), payment_type="CASH", amount=1000)
        Commission.objects.create(associate=self.user, order=o10091, portion_percentage=50)
        Commission.objects.create(associate=self.user3, order=o10091, portion_percentage=50)

        # Cabinetry Quote
        o10092 = Order.objects.create(number="SO-10092", order_date=tznow(), customer=self.cust, status='QUOT',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="cabinetry",
                                  bonus_elig_date=tznow(), is_quote=True)
        Commission.objects.create(associate=self.user2, order=o10092, portion_percentage=50)


        # Flooring #1
        o1010 = Order.objects.create(number="SO-1010", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="flooring",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o1010, payment_date=tznow(), payment_type="CASH", amount=1000)
        Commission.objects.create(associate=self.user2, order=o1010, portion_percentage=100)

        # Flooring #2
        o1011 = Order.objects.create(number="SO-1011", order_date=tznow(), customer=self.cust, status='S',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="flooring",
                                  bonus_elig_date=tznow())
        OrderPayment.objects.create(order=o1011, payment_date=tznow(), payment_type="CASH", amount=1000)
        Commission.objects.create(associate=self.user, order=o1011, portion_percentage=50)
        Commission.objects.create(associate=self.user3, order=o1011, portion_percentage=50)

        # Flooring Quote
        o1012 = Order.objects.create(number="SO-1012", order_date=tznow(), customer=self.cust, status='QUOT',
                                  subtotal_after_discount=1000, store=self.sac_store, order_type="flooring",
                                  bonus_elig_date=tznow(), is_quote=True)
        Commission.objects.create(associate=self.user, order=o1012, portion_percentage=100)

def tznow():
    return timezone.now()
