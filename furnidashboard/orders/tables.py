import collections

import django_tables2 as tables
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.conf import settings
from django_tables2.utils import A  # accessor
from .models import Order, OrderDelivery
from core.tables import CustomTextLinkColumn, DollarAmountColumn
from core.utils import dollars


class OrderTable(tables.Table):
    class Meta:
        model = Order
        order_by = ("-order_date", "-number")
        attrs = {"class": "format-responsive-table"}
        fields = (
            "number", "order_date", "status", "order_type", "customer", "subtotal_after_discount", "grand_total",
            "balance_due",
            )
        row_attrs = {
            'data-order-status': lambda record: record.status,
            'data-balance-due': lambda record: "y" if record.balance_due > 1 else "n"
        }
        template_name = 'core/django_tables_bootstrap_responsive.html'

    number = tables.Column(verbose_name="Order #", attrs={"td": {"data-title": "Order #"}})
    order_date = tables.DateColumn(format=settings.DATE_FORMAT_STANDARD, attrs={"td": {"data-title": "Ordered Date"}})
    status = tables.Column(verbose_name="Status", attrs={"td": {"data-title": "Status"}})
    customer = tables.Column(verbose_name="Customer", attrs={"td": {"data-title": "Customer"}})
    subtotal_after_discount = DollarAmountColumn(verbose_name="Subtotal", attrs={"td": {"data-title": "Subtotal"}})
    grand_total = DollarAmountColumn(orderable=False, attrs={"td": {"data-title": "Grand Total"}})
    balance_due = DollarAmountColumn(verbose_name="Balance Due", orderable=False, attrs={"td": {"data-title": "Balance Due", "class":"balance_due"}})
    order_type = tables.Column(orderable=False, verbose_name="Order Type", attrs={"td": {"data-title": "Order Type"}})

    def render_number(self, value, record):
        output = '''<div class="col-data">
                            <div class="primary">
                                <span class="dropdown">
                                    <button id="quoteBtnMIEDS129106" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default btn-action-button">
                                        <span class="row-icon-type">
                                            <i class="fa fa-truck"></i>
                                        </span>
                                        <!--<span class="status-circle status-o"></span>-->
                                        <span class="quote-number" title="Click for more info">{0}</span>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" >
                                        <li>
                                            <a href="{1}" class="process_quote_m2 ">
                                                <i class="icon fa fa-edit"></i> Edit order
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{2}" class="view_details ">
                                                <i class="icon fa fa-file-alt"></i> View details
                                            </a>
                                        </li>
                                    </ul>
                                </span>
                            </div>
                        </div>'''
        output = output.format(str(record.number), reverse('order_edit', kwargs={'pk': record.pk}),
                               record.get_absolute_url())
        return mark_safe(output)

    def render_order_type(self, value, record):
        value = str(value)

        value += '''
        <br/><div class="text-small">
            <i class="fa fa-user icon-text icon-fixed-width"></i>{0}
        </div>
        '''.format(", ".join(
            [comm.associate.first_name for comm in record.commissions.all()]
        ))
        
        if self.__class__.__name__ == "OrderTable":
            counts = collections.Counter([i.get_vendor_display() for i in record.ordered_items.all()])
            value += '''
                <div class="text-small text-muted">{}</div>
                '''.format("<br> ".join(
                    ["<i class=\"fa fa-shopping-cart icon-text icon-fixed-width\"></i>{}: {}".format(vendor, item_count) 
                        for vendor, item_count in counts.most_common()]
                ))

        return mark_safe(value)

    def render_status(self, value, record):
        output = '<a href="{0}" class="order_status ">{1}</a>'.format(record.get_absolute_url(), value)
        return mark_safe(output)

    def render_customer(self, value, record):
        output = '<a href="{0}" class="order_status ">{1}</a>'.format(record.customer.get_absolute_url(), value)
        output += '''
                <br/><div class="text-small referral-text">
                    <span class="text-muted"> Referral: </span>{0}
                </div>
                '''.format(record.get_referral_display())

        return mark_safe(output)


class DeliveredOrdersTable(OrderTable):
    delivered_date = tables.DateColumn(format=settings.DATE_FORMAT_STANDARD, verbose_name="Delivered On")

    class Meta(OrderTable.Meta):
        fields = (
            "number", "order_date", "status", "order_type", "delivered_date", "order_type", "customer",
            "subtotal_after_discount", "grand_total", "balance_due",
        )


class UnplacedOrdersTable(OrderTable):
    ordered_items = tables.Column(orderable=False, verbose_name="Item Summary", attrs={"td": {"data-title": "Item Summary"}})

    @staticmethod
    def render_ordered_items(value, record):
        counts = collections.Counter([i.get_vendor_display() + "/" + i.get_status_display() for i in record.ordered_items.all()])
        output = '''
        <span>{}</span>
        '''.format("<br> ".join(
            ["{}: {}".format(vendor, item_count) for vendor, item_count in counts.most_common()]
        ))

        return mark_safe(output)

    class Meta(OrderTable.Meta):
        fields = (
            "number", "order_date", "status", "order_type", "ordered_items", "customer", "subtotal_after_discount", "grand_total",
            "balance_due",
        )


class SalesByAssociateTable(tables.Table):
    associate = tables.Column(orderable=False, attrs={'td': {'class': 'associate'}}, footer="Total:")
    sales = DollarAmountColumn(verbose_name="Sales (Subtotal after discount)",
                               footer=lambda table: dollars(sum(x["sales"] for x in table.data)))

    class Meta:
        order_by = '-sales'
        attrs = {"class": "sales-table", "data-table-class": "sales-table"}
        template_name = 'core/django_tables_responsive.html'


class SalesByAssocSalesTable(tables.Table):
    order_number = tables.Column(verbose_name="Order #")
    order_status = tables.Column(verbose_name="Order Status")
    order_date = tables.Column(verbose_name="Order date")
    item = tables.Column(verbose_name="Sold items")
    order_subtotal = DollarAmountColumn(verbose_name="Order Subtotal")
    amount = DollarAmountColumn(verbose_name="Sale Portion")
    order_balance_due = DollarAmountColumn(verbose_name="Order Balance Due")
    # eligible_amount = DollarAmountColumn(verbose_name="Commission/ Bonus Eligible Amount")
    # commissions_paid = DollarAmountColumn(verbose_name="Commissions Paid")
    # commissions_pending = DollarAmountColumn(verbose_name="Estimated Future Commissions")
    # commissions_due = DollarAmountColumn(verbose_name="Current Commissions Due")
    estimated_commissions = DollarAmountColumn(verbose_name="Estimated Commissions")
    claimed_commissions = DollarAmountColumn(verbose_name="Claimed Amount")
    commission_status = tables.Column("Commission Status")
    bonus_elig_date = tables.Column("Bonus Elig Date")
    order_pk = CustomTextLinkColumn('order_detail', args=[A('order_pk')], custom_text="Details", orderable=False,
                                    verbose_name="Actions")

    class Meta:
        order_by = '-order_date'
        # attrs = {"class": "paleblue"}
        template_name = 'core/django_tables_responsive.html'


class SalesByAssociateWithBonusTable(tables.Table):
    associate = tables.Column(orderable=False)
    # sales = DollarAmountColumn(verbose_name="Total Sales")
    sales = DollarAmountColumn(verbose_name="Bonus Eligible Sales")
    # eligible_amount = DollarAmountColumn(verbose_name="Eligible Amount")
    # protection_plan_bonus = DollarAmountColumn(verbose_name="Guardian Protection Plan Bonus")
    bonus = DollarAmountColumn(verbose_name="Bonus amount")

    class Meta:
        order_by = '-sales'
        # attrs = {'class': 'paleblue'}
        template_name = 'core/django_tables_responsive.html'


class ProtectionPlanBonusTable(tables.Table):
    associate = tables.Column(orderable=False)
    protection_plan_bonus = DollarAmountColumn(verbose_name="Guardian Bonus")
    protection_plan_extra_bonus = DollarAmountColumn(verbose_name="Additional Bonus")

    class Meta:
        order_by = '-protection_plan_bonus'
        # attrs = {'class': 'paleblue'}
        template_name = 'core/django_tables_responsive.html'


class ProtectionPlanByAssociateDetailsTable(tables.Table):
    order_number = tables.Column(verbose_name="Order #")
    order_status = tables.Column(verbose_name="Order Status")
    order_date = tables.Column(verbose_name="Order date")
    item = tables.Column(verbose_name="Sold items")
    order_subtotal = DollarAmountColumn(verbose_name="Order Subtotal")
    amount = DollarAmountColumn(verbose_name="Sale Portion")
    order_balance_due = DollarAmountColumn(verbose_name="Order Balance Due")
    protection_plan_price = DollarAmountColumn(verbose_name="Guardian Protection Plan Price")
    protection_plan_bonus = DollarAmountColumn(verbose_name="Guardian Bonus")
    order_pk = CustomTextLinkColumn('order_detail', args=[A('order_pk')], custom_text="Details", orderable=False,
                                    verbose_name="Actions")

    class Meta:
        order_by = '-order_date'
        # attrs = {"class": "paleblue"}
        template_name = 'core/django_tables_responsive.html'


class CommissionsTable(tables.Table):
    commission_status = tables.Column('Commission Status', attrs={'td': {'class': 'commission_status'}})
    associate = tables.Column(orderable=False)  # AssociateNameColumn(verbose_name="Associate")
    order = tables.Column()
    customer = tables.Column()
    order_date = tables.DateColumn(format=settings.DATE_FORMAT_STANDARD)
    order_status = tables.Column()
    delivered_date = tables.DateColumn(format=settings.DATE_FORMAT_STANDARD, verbose_name="Delivered Date")
    order_subtotal = DollarAmountColumn('Subtotal')
    sale_portion = DollarAmountColumn('Sale Portion')
    order_grandtotal = DollarAmountColumn('Grand Total')
    order_balance_due = DollarAmountColumn('Balance Due')
    amount = DollarAmountColumn('Amount Claimed')
    paid_date = tables.DateColumn(format=settings.DATE_FORMAT_STANDARD)
    claim_action = tables.Column('Action')

    def render_order(self, value):
        return mark_safe('<a href="{0}">{1}</a><br/>{2}'.format(value.get_absolute_url(), str(value), value.get_order_type_display()))

    class Meta:
        order_by = ('associate', '-order_date', 'order')
        # attrs = {"class": "paleblue"}
        row_attrs = {
            'data-commission-status': lambda record: record['commission_status'],
            'data-manager-commission-flag': lambda record: record['manager_commission_flag']
        }
        template_name = 'core/django_tables_responsive.html'


class DeliveriesTable(tables.Table):
    order = tables.LinkColumn('order_detail', args=[A('order.pk')])
    pk = CustomTextLinkColumn('delivery_detail', args=[A('pk')], custom_text="Detail", orderable=False,
                              verbose_name="Actions")
    customer = tables.Column(accessor="order.customer", verbose_name="Customer")
    delivery_cost = DollarAmountColumn()

    class Meta:
        model = OrderDelivery
        # attrs = {"class": "paleblue"}
        fields = ("order", "customer", "scheduled_delivery_date", "delivery_type", "delivery_cost", 'delivered_date')
        template_name = 'core/django_tables_responsive.html'


class SalesTotalsTable(tables.Table):
    item = tables.Column(verbose_name="-", orderable=False)
    hq = tables.Column(verbose_name="HQ/Sacramento", orderable=False)
    fnt = tables.Column(verbose_name="Roseville", orderable=False)
    total = tables.Column(verbose_name="Stores Total", orderable=False)

    class Meta:
        # attrs = {"class": "paleblue"}
        template_name = 'core/django_tables_responsive.html'
