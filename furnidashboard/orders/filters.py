import django_filters as filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Div, Field
from django.contrib.auth import get_user_model
from django.db.models import Q

from commissions.filters import DATEPICKER_OPTIONS, DATE_FORMAT
from orders.forms import CustomDatePickerInput
from .models import Order

# CHOICES_FOR_STATUS_FILTER = [('', '--Any--')]
# CHOICES_FOR_STATUS_FILTER.extend(list(Order.ORDER_STATUSES))
EXCLUDED_ORDER_STATUSES = ['H', 'P', 'T', 'X', 'I', 'C']
CHOICES_FOR_STATUS_FILTER = filter(
    lambda c: c[0] not in EXCLUDED_ORDER_STATUSES, list(Order.ORDER_STATUSES))


class OrderFilter(filters.FilterSet):
    status = filters.ChoiceFilter(choices=CHOICES_FOR_STATUS_FILTER)

    def __init__(self, *args, **kwargs):
        super(OrderFilter, self).__init__(*args, **kwargs)

        user_model = get_user_model()

        self.filters['commissions__associate'].field.queryset = user_model.objects \
            .filter(Q(is_active=True)) \
            .filter(Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers")) \
            .order_by('username') \
            .distinct()
        self.filters['commissions__associate'].field.label = 'Associate'

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            Div(
                'order_type',
                'status',
                Field('commissions__associate'),
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )

    class Meta:
        model = Order
        fields = ['order_type', 'status', 'commissions__associate']


class DeliveredOrderFilter(OrderFilter):
    div_attrs = {'class': 'input-group date'}
    attrs = {'class': 'form-control'}
    delivered_date_from = filters.DateFilter(field_name='delivered_date', label='Delivered Date From', lookup_expr='gte',
                                             widget=CustomDatePickerInput(attrs=attrs, div_attrs=div_attrs, format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    delivered_date_to = filters.DateFilter(field_name='delivered_date', label='Delivered Date To', lookup_expr='lte',
                                           widget=CustomDatePickerInput(attrs=attrs, div_attrs=div_attrs, format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    order_date_from = filters.DateFilter(field_name='order_date', label='Order Date From', lookup_expr='gte',
                                         widget=CustomDatePickerInput(div_attrs=div_attrs,
                                                                      format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    order_date_to = filters.DateFilter(field_name='order_date', label='Order Date To', lookup_expr='lte',
                                       widget=CustomDatePickerInput(div_attrs=div_attrs,
                                                                    format=DATE_FORMAT, options=DATEPICKER_OPTIONS))

    def __init__(self, *args, **kwargs):
        super(DeliveredOrderFilter, self).__init__(*args, **kwargs)

        self.helper.field_template = None
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True

        self.helper.layout = Layout(
            Div(
                Field('delivered_date_from', css_class="form-control",
                      template="core/widgets/date.html"),
                'delivered_date_to',
                'order_date_from',
                'order_date_to',
                'order_type',
                'associate',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )
