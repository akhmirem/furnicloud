from django import template
from core.utils import dollars

register = template.Library()

register.filter('dollars', dollars)


@register.filter(name='lookup')
def lookup(dict, index):
    if index in dict:
        return dict[index]
    return ''
