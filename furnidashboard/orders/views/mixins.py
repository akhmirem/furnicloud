import datetime

from crispy_forms.helper import FormHelper
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Q
from django.utils import timezone
from django.views.generic import ListView
from django_tables2 import RequestConfig

from core.utils import get_current_month_date_range
from orders.filters import OrderFilter
from orders.tables import OrderTable


class FilteredTableMixin(object):
    formhelper_class = FormHelper
    context_filter_name = 'filter'
    context_table_name = 'table'
    model = None
    table_class = OrderTable
    context_object_name = "order_list"
    table_paginate_by = None
    filter_class = OrderFilter
    filter_form_id = 'order-filter'
    include_cabinetry_orders = False

    def get_queryset(self, **kwargs):
        qs = super(FilteredTableMixin, self).get_queryset(**kwargs)
        qs = qs.prefetch_related('payments'). \
            select_related('store'). \
            select_related('customer'). \
            prefetch_related('commissions'). \
            prefetch_related('commissions__associate'). \
            distinct()

        self.setup_filter(queryset=qs)
        qs = self.filter.qs

        if not self.include_cabinetry_orders:
            qs = qs.exclude(order_type__in=('cabinetry', 'cabinetry_quote'))

        return qs

    def setup_filter(self, **kwargs):
        self.filter = self.filter_class(self.request.GET, queryset=kwargs['queryset'])
        # self.filter.helper = self.formhelper_class()
        self.filter.helper.form_id = self.filter_form_id
        # self.filter.helper.form_class = "blueForms, well"
        self.filter.helper.form_method = "get"
        # self.filter.helper.add_input(Submit('submit', 'Submit'))

    def get_table(self, **kwargs):
        try:
            page = self.kwargs['page']
        except KeyError:
            page = 1
        options = {'paginate': {'page': page, 'per_page': self.table_paginate_by}}
        table_class = self.table_class
        table = table_class(**kwargs)
        RequestConfig(self.request, **options).configure(table)
        return table

    def get_context_data(self, **kwargs):
        context = super(FilteredTableMixin, self).get_context_data(**kwargs)
        table = self.get_table(data=context[self.context_object_name])
        context[self.context_table_name] = table
        context[self.context_filter_name] = self.filter
        return context

