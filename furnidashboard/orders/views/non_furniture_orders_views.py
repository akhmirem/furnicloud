import datetime
import logging

from django.urls import reverse
from django.views.generic import MonthArchiveView

from core.mixins import PermissionRequiredMixin
from orders.models import Order
from orders.views.mixins import FilteredTableMixin

logger = logging.getLogger('furnicloud')


class NonFurnitureOrderMonthArchiveTableView(PermissionRequiredMixin, FilteredTableMixin, MonthArchiveView):
    model = Order
    table_paginate_by = 50

    # archive view specific fields
    date_field = "order_date"
    make_object_list = True
    allow_future = True
    allow_empty = True
    template_name = "orders/non_furniture_orders_list.html"
    month_format = '%b'
    last_date_of_month = None
    include_cabinetry_orders = True
    order_types = ['cabinetry', 'cabinetry_quote']
    title = ""

    required_permissions = (
        'orders.view_orders',
    )

    def get_queryset(self, **kwargs):
        qs = super(NonFurnitureOrderMonthArchiveTableView, self).get_queryset(**kwargs)
        qs = qs.filter(order_type__in=self.order_types)
        return qs

    def get_context_data(self, **kwargs):
        context = super(NonFurnitureOrderMonthArchiveTableView, self).get_context_data(**kwargs)

        # links to other months data
        context['years_list'] = sorted([str(y) for y in range(2016, datetime.datetime.now().year + 1)], reverse=True)
        context['months_list'] = [['1', 'January'],
                                  ['2', 'February'],
                                  ['3', 'March'],
                                  ['4', 'April'],
                                  ['5', 'May'],
                                  ['6', 'June'],
                                  ['7', 'July'],
                                  ['8', 'August'],
                                  ['9', 'September'],
                                  ['10', 'October'],
                                  ['11', 'November'],
                                  ['12', 'December']]
        context['current_year'] = self.get_year()
        context['current_month'] = self.get_month()

        context["title"] = self.title

        return context
