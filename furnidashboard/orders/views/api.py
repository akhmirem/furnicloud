from orders.serializers import OrderSerializer
from rest_framework import viewsets, filters, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from orders.models import OrderItem, Order
import logging

logger = logging.getLogger('furnicloud')

class OrderApiViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication,)
    #permission_classes = (IsAuthenticated,)

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    #filter_backends = (filters.DjangoFilterBackend,)
    #filter_class = StoreTrafficEntryFilter
    # filter_fields = ('associate__first_name', 'order__number', 'paid')'