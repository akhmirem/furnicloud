import logging

from django.contrib import messages
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import DetailView, UpdateView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import CreateView, DeleteView

import core.utils as utils
import orders.forms as order_forms
import orders.utils as order_utils
from claims.models import Claim
from commissions.models import Commission
from core.mixins import PermissionRequiredMixin
from customers.models import Customer
from orders.forms import OrderForm, CustomerFormSet, get_deliveries_formset, get_commissions_formset, \
    OrderAttachmentFormSet, \
    get_order_issues_formset
from orders.models import Order, OrderItem, OrderPayment, OrderDelivery, OrderAttachment, OrderIssue, OrderFinancing, \
    OrderItemProtectionPlan

logger = logging.getLogger("furnicloud")

protection_plan_orphan_info_error = "Guardian Protection Plan info was entered BUT the 'Protection plan purchased' " \
                                    "checkbox is not selected. Either check the protection plan checkbox or delete Guardian " \
                                    "Protection Plan info by selecting the 'Delete' checkbox."
order_financing_orphan_info_error = "Order financing information has been entered but 'Order financing' option " \
                                    "is not selected. Either check the order financing checkbox or delete Order " \
                                    "financing information by selecting the 'Delete' checkbox."
err_saving_order_info_msg = "Error saving the order information. Please check order tabs to see the problem."

err_invalid_delivered_status_msg = "Cannot update status to 'Delivered' because there are no Deliveries entered " \
                                   "-OR- Delivery Date is missing."
err_invalid_delivered_status_msg2 = "Cannot change order status to 'Delivered' because some items are not received " \
                                    "or in-stock."
err_invalid_order_quote_status_msg = "Cannot set order status to 'Quotation' because there is payment " \
                                     "information entered for order. Remove payment information and try to save again."
err_invalid_new_order_status_msg = "Newly created order status must be either 'New In Stock', 'New SO', or 'Quotation'! " \
                                   "Order must be saved before making further changes."
err_missing_customer_info_msg = "Please select existing customer or fill in new customer information!"
err_blank_order_status_msg = "Order status cannot be blank!"

error_message_tag = "alert alert-danger"


class OrderDetailView(PermissionRequiredMixin, DetailView):
    model = Order
    context_object_name = "order"
    template_name = "orders/order_detail.html"

    required_permissions = (
        'orders.view_orders',
    )

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        context['item_pending_statuses'] = ('P', 'O', 'R')
        order = context[self.context_object_name]
        return context


class OrderHistoryView(PermissionRequiredMixin, DetailView):
    model = Order
    context_object_name = "order"
    template_name = "orders/order_history.html"

    required_permissions = (
        'orders.view_orders',
    )

    def get_context_data(self, **kwargs):
        context = super(OrderHistoryView, self).get_context_data(**kwargs)
        order = context[self.context_object_name]
        audit_log = order.audit_log.order_by("-modified").all()
        context['audit_log'] = audit_log
        field_names = [f.name for f in Order._meta.get_fields()]
        # context['audit_log_objects'] = [a.f for a in audit_log for f in field_names]

        audit_history = []
        order_attrs = ['tax', 'number', 'comments', 'store', 'status', 'financing_option', 'protection_plan',
                       'order_date', 'customer', 'subtotal_after_discount', 'shipping']
        for a in audit_log:
            row = {'action': a.get_action_type_display(),
                   'action_date': a.action_date,
                   'action_user': a.action_user,
                   'data': a.object_state,
                   }
            audit_history.append(row)

        context['audit_log_objects'] = audit_history

        # Item History
        # -----------------------------------------------------------------
        item_history_qs = OrderItem.audit_log.filter(
            order_id=order.pk).order_by("id", "-action_date")
        item_history = []
        num = 0
        prev = 0
        for item_log in item_history_qs:
            if item_log.object_state.id != prev:
                num += 1
                prev = item_log.object_state.id
            item_history.append({'no': num, 'data': item_log})
        context['item_history'] = item_history

        # Payment History
        # -----------------------------------------------------------------
        payment_history_qs = OrderPayment.audit_log.filter(
            order_id=order.pk).order_by("id", "-action_date")
        payment_history = []
        num = 0
        prev = 0
        for payment_log in payment_history_qs:
            if payment_log.object_state.id != prev:
                num += 1
                prev = payment_log.object_state.id
            payment_history.append({'no': num, 'data': payment_log})
        context['payment_history'] = payment_history

        # Commission Update History
        # -----------------------------------------------------------------
        commission_history_qs = Commission.audit_log.filter(order_id=order.pk) \
            .filter(manager_commission_flag=False) \
            .order_by("id", "-action_date")
        commission_history = []
        num = 0
        prev = 0
        for commission_log in commission_history_qs:
            if commission_log.object_state.id != prev:
                num += 1
                prev = commission_log.object_state.id
            commission_history.append({'no': num, 'data': commission_log})
        context['commission_history'] = commission_history

        return context


# -----------------------------------------------------------------------

class OrderDeleteView(PermissionRequiredMixin, DeleteView):
    model = Order
    context_object_name = "order"
    success_url = reverse_lazy("order_list")

    required_permissions = (
        'orders.delete_order',
    )

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()

        # OrderPayment.objects.
        OrderPayment.objects.filter(order_id=self.object.pk).delete()
        OrderItem.objects.filter(order_id=self.object.pk).delete()
        OrderDelivery.objects.filter(order_id=self.object.pk).delete()
        OrderAttachment.objects.filter(order_id=self.object.pk).delete()
        OrderIssue.objects.filter(order_id=self.object.pk).delete()
        OrderFinancing.objects.filter(order_id=self.object.pk).delete()
        OrderItemProtectionPlan.objects.filter(
            order_id=self.object.pk).delete()
        Commission.objects.filter(order_id=self.object.pk).delete()
        Claim.objects.filter(order_ref_id=self.object.pk).delete()

        self.object.delete()
        return HttpResponseRedirect(success_url)


# -----------------------------------------------------------------------

class OrderUpdateView(PermissionRequiredMixin, UpdateView):
    model = Order
    context_object_name = "order"
    template_name = "orders/order_update.html"
    form_class = OrderForm
    orig_status = ''

    required_permissions = (
        'orders.change_order',
    )

    def dispatch(self, request, *args, **kwargs):
        order = self.get_object()

        # prevent updating Delivered orders. Manager's authority required.
        if order.status in ('D', 'C'):
            is_manager = request.user.groups.filter(Q(name="managers")).exists()
            if not is_manager:
                messages.error(
                    request,
                    'Delivered orders can be updated by manager only!',
                    extra_tags="alert alert-danger"
                )
                return redirect(reverse('order_detail', kwargs={'pk': order.pk}))

        return super(OrderUpdateView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """
        Handle GET requests and instantiate blank version of the form
        and it's inline formsets
        """
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        self.request = request

        if not request.user.has_perm('orders.update_status'):
            # disable order status for staff person without privilege
            form.fields['status'].widget.attrs['readonly'] = 'readonly'

        customer_form = CustomerFormSet(queryset=Customer.objects.none(), prefix="customers")

        # if no commissions were saved for this order, add a blank form
        extra = 1 if self.object.commissions.regular().count() == 0 else 0
        special_commissions_formset = get_commissions_formset(extra=extra, max_num=3, request=self.request)
        commissions_form = special_commissions_formset(instance=self.object, prefix="commissions",
                                                       queryset=Commission.objects.regular().filter(order=self.object))

        # if no commissions were saved for this order, add a blank form
        extra = 1 if self.object.payments.count() == 0 else 0
        order_payments_formset = order_forms.get_payments_formset(extra=extra, max_num=10, request=self.request)
        payments_form = order_payments_formset(instance=self.object, prefix="payments")

        extra = 1 if self.object.orderdelivery_set.count() == 0 else 0
        deliveries_formset = get_deliveries_formset(extra=extra, max_num=100, request=self.request)
        delivery_form = deliveries_formset(instance=self.object, prefix="deliveries")

        # prevent empty form showing up if no items were recorded for the order
        # specify at least 1 extra of no items are set
        extra = 1 if self.object.ordered_items.count() == 0 else 0
        sold_items_formset = order_forms.get_item_formset(
            extra=extra, max_num=100, order_type=self.object.order_type, is_quote=self.object.is_quote)
        items_form = sold_items_formset(instance=self.object, prefix="ordered_items")

        # attachments
        attachment_form = OrderAttachmentFormSet(instance=self.object, prefix="attachments")

        # issues form
        extra = 1 if self.object.orderissue_set.count() == 0 else 0
        order_issues_formset = get_order_issues_formset(extra=extra, max_num=100)
        issues_form = order_issues_formset(instance=self.object, prefix='issues')

        # notes form
        extra = 1 if self.object.notes.count() == 0 else 0
        order_notes_formset = order_forms.get_order_notes_formset(extra=extra, max_num=100)
        notes_form = order_notes_formset(instance=self.object, prefix='notes')

        # protection plan form
        protection_plan_form = order_forms.ProtectionPlanFormSet(instance=self.object, prefix="protectionPlan")

        # special financing option form
        order_financing_form = order_forms.OrderFinancingFormSet(instance=self.object, prefix="financing")

        extra_forms = {
            'form': form,
            'customer_form': customer_form,
            'items_form': items_form,
            'commissions_form': commissions_form,
            'payments_form': payments_form,
            'delivery_form': delivery_form,
            'attachment_form': attachment_form,
            'issues_form': issues_form,
            'protection_plan_form': protection_plan_form,
            'financing_form': order_financing_form,
            'notes_form': notes_form,
            # 'tracking_form': tracking_form,
        }
        context = self.get_context_data()
        context.update(extra_forms)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiates a form instance and its inline formsets with the passed POST variables
        and then checks them for validity
        """
        self.object = self.get_object()
        self.orig_status = self.object.status
        form = self.get_form(self.get_form_class())
        self.request = request
        logger.debug('Order prev status: ' + str(self.object.status))
        logger.debug('Order Update: order type : ' + str(self.object.order_type))

        if not request.user.has_perm('orders.update_status'):
            # disable order status for staff person without privilege
            form.fields['status'].widget.attrs['readonly'] = 'readonly'

        customer_form = CustomerFormSet(self.request.POST, self.request.FILES, prefix="customers")

        # if no commissions were saved for this order, add a blank form
        extra = 1 if self.object.commissions.regular().count() == 0 else 0
        special_commission_formset = get_commissions_formset(extra=extra, max_num=3, request=self.request)
        commissions_form = special_commission_formset(self.request.POST, self.request.FILES, instance=self.object,
                                                      prefix="commissions")

        extra = 1 if self.object.payments.count() == 0 else 0
        order_payments_formset = order_forms.get_payments_formset(extra=extra, max_num=10, request=self.request)
        payments_form = order_payments_formset(self.request.POST, self.request.FILES, instance=self.object,
                                               prefix="payments")

        extra = 1 if self.object.ordered_items.count() == 0 else 0
        items_formset = order_forms.get_item_formset(extra=extra, order_type=self.object.order_type, is_quote=self.object.is_quote)
        items_form = items_formset(self.request.POST, self.request.FILES, instance=self.object, prefix="ordered_items")

        deliveries_formset = get_deliveries_formset(extra=0, max_num=100, request=self.request)
        delivery_form = deliveries_formset(self.request.POST, self.request.FILES, instance=self.object, prefix="deliveries")

        attachment_form = OrderAttachmentFormSet(self.request.POST, self.request.FILES, instance=self.object, prefix="attachments")

        order_issues_formset = get_order_issues_formset()
        issues_form = order_issues_formset(self.request.POST, self.request.FILES, instance=self.object, prefix="issues")

        order_notes_formset = order_forms.get_order_notes_formset()
        notes_form = order_notes_formset(self.request.POST, self.request.FILES, instance=self.object, prefix="notes")

        protection_plan_form = order_forms.ProtectionPlanFormSet(self.request.POST, self.request.FILES,
                                                                 instance=self.object, prefix="protectionPlan")
        order_financing_form = order_forms.OrderFinancingFormSet(self.request.POST, self.request.FILES,
                                                                 instance=self.object, prefix="financing")

        forms = {
            'form': form,
            'customer_form': customer_form,
            'items_form': items_form,
            'commissions_form': commissions_form,
            'delivery_form': delivery_form,
            'attachment_form': attachment_form,
            'issues_form': issues_form,
            'protection_plan_form': protection_plan_form,
            'financing_form': order_financing_form,
            'payments_form': payments_form,
            'notes_form': notes_form,
        }

        # check if forms are valid
        forms_valid = form.is_valid()

        if not forms_valid:
            logger.debug("Order form validation failed")

        if forms_valid and payments_form.has_changed():
            forms_valid = payments_form.is_valid()
            if not forms_valid:
                messages.error(self.request, "Error saving payment information", error_message_tag)
                logger.debug("Payments form validation failed")

        if forms_valid and commissions_form.has_changed():
            forms_valid = commissions_form.is_valid()
            if not forms_valid:
                logger.debug("Commissions form validation failed")
                logger.debug(commissions_form.errors)

        if forms_valid and items_form.has_changed():
            forms_valid = items_form.is_valid()
            if not forms_valid:
                logger.debug("Items form validation failed")

        if forms_valid:
            for form in [f for f in delivery_form.forms if f.has_changed()]:
                forms_valid = form.is_valid()
                if not forms_valid:
                    logger.debug("Delivery form validation failed")
                    break

        if forms_valid and attachment_form.has_changed():
            forms_valid = attachment_form.is_valid()
            if not forms_valid:
                logger.debug("Attachment form validation failed")

        if forms_valid and protection_plan_form.has_changed():
            forms_valid = protection_plan_form.is_valid()
            if not forms_valid:
                logger.debug("Protection Plan form validation failed")

        if forms_valid and order_financing_form.has_changed():
            forms_valid = order_financing_form.is_valid()
            if not forms_valid:
                logger.debug("Financing form validation failed")

        if forms_valid:
            return self.form_valid(**forms)
        else:
            messages.add_message(self.request, messages.ERROR,
                                 err_saving_order_info_msg, extra_tags=error_message_tag)
            return self.form_invalid(**forms)

    def form_valid(self, *args, **kwargs):
        """
        Called if all forms are valid. Creates an Order instance along with associated Customer
        and Commission instances and then redirects to a success page
        """
        form = kwargs['form']
        customer_form = kwargs['customer_form']
        items_form = kwargs['items_form']
        commissions_form = kwargs['commissions_form']
        delivery_form = kwargs['delivery_form']
        attachment_form = kwargs['attachment_form']
        issues_form = kwargs.get('issues_form')
        protection_plan_form = kwargs['protection_plan_form']
        order_financing_form = kwargs['financing_form']
        payments_form = kwargs['payments_form']
        notes_form = kwargs['notes_form']

        self.object = form.save(commit=False)
        logger.debug("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        logger.debug("Order ID= {0} | BEFORE UPDATE".format(self.object.pk))

        # flags
        br_valid = True
        new_customer = False
        blank_customer = False

        # validate order status
        new_status = form.cleaned_data['status']
        payment_records = [payment_row for payment_row in payments_form.cleaned_data if not payment_row.get('DELETE', False)]

        last_payment_date = None
        if len(payment_records) > 0:
            try:
                last_payment_date = max(payment_row.get('payment_date') for payment_row in payment_records
                                        if payment_row.get('amount', 0) > 0)
            except:
                last_payment_date = None

        paid_amount = sum([payment_row.get('amount', 0) for payment_row in payment_records])

        if not form.data['status']:
            messages.error(self.request, err_blank_order_status_msg, extra_tags="alert")
            br_valid = False
        else:
            if new_status == 'D':
                if any([f for f in delivery_form if utils.delivery_form_empty(f.cleaned_data) or
                        f.cleaned_data['delivered_date'] is None]):
                    logger.debug(
                        "Order ID= {0} | ERROR:{1}".format(str(self.object.pk), err_invalid_delivered_status_msg))
                    messages.add_message(self.request, messages.ERROR, err_invalid_delivered_status_msg,
                                         extra_tags=error_message_tag)
                    br_valid = False

                if br_valid:
                    for item_row in items_form.cleaned_data:
                        if item_row.get('status') not in ('R', 'D', 'S', 'I'):
                            logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                            err_invalid_delivered_status_msg2))
                            messages.add_message(self.request, messages.ERROR, err_invalid_delivered_status_msg2,
                                                 extra_tags=error_message_tag)
                            br_valid = False

            elif new_status in ('X', 'QUOT'):
                if paid_amount > 0 or paid_amount < 0:
                    logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                    err_invalid_order_quote_status_msg))
                    messages.add_message(self.request, messages.ERROR, err_invalid_order_quote_status_msg,
                                         extra_tags=error_message_tag)
                    br_valid = False

        # validate customer
        if br_valid:
            if form.cleaned_data.get('customer', None) is None:
                if customer_form.is_valid():
                    # check that first and last name are filled
                    try:
                        if not customer_form[0].cleaned_data['first_name'] or \
                                not customer_form[0].cleaned_data['last_name']:
                            if new_status != 'QUOT':
                                logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                                err_missing_customer_info_msg))
                                messages.error(self.request, err_missing_customer_info_msg,
                                               extra_tags=error_message_tag)
                                br_valid = False
                            else:
                                blank_customer = True  # allowing customer to be blank for quotation orders
                        else:
                            new_customer = True
                    except KeyError:
                        if new_status != 'QUOT':
                            logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                            err_missing_customer_info_msg))
                            messages.error(
                                self.request, err_missing_customer_info_msg, extra_tags=error_message_tag)
                            br_valid = False
                        else:
                            blank_customer = True  # allowing customer to be blank for quotation orders
                else:
                    logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                    "Error saving customer information!"))
                    messages.error(
                        self.request, "Error saving customer information!", extra_tags=error_message_tag)
                    br_valid = False

        # validate issues
        if br_valid and not issues_form.is_valid():
            logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                            "Error saving 'order issues' information!"))
            messages.error(
                self.request, "Error saving 'order issues' information!", extra_tags=error_message_tag)
            br_valid = False

        # validate protection plan
        if br_valid and not self.object.protection_plan:
            if not _is_valid_protection_plan(protection_plan_form):
                logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                protection_plan_orphan_info_error))
                messages.error(
                    self.request, protection_plan_orphan_info_error, extra_tags=error_message_tag)
                br_valid = False

        # validate order financing information
        if br_valid and not self.object.financing_option:
            if not _is_valid_financing_plan(order_financing_form):
                logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                order_financing_orphan_info_error))
                messages.error(
                    self.request, order_financing_orphan_info_error, extra_tags=error_message_tag)
                br_valid = False

        # validate notes
        if br_valid and not notes_form.is_valid():
            logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                            "Error saving 'order notes'!"))
            messages.error(
                self.request, "Error saving 'order notes'!", extra_tags=error_message_tag)
            br_valid = False

        # Final check
        if not br_valid:
            logger.debug("Order ID= {0} | ERROR: Failed to save Order Information".format(
                self.object.pk))
            return self.form_invalid(**kwargs)

        total = self.object.subtotal_after_discount + self.object.tax + self.object.shipping
        logger.debug("Order Subtotal = " + str(total))
        logger.debug("Total Paid= " + str(paid_amount))
        logger.debug("Order Balance= " + str(total - paid_amount))
        self.object.current_balance = total - paid_amount
        if abs(total - paid_amount) < 0.01:
            self.object.current_balance = 0.0

        # determine if 30% deposit has been paid
        # and mark order bonus eligibility date
        if total > 0.0 and paid_amount / total >= 0.25:
            logger.debug("Order ID= {0} | Setting Bonus Elig Date = {1}".format(self.object.pk,
                                                                                last_payment_date))
            if self.object.bonus_elig_date is None:
                self.object.bonus_elig_date = last_payment_date

        # check order status again after balance is calculated...
        if new_status in ('S', 'D'):
            if self.object.current_balance > 1:
                br_valid = False
                logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                                'Cannot update status: order balance is not paid '
                                                                'in full!'))
                messages.error(self.request, 'Cannot update status: order balance is not paid in full!',
                               extra_tags=error_message_tag)
                logger.debug("Outstanding balance: " + str(self.object.current_balance))
                return self.form_invalid(**kwargs)

            # auto-populate Delivered date for Order
            if new_status == 'D':  # and self.object.delivered_date is None:
                dt = max([f.cleaned_data['delivered_date'] for f in delivery_form if
                          not f.cleaned_data.get('DELETE', False) and f.cleaned_data['delivered_date']])
                logger.debug("Order ID= {0} | Marking Delivery Date: {1}".format(self.object.pk,
                                                                                 dt.strftime('%Y-&m-%d')))
                self.object.delivered_date = dt

        try:
            with transaction.atomic():

                # save customer
                if new_customer:
                    customer_object = customer_form[0].save()
                    self.object.customer = customer_object
                elif blank_customer:
                    self.object.customer = order_utils.get_default_customer()

                # save order
                self.object.save()

                # save payments
                payments_form.instance = self.object
                payments_form.save()

                # save items
                if items_form.has_changed():
                    items_form.instance = self.object
                    order_items = items_form.save()
                else:
                    order_items = self.object.ordered_items.all()

                # save commissions
                if commissions_form.has_changed():
                    commissions_form.instance = self.object
                    commissions_form.save()

                # save deliveries
                delivery_warning = False
                for del_form in delivery_form:
                    if del_form.has_changed():
                        if del_form.instance.pk or not utils.delivery_form_empty(del_form.cleaned_data):
                            del_form.order = self.object
                            del_form.save()
                            if any(self.object.ordered_items.filter(~Q(status__in=['S', 'R']))):
                                delivery_warning = True

                if delivery_warning:
                    logger.debug("Order ID= {0} | WARNING: {1}".format(str(self.object.pk),
                                                                       "Warning: a delivery has been scheduled for "
                                                                       "this order BUT item(s) are not "
                                                                       "in stock/not delivered. Please check the order status for any issues."))
                    messages.add_message(self.request, messages.ERROR,
                                         "Warning: a delivery has been scheduled for this order BUT item(s) are not "
                                         "in stock/not delivered. Please check the order status for any issues.",
                                         extra_tags="alert alert-warning")

                # save attachments
                if attachment_form.has_changed():
                    attachment_form.instance = self.object
                    attachment_form.save()

                # save issues
                if issues_form.has_changed():
                    issues_form.instance = self.object
                    issues_form.save()

                # save order protection plan form
                if protection_plan_form.has_changed():
                    protection_plan_form.instance = self.object
                    protection_plan_form.save()

                # save order financing option form
                if order_financing_form.has_changed():
                    order_financing_form.instance = self.object
                    order_financing_form.save()

                # save notes
                if notes_form.has_changed():
                    notes_form.instance = self.object
                    notes_form.save()

        except Exception as e:
            logger.debug(
                "Order ID= {0} | ERROR: Failed to save Order Information. Exception: {1}".format(self.object.pk,
                                                                                                 str(e)))
            return self.form_invalid(**kwargs)

        logger.debug("Order ID= " + str(self.object.pk) + " | AFTER UPDATE")
        logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, *args, **kwargs):
        """
        Called if any of the forms on order page is invalid. Returns a response with an invalid form in the context
        """
        context = self.get_context_data()
        context.update(kwargs)

        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(OrderUpdateView, self).get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return self.get_object().get_absolute_url()


# ------------------------------------------------------------------------

class OrderCreateView(PermissionRequiredMixin, CreateView):
    """
    Order Creation view. Presents blank forms for inputting new orders.
    """

    model = Order
    template_name = "orders/order_update.html"
    form_class = OrderForm
    order_type = "stock"
    is_quote = False

    required_permissions = (
        'orders.add_order',
    )

    def extract_additional_kwargs(self, request):
        self.order_type = request.GET.get('order_type', request.POST.get('order_type', None))

        self.is_quote = self.request.GET.get('quote', self.request.POST.get('quote', None))
        if self.is_quote is None:
            self.is_quote = False
        else:
            self.is_quote = str(self.is_quote).lower() in ("yes", "true", "t", "1")

    def get(self, request, *args, **kwargs):
        """
        Handle GET requests and instantiate blank version of the form
        and it's inline formsets
        """

        self.extract_additional_kwargs(request)
        logger.debug("New Order with order type: " + self.order_type + " is quote: " + str(self.is_quote))

        if self.order_type is None:
            raise Exception("Order Type was not specified!")

        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        # scan query string for order #
        if request.GET:
            order_number = request.GET.get('order', '').strip()
            if order_number:
                form.initial['number'] = order_number

        form.initial['order_type'] = self.order_type
        form.initial['is_quote'] = self.is_quote

        # initialize form defaults
        form.fields['status'].widget.attrs['readonly'] = True

        customer_form = CustomerFormSet(queryset=Customer.objects.none(), prefix="customers")

        items_formset = order_forms.get_item_formset(order_type=self.order_type, is_quote=self.is_quote)
        items_form = items_formset(prefix="ordered_items")

        extra = 1
        special_commission_formset = get_commissions_formset(extra=extra, max_num=3, request=self.request)
        commissions_form = special_commission_formset(prefix="commissions")

        order_payments_formset = order_forms.get_payments_formset(extra=extra, max_num=10, request=self.request)
        payments_form = order_payments_formset(prefix="payments")

        protection_plan_form = order_forms.ProtectionPlanFormSet(prefix="protectionPlan")
        order_financing_form = order_forms.OrderFinancingFormSet(prefix="financing")

        order_notes_formset = order_forms.get_order_notes_formset(extra=extra, max_num=100)
        notes_form = order_notes_formset(prefix='notes')

        context = self.get_context_data()
        extra_forms = {
            'form': form,
            'customer_form': customer_form,
            'items_form': items_form,
            'commissions_form': commissions_form,
            'payments_form': payments_form,
            'protection_plan_form': protection_plan_form,
            'financing_form': order_financing_form,
            'notes_form': notes_form,
        }
        context.update(extra_forms)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiates a form instance and its inline formsets with the passed POST variables
        and then checks them for validity
        """

        self.extract_additional_kwargs(request)

        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        # self.order_type = form.data.get('order_type', None)
        logger.debug("Saving Order with order type: " + self.order_type)

        if self.order_type is None:
            raise Exception("Order Type was not specified!")

        # initialize form defaults
        form.fields['status'].widget.attrs['readonly'] = True

        customer_form = CustomerFormSet(self.request.POST, self.request.FILES, prefix="customers")

        items_formset = order_forms.get_item_formset(order_type=self.order_type, is_quote=self.is_quote)
        items_form = items_formset(self.request.POST, self.request.FILES, prefix="ordered_items")

        special_commissions_formset = get_commissions_formset(extra=1, max_num=3, request=self.request)
        commissions_form = special_commissions_formset(self.request.POST, self.request.FILES, prefix="commissions")

        order_payments_formset = order_forms.get_payments_formset(extra=1, max_num=10, request=self.request)
        payments_form = order_payments_formset(self.request.POST, self.request.FILES, prefix="payments")

        protection_plan_form = order_forms.ProtectionPlanFormSet(self.request.POST, self.request.FILES,
                                                                 prefix="protectionPlan")
        order_financing_form = order_forms.OrderFinancingFormSet(self.request.POST, self.request.FILES,
                                                                 prefix="financing")

        order_notes_formset = order_forms.get_order_notes_formset(extra=1, max_num=100)
        notes_form = order_notes_formset(self.request.POST, self.request.FILES, prefix='notes')

        forms = {
            'form': form,
            'customer_form': customer_form,
            'items_form': items_form,
            'commissions_form': commissions_form,
            'payments_form': payments_form,
            'protection_plan_form': protection_plan_form,
            'financing_form': order_financing_form,
            'notes_form': notes_form,
        }

        # check if forms are valid
        forms_valid = form.is_valid()

        if not forms_valid:
            logger.debug("Order form validation failed")

        if forms_valid and payments_form.has_changed():
            forms_valid = payments_form.is_valid()
            if not forms_valid:
                messages.error(self.request, "Error saving payment information", error_message_tag)
                logger.debug("Payments form validation failed")

        if forms_valid and commissions_form.has_changed():
            forms_valid = commissions_form.is_valid()
            if not forms_valid:
                logger.debug("Commissions form validation failed")

        if forms_valid and items_form.has_changed():
            forms_valid = items_form.is_valid()
            if not forms_valid:
                logger.debug("Items form validation failed")

        if forms_valid and protection_plan_form.has_changed():
            forms_valid = protection_plan_form.is_valid()
            if not forms_valid:
                logger.debug("Protection Plan form validation failed")

        if forms_valid and order_financing_form.has_changed():
            forms_valid = order_financing_form.is_valid()
            if not forms_valid:
                logger.debug("Financing form validation failed")

        if forms_valid:
            return self.form_valid(**forms)
        else:
            messages.add_message(self.request, messages.ERROR,
                                 err_saving_order_info_msg, extra_tags=error_message_tag)
            return self.form_invalid(**forms)

    def get_form(self, form_class=None):
        """
        Returns an instance of the form to be used in this view.
        """
        if form_class is None:
            form_class = self.get_form_class()
        kwargs = self.get_form_kwargs()
        kwargs.update({
            'order_type': self.order_type,
            'is_quote': self.is_quote,
            })
        return form_class(**kwargs)

    def form_invalid(self, *args, **kwargs):
        """
        Called if any of the forms on order page is invalid. Returns a response with an invalid form in the context
        """
        context = self.get_context_data()
        context.update(kwargs)
        return self.render_to_response(context)

    def form_valid(self, *args, **kwargs):
        """
        Called if all forms are valid. Creates an Order instance along with associated Customer and Commission instances
        and then redirects to a success page
        """
        form = kwargs['form']
        customer_form = kwargs['customer_form']
        items_form = kwargs['items_form']
        commissions_form = kwargs['commissions_form']
        protection_plan_form = kwargs['protection_plan_form']
        order_financing_form = kwargs['financing_form']
        payments_form = kwargs['payments_form']
        notes_form = kwargs['notes_form']

        self.object = form.save(commit=False)

        # flags
        br_passed = True
        new_customer = False
        has_customer = False

        if not form.data['status'] or form.data['status'] not in ('Q', 'R', 'N', 'X', 'QUOT', 'NEW_SO'):
            messages.error(self.request, err_invalid_new_order_status_msg, extra_tags=error_message_tag)
            br_passed = False

        # validate customer
        if br_passed:

            try:
                has_customer = self.object.customer is not None
            except Customer.DoesNotExist:
                has_customer = False

            if not has_customer:
                if customer_form.has_changed() and customer_form.is_valid():
                    try:
                        # check that first and last name are filled
                        if (not customer_form[0].cleaned_data['first_name'] or
                            not customer_form[0].cleaned_data['last_name']):
                            messages.error(
                                self.request, err_missing_customer_info_msg, extra_tags=error_message_tag)
                            br_passed = False
                        else:
                            new_customer = True
                    except KeyError:
                        if form.data['status'] != 'QUOT':
                            messages.error(
                                self.request, err_missing_customer_info_msg, extra_tags=error_message_tag)
                            br_passed = False
                else:
                    if form.data['status'] != 'QUOT':
                        messages.error(self.request, "Error saving customer form information!",
                                       extra_tags=error_message_tag)
                        br_passed = False

        # validate protection plan
        if br_passed and not self.object.protection_plan:
            if not _is_valid_protection_plan(protection_plan_form):
                messages.error(
                    self.request, protection_plan_orphan_info_error, extra_tags=error_message_tag)
                br_passed = False

        # validate order financing information
        if br_passed and not self.object.financing_option:
            if not _is_valid_financing_plan(order_financing_form):
                messages.error(
                    self.request, order_financing_orphan_info_error, extra_tags=error_message_tag)
                br_passed = False

        # validate notes
        if br_passed and not notes_form.is_valid():
            logger.debug("Order ID= {0} | ERROR:{1}".format(str(self.object.pk),
                                                            "Error saving 'order notes'!"))
            messages.error(
                self.request, "Error saving 'order notes'!", extra_tags=error_message_tag)
            br_passed = False

        if br_passed:
            try:
                with transaction.atomic():
                    # save customer
                    if new_customer:
                        customer_object = customer_form[0].save()
                        self.object.customer = customer_object
                    elif not has_customer:
                        self.object.customer = order_utils.get_default_customer()

                    # temporarily set current balance
                    self.object.current_balance = self.object.subtotal_after_discount + self.object.tax + \
                        self.object.shipping
                    # save order
                    self.object.save()

                    # save items
                    items_form.instance = self.object
                    items_form.save()

                    # save commissions
                    commissions_form.instance = self.object
                    commissions_form.save()

                    # save order protection plan form
                    if protection_plan_form.has_changed():
                        protection_plan_form.instance = self.object
                        protection_plan_form.save()

                    # save order financing option form
                    if order_financing_form.has_changed():
                        order_financing_form.instance = self.object
                        order_financing_form.save()

                    # save notes
                    if notes_form.has_changed():
                        notes_form.instance = self.object
                        notes_form.save()

                    # save payments
                    total_paid = 0.0
                    last_payment_date = None
                    if payments_form.has_changed():
                        payments_form.instance = self.object
                        saved_payments = payments_form.save()
                        total_paid = sum([p.amount for p in saved_payments])
                        try:
                            last_payment_date = max(
                                p.payment_date for p in saved_payments if p.amount > 0)
                        except:
                            last_payment_date = None

                    if not self.object.is_quote:

                        # determine if 30% deposit has been paid
                        # and if so, mark order bonus eligibility date
                        if self.object.subtotal_after_discount > 0 and total_paid / self.object.subtotal_after_discount >= 0.3:
                            self.object.bonus_elig_date = last_payment_date

                    self.object.current_balance = self.object.subtotal_after_discount + self.object.tax + \
                        self.object.shipping - total_paid
                    self.object.save()

                    return HttpResponseRedirect(self.get_success_url())
            except Exception as e:
                logger.debug("Error saving order info:")
                logger.debug(e)
                return self.form_invalid(**kwargs)
        else:
            return self.form_invalid(**kwargs)

    def get_context_data(self, **kwargs):
        context = super(OrderCreateView, self).get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return reverse('order_detail', kwargs={'pk': self.object.pk})


class ConvertOrderView(PermissionRequiredMixin, SingleObjectMixin, View):
    model = Order
    context_object_name = 'order'
    available_order_types = [t[0] for t in Order.ORDER_TYPES if "Quote" not in t[1]]
    required_permissions = (
        'orders.add_order',
    )

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        new_order_type = self.request.POST.get('order_type', None)

        if new_order_type is None or new_order_type not in self.available_order_types:
            raise AttributeError("Order Type was not selected!")

        self.object.order_type = new_order_type
        self.object.is_quote = False
        if new_order_type == 'stock':
            self.object.status = 'N'
        else:
            self.object.status = 'NEW_SO'

        self.object.save()

        messages.add_message(self.request, messages.SUCCESS,
                             'Order status was updated. Please check Status, Payments, and Items info.',
                             extra_tags="alert alert-success")

        logger.debug("New order type = " + new_order_type)
        order_url = reverse_lazy('order_edit', kwargs={'pk': self.object.pk})
        return HttpResponseRedirect(order_url)


############################ UTILITY FUNCTIONS ######################################

def _is_valid_protection_plan(protection_plan_form):
    """
    function that checks if orphaned protection plan info exists while order's protection status flag is not set
    """
    for cleaned_data in protection_plan_form.cleaned_data:
        if cleaned_data:
            if 'DELETE' in cleaned_data and cleaned_data['DELETE']:
                continue  # form row has been marked for deletion, skip it
            else:
                # check if specified fields are populated in the form
                if _any_form_fields_entered(cleaned_data, ('approval_no', 'details')):
                    return False
    return True


def _is_valid_financing_plan(financing_plan_form):
    """
    function that checks if orphaned financing plan info exists while order's financing plan status flag is not set.
    this data has similar structure to 'protection plan' that is why it refers to protection plan validation function
    """
    return _is_valid_protection_plan(financing_plan_form)


def _any_form_fields_entered(cleaned_data, field_list):
    """
    function that checks whether a form has any specified fields filled out (not blank)
    """
    res = False
    for field in field_list:
        if field in cleaned_data:
            if len(cleaned_data[field]):
                res = True
                break

    return res
