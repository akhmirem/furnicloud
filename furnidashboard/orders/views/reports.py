from datetime import datetime
from django.http import QueryDict
from django.views.generic import ListView
from core.mixins import PermissionRequiredMixin
from orders.filters import DeliveredOrderFilter
from orders.models import Order
from orders.tables import DeliveredOrdersTable
from orders.views.mixins import FilteredTableMixin
import core.utils as core_utils


class DeliveredOrdersTableView(PermissionRequiredMixin, FilteredTableMixin, ListView):
    model = Order
    table_paginate_by = 1000
    context_object_name = 'order_list'
    date_field = "delivered_date"
    make_object_list = True
    allow_future = True
    allow_empty = True
    filter_class = DeliveredOrderFilter
    table_class = DeliveredOrdersTable
    template_name = "orders/delivered_orders_report.html"
    required_permissions = (
        'orders.view_orders',
    )
    queryset = Order.objects.delivered_orders()

    def setup_filter(self, **kwargs):

        # Create a mutable QueryDict object, default is immutable
        initial = QueryDict(mutable=True)
        from_date, to_date = core_utils.get_current_month_date_range()

        from_date = datetime.combine(from_date, datetime.min.time())
        to_date = datetime.combine(to_date, datetime.max.time())

        initial['delivered_date_from'] = core_utils.date_with_tz(from_date)
        initial['delivered_date_to'] = core_utils.date_with_tz(to_date)

        # if no filters are present, try to get filters stored in session
        # if self.request.GET.get('submit', '') != u'Filter':
        #     initial.update(self.request.session.get('commission_filters', QueryDict()))

        initial.update(self.request.GET)

        self.filter = self.filter_class(initial, queryset=kwargs['queryset'])

        # save selected filters to session
        # if self.filter.data.get('submit', '') == u'Filter':
        #     self.request.session['commission_filters'] = self.filter.data

        self.filter.helper.form_id = self.filter_form_id
        self.filter.helper.form_method = "get"

    def get_context_data(self, **kwargs):
        context = {}
        context.update(kwargs)
        context[self.context_object_name] = self.get_queryset(**kwargs)

        context['list_label'] = 'Delivered Orders'

        order_subtotals = {
            'count': 0,
            'subtotal': 0.0,
            'grand_total': 0.0,
            'balance_due': 0.0
        }

        for o in self.get_queryset():
            order_subtotals['count'] += 1
            order_subtotals['subtotal'] += o.subtotal_after_discount
            order_subtotals['grand_total'] += o.grand_total
            order_subtotals['balance_due'] += o.balance_due

        context['order_subtotals'] = order_subtotals
        context['subtotals_table'] = order_subtotals

        return super(DeliveredOrdersTableView, self).get_context_data(**context)

    # def get_dated_items(self):
    #     """
    #     Return (date_list, items, extra_context) for this request.
    #     """
    #     year = self.get_year()
    #     month = self.get_month()
    #
    #     date_field = self.get_date_field()
    #     date = _date_from_string(year, self.get_year_format(),
    #                              month, self.get_month_format())
    #
    #     since = self._make_date_lookup_arg(date)
    #     until = self._make_date_lookup_arg(self._get_next_month(date))
    #     lookup_kwargs = {
    #         '%s__gte' % date_field: since,
    #         '%s__lt' % date_field: until,
    #     }
    #
    #     qs = self.get_dated_queryset(**lookup_kwargs)
    #     date_list = self.get_date_list(qs)
    #
    #     return (date_list, qs, {
    #         'month': date,
    #         'next_month': self.get_next_month(date),
    #         'previous_month': self.get_previous_month(date),
    #     })