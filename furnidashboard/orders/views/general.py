import logging
from datetime import timedelta, datetime

from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, RedirectView, TemplateView
from django.views.generic.dates import MonthArchiveView
from django_tables2 import RequestConfig

import core.utils as utils
import orders.forms as order_forms
import orders.utils as order_utils
from core.mixins import LoginRequiredMixin, PermissionRequiredMixin
from orders.models import Order
from orders.tables import SalesByAssociateTable, SalesTotalsTable, SalesByAssocSalesTable
from orders.views.mixins import FilteredTableMixin

logger = logging.getLogger('furnicloud')


class OrderMonthArchiveTableView(PermissionRequiredMixin, FilteredTableMixin, MonthArchiveView):
    model = Order
    table_paginate_by = 50
    date_field = "order_date"
    make_object_list = True
    allow_future = True
    allow_empty = True
    template_name = "orders/order_archive_month.html"
    month_format = '%b'
    last_date_of_month = None

    required_permissions = (
        'orders.view_orders',
    )

    def get(self, request, *args, **kwargs):
        self.date_list, self.object_list, extra_context = self.get_dated_items()
        next_month = extra_context['next_month']
        self.last_date_of_month = next_month - timedelta(days=1)
        context = self.get_context_data(object_list=self.object_list,
                                        date_list=self.date_list)
        context.update(extra_context)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        orders_for_month = self.get_month_dated_queryset()
        context = super(OrderMonthArchiveTableView, self).get_context_data(**kwargs)

        orders_for_month = orders_for_month.select_related('store')
        # get monthly sales totals
        month_totals_table = _get_sales_totals(orders_for_month)
        RequestConfig(self.request, paginate=False).configure(month_totals_table)
        context['month_totals_table'] = month_totals_table

        # calc YTD stats
        year = self.get_year()
        date_field = self.get_date_field()
        date = datetime.strptime(str(year), self.get_year_format()).date()
        since = self._make_date_lookup_arg(date)
        until = self._make_date_lookup_arg(self.last_date_of_month)
        lookup_kwargs = {
            '%s__gte' % date_field: since,
            '%s__lt' % date_field: until,
            'order_type__in': ('stock', 'special_order'),
        }

        ytd_orders = self.model._default_manager.select_related('store')\
            .filter(**lookup_kwargs)
        ytd_totals_table = _get_sales_totals(ytd_orders)
        RequestConfig(self.request, paginate=False).configure(ytd_totals_table)
        context['ytd_totals_table'] = ytd_totals_table

        # calc sales totals by associate
        sales_by_assoc_data, tmp = order_utils.get_sales_data_from_orders(orders_for_month)
        sales_by_assoc = SalesByAssociateTable(sales_by_assoc_data)
        RequestConfig(self.request, paginate=False).configure(sales_by_assoc)
        context['sales_by_associate'] = sales_by_assoc

        context['years_list'] = get_years_list()
        context['months_list'] = get_months_list()
        context['current_year'] = self.get_year()
        context['current_month'] = self.get_month()

        return context

    def get_month_dated_queryset(self):
        year = self.get_year()
        month = self.get_month()
        date_field = self.get_date_field()
        date = datetime.strptime("-".join((year, month)),
                                 "-".join((self.get_year_format(), self.get_month_format()))).date()
        since = self._make_date_lookup_arg(date)
        until = self._make_date_lookup_arg(self._get_next_month(date))
        lookup_kwargs = {
            '%s__gte' % date_field: since,
            '%s__lt' % date_field: until,
            'order_type__in': ('stock', 'special_order'),
        }
        qs = self.model._default_manager.all() \
            .filter(**lookup_kwargs)

        return qs

# -----------------------------------------------------------------------


class ActiveOrdersTableView(PermissionRequiredMixin, FilteredTableMixin, ListView):
    model = Order
    table_paginate_by = 50
    context_object_name = 'order_list'
    template_name = "orders/order_filtered_list.html"
    required_permissions = (
        'orders.view_orders',
    )
    queryset = Order.objects.open_orders()

    def get_context_data(self, **kwargs):
        context = super(ActiveOrdersTableView, self).get_context_data(**kwargs)
        table = self.get_table(data=context[self.context_object_name])
        context[self.context_table_name] = table
        context[self.context_filter_name] = self.filter
        context['list_label'] = 'All Active Orders'
        return context


# -----------------------------------------------------------------------

class MyOrderListView(PermissionRequiredMixin, FilteredTableMixin, ListView):
    model = Order
    context_object_name = "order_list"
    # template_name = "orders/order_filtered_table.html"
    template_name = "orders/order_filtered_list.html"
    table_paginate_by = 50

    required_permissions = (
        'orders.view_orders',
    )

    def get_queryset(self, **kwargs):
        me = self.request.user
        lookup_kwargs = {
            'order_type__in': ('cabinetry', 'cabinetry_quote')
        }
        qs = Order.objects.select_related().filter(commission__associate=me).filter(**lookup_kwargs)
        self.setup_filter(queryset=qs)
        return self.filter.qs

    def get_context_data(self, **kwargs):
        context = super(MyOrderListView, self).get_context_data(**kwargs)
        context['list_label'] = 'Just my orders'
        return context


# -----------------------------------------------------------------------

class SalesStandingsMonthTableView(PermissionRequiredMixin, ListView):
    model = Order
    context_object_name = "order_list"
    template_name = "orders/commissions_monthly.html"
    from_date = ""
    to_date = ""
    department = 'furniture'
    department_to_order_types_map = {
        'furniture': ['stock', 'special_order'],
        'kitchen-and-bath': ['cabinetry', 'cabinetry_quote'],
        'appliances': ['appliances'],
        'windows': ['windows'],
        'design': ['design'],
        'flooring': ['flooring'],
    }

    required_permissions = (
        'orders.view_sales',
    )

    def get_queryset(self, **kwargs):
        # qs = super(FilteredTableMixin, self).get_queryset(**kwargs)
        date_range = "month"  # default
        try:
            date_range = self.request.GET['date_range']
        except KeyError as e:
            pass

        self.from_date, self.to_date = utils.get_date_range_from_string(
            date_range, self.request)

        self.department = self.request.GET.get('department',  self.department)

        lookup_kwargs = {
            'order_type__in': (self.department_to_order_types_map.get(self.department))
        }
        qs = Order.objects.get_dated_qs(self.from_date, self.to_date)\
            .filter(**lookup_kwargs)

        return qs

    def get_context_data(self, **kwargs):
        context = super(SalesStandingsMonthTableView, self).get_context_data(**kwargs)

        date_range = 'month'
        if "date_range" in self.request.GET:
            context['date_range_filter'] = order_forms.DateRangeForm(self.request.GET)
        else:
            context['date_range_filter'] = order_forms.DateRangeForm(initial={'date_range': date_range})

        context['dates_caption'] = "{0} - {1}".format(self.from_date.strftime("%b %d, %Y"),
                                                      self.to_date.strftime("%b %d, %Y"))

        orders = context[self.context_object_name]
        sales_by_assoc_data, sales_by_assoc_expanded_data = order_utils.get_sales_data_from_orders(orders)

        # total sales table
        sales_by_assoc_table = SalesByAssociateTable(sales_by_assoc_data)
        RequestConfig(self.request, paginate=False).configure(sales_by_assoc_table)
        context['sales_by_associate'] = sales_by_assoc_table

        # prepare expanded sales tables per each associate
        sales_by_assoc_expanded_tables = {}
        count = 1
        for assoc, sales in sales_by_assoc_expanded_data.items():
            sales_by_assoc_expanded_tables[assoc] = SalesByAssocSalesTable(sales, prefix="tbl" + str(count))
            RequestConfig(self.request).configure(sales_by_assoc_expanded_tables[assoc])
            count += 1
        context['sales_by_assoc_expanded_tables'] = sales_by_assoc_expanded_tables

        # sales by stores table
        orders = orders.select_related('store')
        store_totals_table = _get_sales_totals(orders)
        RequestConfig(self.request, paginate=False).configure(store_totals_table)
        context['store_totals_table'] = store_totals_table

        # employee of the month
        last_month_begin, last_month_end = utils.get_date_range_from_string('last-month', self.request)
        last_month_orders = Order.objects.get_dated_qs(last_month_begin, last_month_end)
        last_month_sales, _ = order_utils.get_sales_data_from_orders(last_month_orders)
        last_month_sales = sorted(last_month_sales, key=lambda a: a['sales'], reverse=True)
        try:
            context['employee_of_the_month'] = last_month_sales[0]['associate']
            context['employee_of_the_month_period'] = last_month_begin.strftime("%b, %Y")
        except (KeyError, IndexError):
            context['employee_of_the_month_period'] = context['employee_of_the_month'] = ''

        return context


# -----------------------------------------------------------------------


class CabinetrySalesStandingsMonthTableView(SalesStandingsMonthTableView):
    model = Order
    context_object_name = "order_list"
    template_name = "orders/commissions_monthly.html"
    from_date = ""
    to_date = ""

    required_permissions = (
        'orders.view_cabinetry_orders',
    )

    def get_queryset(self, **kwargs):
        date_range = "month"  # default
        try:
            date_range = self.request.GET['date_range']
        except KeyError:
            pass

        self.from_date, self.to_date = utils.get_date_range_from_string(date_range, self.request)

        filter_kwargs = {
            'order_type__in': ('cabinetry', 'cabinetry_quote')
        }
        qs = Order.objects.get_dated_qs(self.from_date, self.to_date).filter(**filter_kwargs)

        return qs


class HomePageRedirectView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('order_list')

    def get_redirect_url(self, **kwargs):
        # redirect directly to deliveries page if user belongs to group delivery_person
        if utils.is_user_delivery_group(self.request):
            self.url = reverse('delivery_list')
        # else:
        #    self.url = reverse('dashboard_url')

        return super(HomePageRedirectView, self).get_redirect_url(**kwargs)


class DashBoardView(LoginRequiredMixin, TemplateView):
    template_name = "core/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(DashBoardView, self).get_context_data()
        context['test'] = "TEST"
        context['associate'] = self.request.user
        return context


def _get_sales_totals(qs):
    subtotal_sac = subtotal_fnt = grand_total_sac = grand_total_fnt = 0.0
    for o in qs:
        if o.status not in ('X', 'QUOT', 'E'):
            if o.store.name == 'Sacramento':
                subtotal_sac += o.subtotal_after_discount
                grand_total_sac += o.grand_total
            elif o.store.name == 'Roseville':
                subtotal_fnt += o.subtotal_after_discount
                grand_total_fnt += o.grand_total

    totals_data = [
        {'item': 'Subtotal After Discount', 'hq': utils.dollars(subtotal_sac), 'fnt': utils.dollars(subtotal_fnt),
         'total': utils.dollars(subtotal_sac + subtotal_fnt)},
        {'item': 'Grand Total', 'hq': utils.dollars(grand_total_sac), 'fnt': utils.dollars(grand_total_fnt),
         'total': utils.dollars(grand_total_sac + grand_total_fnt)},
    ]

    totals_table = SalesTotalsTable(totals_data)
    return totals_table


def get_months_list():
    return [['1', 'January'],
            ['2', 'February'],
            ['3', 'March'],
            ['4', 'April'],
            ['5', 'May'],
            ['6', 'June'],
            ['7', 'July'],
            ['8', 'August'],
            ['9', 'September'],
            ['10', 'October'],
            ['11', 'November'],
            ['12', 'December']]


def get_years_list():
    return sorted([str(y) for y in range(2015, datetime.now().year + 1)], reverse=True)
