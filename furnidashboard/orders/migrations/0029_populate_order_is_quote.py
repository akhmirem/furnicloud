# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def populate_order_is_quote(apps, schema_editor):
    Order = apps.get_model("orders", "Order")
    for o in Order.objects.all():
        if o.order_type in ('quote', 'cabinetry_quote'):
            o.is_quote = True
            o.save()


class Migration(migrations.Migration):
    dependencies = [
        ('orders', '0028_auto_20221020_2042'),
    ]

    operations = [
        migrations.RunPython(populate_order_is_quote),
    ]
