# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0012_auto_20170616_1818'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdelivery',
            name='delivery_type',
            field=models.CharField(blank=True, max_length=25, null=True, choices=[(b'SELF', b'Self Pickup/ Will Call'), (b'RFD', b'Roberts Furniture Delivery'), (b'MGL', b'Miguel'), (b'CUSTOM', b'Other')]),
        ),
    ]
