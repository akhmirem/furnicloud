# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0003_auto_20160921_1031'),
    ]

    operations = [
        #migrations.AlterField(
        #    model_name='order',
        #    name='customer',
        #    field=models.ForeignKey(related_name='orders', blank=True, to='customers.Customer', null=True),
        #),
        migrations.AlterField(
            model_name='order',
            name='store',
            field=models.ForeignKey(related_name='orders', to='stores.Store', on_delete=models.CASCADE),
        ),
        #migrations.AlterField(
        #    model_name='orderauditlogentry',
        #    name='customer',
        #    field=models.ForeignKey(related_name='_auditlog_orders', blank=True, to='customers.Customer', null=True),
        #),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='store',
            field=models.ForeignKey(related_name='_auditlog_orders', to='stores.Store', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='order',
            field=models.ForeignKey(related_name='orderitem_set', to='orders.Order', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='orderpayment',
            name='order',
            field=models.ForeignKey(related_name='payments', to='orders.Order', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='orderpayment',
            name='payment_type',
            field=models.CharField(max_length=30, choices=[(b'CASH', b'Cash'), (b'CREDIT', b'Credit Card'), (b'DEBIT', b'ATM/Debit Card'), (b'CHECK', b'Check'), (b'FINANCING', b'Financing Plan'), (b'OTHER', b'Other')]),
        ),
        migrations.AlterField(
            model_name='orderpaymentauditlogentry',
            name='order',
            field=models.ForeignKey(related_name='_auditlog_payments', to='orders.Order', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='orderpaymentauditlogentry',
            name='payment_type',
            field=models.CharField(max_length=30, choices=[(b'CASH', b'Cash'), (b'CREDIT', b'Credit Card'), (b'DEBIT', b'ATM/Debit Card'), (b'CHECK', b'Check'), (b'FINANCING', b'Financing Plan'), (b'OTHER', b'Other')]),
        ),
    ]
