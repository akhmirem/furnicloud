# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import audit_log.models.fields
import django.utils.timezone
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '__first__'),
        ('stores', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('number', models.CharField(max_length=50)),
                ('order_date', models.DateTimeField(null=True)),
                ('status', models.CharField(max_length=5, choices=[(b'N', b'New'), (b'QUOT', b'Quotation'), (b'Q', b'Pending'), (b'H', b'On Hold'), (b'E', b'Cancelled'), (b'P', b'In Production'), (b'T', b'In Transit'), (b'S', b'Scheduled for Delivery'), (b'D', b'Delivered'), (b'X', b'Dummy'), (b'I', b'Historical (Excel Import)'), (b'C', b'Closed')])),
                ('deposit_balance', models.FloatField(default=0.0, blank=True)),
                ('subtotal_after_discount', models.FloatField(default=0.0, blank=True)),
                ('tax', models.FloatField(default=0.0, blank=True)),
                ('shipping', models.FloatField(default=0.0, blank=True)),
                ('comments', models.TextField(blank=True)),
                ('referral', models.CharField(blank=True, max_length=50, null=True, choices=[(b'NO', b'Not Referred'), (b'RET', b'Returning Customer'), (b'RVL', b'Roseville Store'), (b'CST', b'Referred by existing Furnitalia customer'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'WEB', b'Website'), (b'EBL', b'E-mail (or Eblast)'), (b'MAG', b'Magazine'), (b'SOC', b'Social networks'), (b'NWP', b'Newspaper'), (b'TV', b'TV')])),
                ('protection_plan', models.BooleanField(default=False)),
                ('financing_option', models.BooleanField(default=False)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_orders_order_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('customer', models.ForeignKey(blank=True, to='customers.Customer', null=True, on_delete=models.CASCADE)),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_orders_order_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('store', models.ForeignKey(to='stores.Store', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['-order_date'],
                'db_table': 'order_info',
                'permissions': (('view_orders', 'Can View Orders'), ('view_sales', 'Can View Sales Reports'), ('update_status', 'Can Update Order Status')),
            },
        ),
        migrations.CreateModel(
            name='OrderAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'attachments/%Y/%m')),
                ('description', models.CharField(max_length=255, null=True, blank=True)),
                ('order', models.ForeignKey(to='orders.Order', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'order_attachments',
            },
        ),
        migrations.CreateModel(
            name='OrderAuditLogEntry',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('number', models.CharField(max_length=50)),
                ('order_date', models.DateTimeField(null=True)),
                ('status', models.CharField(max_length=5, choices=[(b'N', b'New'), (b'QUOT', b'Quotation'), (b'Q', b'Pending'), (b'H', b'On Hold'), (b'E', b'Cancelled'), (b'P', b'In Production'), (b'T', b'In Transit'), (b'S', b'Scheduled for Delivery'), (b'D', b'Delivered'), (b'X', b'Dummy'), (b'I', b'Historical (Excel Import)'), (b'C', b'Closed')])),
                ('deposit_balance', models.FloatField(default=0.0, blank=True)),
                ('subtotal_after_discount', models.FloatField(default=0.0, blank=True)),
                ('tax', models.FloatField(default=0.0, blank=True)),
                ('shipping', models.FloatField(default=0.0, blank=True)),
                ('comments', models.TextField(blank=True)),
                ('referral', models.CharField(blank=True, max_length=50, null=True, choices=[(b'NO', b'Not Referred'), (b'RET', b'Returning Customer'), (b'RVL', b'Roseville Store'), (b'CST', b'Referred by existing Furnitalia customer'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'WEB', b'Website'), (b'EBL', b'E-mail (or Eblast)'), (b'MAG', b'Magazine'), (b'SOC', b'Social networks'), (b'NWP', b'Newspaper'), (b'TV', b'TV')])),
                ('protection_plan', models.BooleanField(default=False)),
                ('financing_option', models.BooleanField(default=False)),
                ('action_id', models.AutoField(serialize=False, primary_key=True)),
                ('action_date', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('action_type', models.CharField(max_length=1, editable=False, choices=[('I', 'Created'), ('U', 'Changed'), ('D', 'Deleted')])),
                ('action_user', audit_log.models.fields.LastUserField(related_name='_order_audit_log_entry', editable=False, to=settings.AUTH_USER_MODEL, null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='_auditlog_created_orders_order_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('customer', models.ForeignKey(related_name='_auditlog_order_set', blank=True, to='customers.Customer', null=True, on_delete=models.CASCADE)),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='_auditlog_modified_orders_order_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('store', models.ForeignKey(related_name='_auditlog_order_set', to='stores.Store', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('-action_date',),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='OrderDelivery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('delivery_type', models.CharField(blank=True, max_length=25, null=True, choices=[(b'SELF', b'Self Pickup'), (b'RFD', b'Roberts Furniture Delivery'), (b'MGL', b'Miguel'), (b'CUSTOM', b'Other')])),
                ('scheduled_delivery_date', models.DateField(null=True, blank=True)),
                ('delivered_date', models.DateField(null=True, blank=True)),
                ('delivery_slip', models.FileField(null=True, upload_to=b'deliveries/%Y/%m', blank=True)),
                ('comments', models.TextField(null=True, blank=True)),
                ('delivery_person_notes', models.TextField(null=True, blank=True)),
                ('delivery_cost', models.FloatField(default=0.0, blank=True)),
                ('paid', models.BooleanField(default=False)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_orders_orderdelivery_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_orders_orderdelivery_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('order', models.ForeignKey(to='orders.Order', on_delete=models.CASCADE)),
                ('pickup_from', models.ForeignKey(blank=True, to='stores.Store', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 'deliveries',
                'verbose_name_plural': 'deliveries',
                'permissions': (('modify_delivery_fee', 'Modify Delivery Fee'),),
            },
        ),
        migrations.CreateModel(
            name='OrderFinancing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('approval_no', models.CharField(max_length=50)),
                ('details', models.TextField(null=True, blank=True)),
                ('order', models.ForeignKey(to='orders.Order', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='OrderIssue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('status', models.CharField(max_length=5, choices=[(b'N', b'New'), (b'C', b'Claim submitted'), (b'T', b'Technician sent'), (b'E', b'Eligible for Credit'), (b'R', b'Resolved')])),
                ('comments', models.TextField(null=True, blank=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_orders_orderissue_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_orders_orderissue_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('order', models.ForeignKey(to='orders.Order', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['-created'],
                'db_table': 'order_issues',
                'permissions': (('update_order_issues', 'Can Update Order Issues (Claims)Information'),),
            },
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('status', models.CharField(blank=True, max_length=15, null=True, choices=[(b'P', b'Pending'), (b'O', b'Ordered'), (b'R', b'Received'), (b'D', b'Delivered'), (b'S', b'In Stock'), (b'I', b'Historical (Excel Import)')])),
                ('in_stock', models.BooleanField(default=True)),
                ('description', models.CharField(max_length=255)),
                ('po_num', models.CharField(max_length=125, blank=True)),
                ('po_date', models.DateField(null=True, blank=True)),
                ('ack_num', models.CharField(max_length=125, blank=True)),
                ('ack_date', models.DateField(null=True, blank=True)),
                ('eta', models.DateField(null=True, blank=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_orders_orderitem_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_orders_orderitem_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('order', models.ForeignKey(to='orders.Order', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'order_items',
                'verbose_name_plural': 'ordered items',
            },
        ),
        migrations.CreateModel(
            name='OrderItemAuditLogEntry',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('status', models.CharField(blank=True, max_length=15, null=True, choices=[(b'P', b'Pending'), (b'O', b'Ordered'), (b'R', b'Received'), (b'D', b'Delivered'), (b'S', b'In Stock'), (b'I', b'Historical (Excel Import)')])),
                ('in_stock', models.BooleanField(default=True)),
                ('description', models.CharField(max_length=255)),
                ('po_num', models.CharField(max_length=125, blank=True)),
                ('po_date', models.DateField(null=True, blank=True)),
                ('ack_num', models.CharField(max_length=125, blank=True)),
                ('ack_date', models.DateField(null=True, blank=True)),
                ('eta', models.DateField(null=True, blank=True)),
                ('action_id', models.AutoField(serialize=False, primary_key=True)),
                ('action_date', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('action_type', models.CharField(max_length=1, editable=False, choices=[('I', 'Created'), ('U', 'Changed'), ('D', 'Deleted')])),
                ('action_user', audit_log.models.fields.LastUserField(related_name='_orderitem_audit_log_entry', editable=False, to=settings.AUTH_USER_MODEL, null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='_auditlog_created_orders_orderitem_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='_auditlog_modified_orders_orderitem_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
                ('order', models.ForeignKey(related_name='_auditlog_orderitem_set', to='orders.Order', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ('-action_date',),
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='OrderItemProtectionPlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('approval_no', models.CharField(max_length=50)),
                ('details', models.TextField(null=True, blank=True)),
                ('order', models.ForeignKey(to='orders.Order', on_delete=models.CASCADE)),
            ],
        ),
    ]
