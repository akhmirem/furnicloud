# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_orderitemprotectionplan_associate'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='order_last_checked_date',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='special_order_tracking_desc',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='orderitemauditlogentry',
            name='order_last_checked_date',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='orderitemauditlogentry',
            name='special_order_tracking_desc',
            field=models.TextField(null=True, blank=True),
        ),
    ]
