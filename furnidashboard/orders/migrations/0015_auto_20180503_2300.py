# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0014_auto_20180423_2217'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='order_type',
            field=models.CharField(blank=True, max_length=25, null=True, choices=[(b'stock', b'In Stock Order'), (b'special_order', b'Special Order'), (b'quote', b'Quote'), (b'cabinetry', b'Cabinetry')]),
        ),
        migrations.AddField(
            model_name='orderauditlogentry',
            name='order_type',
            field=models.CharField(blank=True, max_length=25, null=True, choices=[(b'stock', b'In Stock Order'), (b'special_order', b'Special Order'), (b'quote', b'Quote'), (b'cabinetry', b'Cabinetry')]),
        ),
        migrations.AlterField(
            model_name='order',
            name='delivered_date',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(max_length=25, choices=[(b'N', b'New/ In-Stock'), (b'NEW_SO', b'New SO'), (b'QUOT', b'Quotation'), (b'ORDERED', b'Ordered'), (b'Q', b'Pending'), (b'H', b'On Hold'), (b'P', b'In Production'), (b'T', b'In Transit'), (b'R', b'Received/ In Warehouse'), (b'S', b'Scheduled for Delivery'), (b'D', b'Delivered'), (b'E', b'Cancelled'), (b'X', b'Dummy'), (b'I', b'Historical (Excel Import)'), (b'C', b'Closed')]),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='delivered_date',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='status',
            field=models.CharField(max_length=25, choices=[(b'N', b'New/ In-Stock'), (b'NEW_SO', b'New SO'), (b'QUOT', b'Quotation'), (b'ORDERED', b'Ordered'), (b'Q', b'Pending'), (b'H', b'On Hold'), (b'P', b'In Production'), (b'T', b'In Transit'), (b'R', b'Received/ In Warehouse'), (b'S', b'Scheduled for Delivery'), (b'D', b'Delivered'), (b'E', b'Cancelled'), (b'X', b'Dummy'), (b'I', b'Historical (Excel Import)'), (b'C', b'Closed')]),
        ),
    ]
