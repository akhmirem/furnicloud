# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def populate_order_type(apps, schema_editor):
    Order = apps.get_model("orders", "Order")
    for o in Order.objects.all():
        if o.status in ('QUOT', 'X'):
            o.order_type = 'quote'
        elif o.status == 'I':
            o.order_type = "special_order"
        else:
            if any([i for i in o.ordered_items.all() if not i.in_stock]):
                o.order_type = "special_order"
            else:
                o.order_type = "stock"
        o.save()


class Migration(migrations.Migration):
    dependencies = [
        ('orders', '0015_auto_20180503_2300'),
    ]

    operations = [
        migrations.RunPython(populate_order_type),
    ]
