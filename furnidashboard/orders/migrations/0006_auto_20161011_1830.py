# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0005_auto_20161009_2209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderpayment',
            name='payment_type',
            field=models.CharField(max_length=30, choices=[(b'CASH', b'Cash'), (b'CREDIT', b'Credit Card'), (b'DEBIT', b'ATM/Debit Card'), (b'CHECK', b'Check'), (b'FINANCING', b'Financing Plan'), (b'RETURN', b'Return/Credit back/Reverse'), (b'OTHER', b'Other')]),
        ),
        migrations.AlterField(
            model_name='orderpaymentauditlogentry',
            name='payment_type',
            field=models.CharField(max_length=30, choices=[(b'CASH', b'Cash'), (b'CREDIT', b'Credit Card'), (b'DEBIT', b'ATM/Debit Card'), (b'CHECK', b'Check'), (b'FINANCING', b'Financing Plan'), (b'RETURN', b'Return/Credit back/Reverse'), (b'OTHER', b'Other')]),
        ),
    ]
