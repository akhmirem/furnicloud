# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def create_payments_history(apps, schema_editor):
    Order = apps.get_model("orders", "Order")
    OrderPayment = apps.get_model("orders", "OrderPayment")
    for o in Order.objects.all():
        if o.payments.count() == 0:
            p = OrderPayment.objects.create(order=o, payment_date=o.order_date)
            p.amount = o.deposit_balance or 0.0
            p.payment_type = 'OTHER'
            p.comments = 'Auto-generated from order deposit balance.'
            p.save()


class Migration(migrations.Migration):
    dependencies = [
        ('orders', '0006_auto_20161011_1830'),
    ]

    operations = [
        migrations.RunPython(create_payments_history),
    ]
