# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_create_payment_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitemprotectionplan',
            name='plan_price',
            field=models.FloatField(default=0.0, blank=True),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='in_stock',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='orderitemauditlogentry',
            name='in_stock',
            field=models.BooleanField(default=False),
        ),
    ]
