# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import audit_log.models.fields
import django_extensions.db.fields
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderPayment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('payment_date', models.DateTimeField()),
                ('payment_type', models.CharField(max_length=30, choices=[(b'CASH', b'Cash'), (b'CREDIT', b'Credit Card'), (b'DEBIT', b'ATM/Debit Card'), (b'CHECK', b'Check'), (b'FINANCING', b'Financing Plan')])),
                ('amount', models.FloatField(default=0.0, blank=True)),
                ('comments', models.TextField(null=True, blank=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='created_orders_orderpayment_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='modified_orders_orderpayment_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
            ],
            options={
                'ordering': ['-payment_date'],
                'db_table': 'order_payments',
                'verbose_name_plural': 'order_payments',
                'permissions': (('update_order_payments', 'Can Update Order Payments'),),
            },
        ),
        migrations.CreateModel(
            name='OrderPaymentAuditLogEntry',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('created_with_session_key', audit_log.models.fields.CreatingSessionKeyField(max_length=40, null=True, editable=False)),
                ('modified_with_session_key', audit_log.models.fields.LastSessionKeyField(max_length=40, null=True, editable=False)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('payment_date', models.DateTimeField()),
                ('payment_type', models.CharField(max_length=30, choices=[(b'CASH', b'Cash'), (b'CREDIT', b'Credit Card'), (b'DEBIT', b'ATM/Debit Card'), (b'CHECK', b'Check'), (b'FINANCING', b'Financing Plan')])),
                ('amount', models.FloatField(default=0.0, blank=True)),
                ('comments', models.TextField(null=True, blank=True)),
                ('action_id', models.AutoField(serialize=False, primary_key=True)),
                ('action_date', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('action_type', models.CharField(max_length=1, editable=False, choices=[('I', 'Created'), ('U', 'Changed'), ('D', 'Deleted')])),
                ('action_user', audit_log.models.fields.LastUserField(related_name='_orderpayment_audit_log_entry', editable=False, to=settings.AUTH_USER_MODEL, null=True)),
                ('created_by', audit_log.models.fields.CreatingUserField(related_name='_auditlog_created_orders_orderpayment_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='created by')),
                ('modified_by', audit_log.models.fields.LastUserField(related_name='_auditlog_modified_orders_orderpayment_set', editable=False, to=settings.AUTH_USER_MODEL, null=True, verbose_name='modified by')),
            ],
            options={
                'ordering': ('-action_date',),
                'default_permissions': (),
            },
        ),
        migrations.AddField(
            model_name='order',
            name='current_balance',
            field=models.FloatField(default=0.0, blank=True),
        ),
        migrations.AddField(
            model_name='orderauditlogentry',
            name='current_balance',
            field=models.FloatField(default=0.0, blank=True),
        ),
        migrations.AddField(
            model_name='orderpaymentauditlogentry',
            name='order',
            field=models.ForeignKey(related_name='_auditlog_orderpayment_set', to='orders.Order', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='orderpayment',
            name='order',
            field=models.ForeignKey(to='orders.Order', on_delete=models.CASCADE),
        ),
    ]
