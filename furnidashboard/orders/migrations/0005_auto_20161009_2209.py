# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0004_auto_20161007_2332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderitem',
            name='order',
            field=models.ForeignKey(related_name='ordered_items', to='orders.Order', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='orderitemauditlogentry',
            name='order',
            field=models.ForeignKey(related_name='_auditlog_ordered_items', to='orders.Order', on_delete=models.CASCADE),
        ),
    ]
