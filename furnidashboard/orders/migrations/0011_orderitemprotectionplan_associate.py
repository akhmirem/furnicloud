# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orders', '0010_orderitemprotectionplan_sold_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitemprotectionplan',
            name='associate',
            field=models.ForeignKey(related_name='protection_plans', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL),
        ),
    ]
