# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20170324_1112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(related_name='orders', to='customers.Customer', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='customer',
            field=models.ForeignKey(related_name='_auditlog_orders', to='customers.Customer', on_delete=models.CASCADE),
        ),
    ]
