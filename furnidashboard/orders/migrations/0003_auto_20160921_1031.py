# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20160919_1309'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='orderpayment',
            options={'ordering': ['-payment_date'], 'verbose_name_plural': 'order_payments'},
        ),
        migrations.AlterField(
            model_name='order',
            name='deposit_balance',
            field=models.FloatField(default=0.0, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='deposit_balance',
            field=models.FloatField(default=0.0, null=True, blank=True),
        ),
    ]
