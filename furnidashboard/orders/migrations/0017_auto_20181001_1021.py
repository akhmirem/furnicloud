# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0016_populate_order_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='category',
            field=models.CharField(blank=True, default=None, null=True, choices=[('CABINETS', 'Cabinets'), ('APPLIANCES', 'Appliances'), ('COUNTER', 'Counter Tops'), ('BATHROOM', 'Bathroom Cabinets'), ('BACKSPLASH', 'Backsplash'), ('CAB_INSTALL', 'Cabinets Installation'), ('APP_INSTALL', 'Appliances Installation'), ('DESIGN', 'Design Fee'), ('OTHER', 'Other Labor'), ('Discount', 'Discount')], max_length=125),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='cost',
            field=models.FloatField(blank=True, default=0.0),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='price',
            field=models.FloatField(blank=True, default=0.0),
        ),
        migrations.AddField(
            model_name='orderitemauditlogentry',
            name='category',
            field=models.CharField(blank=True, default=None, null=True, choices=[('CABINETS', 'Cabinets'), ('APPLIANCES', 'Appliances'), ('COUNTER', 'Counter Tops'), ('BATHROOM', 'Bathroom Cabinets'), ('BACKSPLASH', 'Backsplash'), ('CAB_INSTALL', 'Cabinets Installation'), ('APP_INSTALL', 'Appliances Installation'), ('DESIGN', 'Design Fee'), ('OTHER', 'Other Labor'), ('Discount', 'Discount')], max_length=125),
        ),
        migrations.AddField(
            model_name='orderitemauditlogentry',
            name='cost',
            field=models.FloatField(blank=True, default=0.0),
        ),
        migrations.AddField(
            model_name='orderitemauditlogentry',
            name='price',
            field=models.FloatField(blank=True, default=0.0),
        ),
        migrations.AlterField(
            model_name='order',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='order',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_type',
            field=models.CharField(blank=True, null=True, choices=[('stock', 'In Stock Order'), ('special_order', 'Special Order'), ('quote', 'Quote'), ('cabinetry', 'Cabinetry')], max_length=25),
        ),
        migrations.AlterField(
            model_name='order',
            name='referral',
            field=models.CharField(blank=True, null=True, choices=[('RVL', 'Roseville Store'), ('RET', 'Returning Customer'), ('GOOG', 'Google/Bing Search Engines'), ('EBL', 'E-Blast'), ('WEB', 'Website Direct Visit'), ('VENDOR', 'Vendor Website Referral'), ('TV', 'TV'), ('MAG', 'Magazine'), ('REF', 'Referred by friends/relatives/acquaintances'), ('SOC', 'Social networks'), ('EXPO', 'Home & Landscape Show'), ('CST', 'Referred by existing Furnitalia customer'), ('NWP', 'Newspaper'), ('NO', 'Not Referred')], max_length=50),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('N', 'New/ In-Stock'), ('NEW_SO', 'New SO'), ('QUOT', 'Quotation'), ('ORDERED', 'Ordered'), ('Q', 'Pending'), ('H', 'On Hold'), ('P', 'In Production'), ('T', 'In Transit'), ('R', 'Received/ In Warehouse'), ('S', 'Scheduled for Delivery'), ('D', 'Delivered'), ('E', 'Cancelled'), ('X', 'Dummy'), ('I', 'Historical (Excel Import)'), ('C', 'Closed')], max_length=25),
        ),
        migrations.AlterField(
            model_name='orderattachment',
            name='file',
            field=models.FileField(upload_to='attachments/%Y/%m'),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='order_type',
            field=models.CharField(blank=True, null=True, choices=[('stock', 'In Stock Order'), ('special_order', 'Special Order'), ('quote', 'Quote'), ('cabinetry', 'Cabinetry')], max_length=25),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='referral',
            field=models.CharField(blank=True, null=True, choices=[('RVL', 'Roseville Store'), ('RET', 'Returning Customer'), ('GOOG', 'Google/Bing Search Engines'), ('EBL', 'E-Blast'), ('WEB', 'Website Direct Visit'), ('VENDOR', 'Vendor Website Referral'), ('TV', 'TV'), ('MAG', 'Magazine'), ('REF', 'Referred by friends/relatives/acquaintances'), ('SOC', 'Social networks'), ('EXPO', 'Home & Landscape Show'), ('CST', 'Referred by existing Furnitalia customer'), ('NWP', 'Newspaper'), ('NO', 'Not Referred')], max_length=50),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='status',
            field=models.CharField(choices=[('N', 'New/ In-Stock'), ('NEW_SO', 'New SO'), ('QUOT', 'Quotation'), ('ORDERED', 'Ordered'), ('Q', 'Pending'), ('H', 'On Hold'), ('P', 'In Production'), ('T', 'In Transit'), ('R', 'Received/ In Warehouse'), ('S', 'Scheduled for Delivery'), ('D', 'Delivered'), ('E', 'Cancelled'), ('X', 'Dummy'), ('I', 'Historical (Excel Import)'), ('C', 'Closed')], max_length=25),
        ),
        migrations.AlterField(
            model_name='orderdelivery',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderdelivery',
            name='delivery_slip',
            field=models.FileField(blank=True, null=True, upload_to='deliveries/%Y/%m'),
        ),
        migrations.AlterField(
            model_name='orderdelivery',
            name='delivery_type',
            field=models.CharField(blank=True, null=True, choices=[('SELF', 'Self Pickup/ Will Call'), ('RFD', 'Roberts Furniture Delivery'), ('MGL', 'Miguel'), ('CUSTOM', 'Other')], max_length=25),
        ),
        migrations.AlterField(
            model_name='orderdelivery',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderissue',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderissue',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderissue',
            name='status',
            field=models.CharField(choices=[('N', 'New'), ('C', 'Claim submitted'), ('T', 'Technician sent'), ('E', 'Eligible for Credit'), ('R', 'Resolved')], max_length=5),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='status',
            field=models.CharField(blank=True, null=True, choices=[('P', 'Unordered'), ('O', 'Ordered'), ('R', 'Received'), ('D', 'Delivered'), ('S', 'In Stock'), ('I', 'Historical (Excel Import)')], max_length=15),
        ),
        migrations.AlterField(
            model_name='orderitemauditlogentry',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderitemauditlogentry',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderitemauditlogentry',
            name='status',
            field=models.CharField(blank=True, null=True, choices=[('P', 'Unordered'), ('O', 'Ordered'), ('R', 'Received'), ('D', 'Delivered'), ('S', 'In Stock'), ('I', 'Historical (Excel Import)')], max_length=15),
        ),
        migrations.AlterField(
            model_name='orderpayment',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderpayment',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderpayment',
            name='payment_type',
            field=models.CharField(choices=[('CASH', 'Cash'), ('CREDIT', 'Credit Card'), ('DEBIT', 'ATM/Debit Card'), ('CHECK', 'Check'), ('FINANCING', 'Financing Plan'), ('RETURN', 'Return/Credit back/Reverse'), ('OTHER', 'Other')], max_length=30),
        ),
        migrations.AlterField(
            model_name='orderpaymentauditlogentry',
            name='created',
            field=django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created'),
        ),
        migrations.AlterField(
            model_name='orderpaymentauditlogentry',
            name='modified',
            field=django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified'),
        ),
        migrations.AlterField(
            model_name='orderpaymentauditlogentry',
            name='payment_type',
            field=models.CharField(choices=[('CASH', 'Cash'), ('CREDIT', 'Credit Card'), ('DEBIT', 'ATM/Debit Card'), ('CHECK', 'Check'), ('FINANCING', 'Financing Plan'), ('RETURN', 'Return/Credit back/Reverse'), ('OTHER', 'Other')], max_length=30),
        ),
    ]
