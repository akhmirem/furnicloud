# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_auto_20170324_1354'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitemprotectionplan',
            name='sold_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
