# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0013_auto_20171007_2248'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='delivered_date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='orderauditlogentry',
            name='delivered_date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='referral',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'RVL', b'Roseville Store'), (b'RET', b'Returning Customer'), (b'GOOG', b'Google/Bing Search Engines'), (b'EBL', b'E-Blast'), (b'WEB', b'Website Direct Visit'), (b'VENDOR', b'Vendor Website Referral'), (b'TV', b'TV'), (b'MAG', b'Magazine'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'SOC', b'Social networks'), (b'EXPO', b'Home & Landscape Show'), (b'CST', b'Referred by existing Furnitalia customer'), (b'NWP', b'Newspaper'), (b'NO', b'Not Referred')]),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(max_length=5, choices=[(b'N', b'New/ In-Stock'), (b'QUOT', b'Quotation'), (b'Q', b'Pending'), (b'H', b'On Hold'), (b'E', b'Cancelled'), (b'P', b'In Production'), (b'T', b'In Transit'), (b'R', b'Received/ In Warehouse'), (b'S', b'Scheduled for Delivery'), (b'D', b'Delivered'), (b'X', b'Dummy'), (b'I', b'Historical (Excel Import)'), (b'C', b'Closed')]),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='referral',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'RVL', b'Roseville Store'), (b'RET', b'Returning Customer'), (b'GOOG', b'Google/Bing Search Engines'), (b'EBL', b'E-Blast'), (b'WEB', b'Website Direct Visit'), (b'VENDOR', b'Vendor Website Referral'), (b'TV', b'TV'), (b'MAG', b'Magazine'), (b'REF', b'Referred by friends/relatives/acquaintances'), (b'SOC', b'Social networks'), (b'EXPO', b'Home & Landscape Show'), (b'CST', b'Referred by existing Furnitalia customer'), (b'NWP', b'Newspaper'), (b'NO', b'Not Referred')]),
        ),
        migrations.AlterField(
            model_name='orderauditlogentry',
            name='status',
            field=models.CharField(max_length=5, choices=[(b'N', b'New/ In-Stock'), (b'QUOT', b'Quotation'), (b'Q', b'Pending'), (b'H', b'On Hold'), (b'E', b'Cancelled'), (b'P', b'In Production'), (b'T', b'In Transit'), (b'R', b'Received/ In Warehouse'), (b'S', b'Scheduled for Delivery'), (b'D', b'Delivered'), (b'X', b'Dummy'), (b'I', b'Historical (Excel Import)'), (b'C', b'Closed')]),
        ),
        migrations.AlterField(
            model_name='orderitem',
            name='status',
            field=models.CharField(blank=True, max_length=15, null=True, choices=[(b'P', b'Unordered'), (b'O', b'Ordered'), (b'R', b'Received'), (b'D', b'Delivered'), (b'S', b'In Stock'), (b'I', b'Historical (Excel Import)')]),
        ),
        migrations.AlterField(
            model_name='orderitemauditlogentry',
            name='status',
            field=models.CharField(blank=True, max_length=15, null=True, choices=[(b'P', b'Unordered'), (b'O', b'Ordered'), (b'R', b'Received'), (b'D', b'Delivered'), (b'S', b'In Stock'), (b'I', b'Historical (Excel Import)')]),
        ),
    ]
