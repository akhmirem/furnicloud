import re
from datetime import datetime

from django.conf import settings
from django.db.models import Prefetch
from django.utils import timezone

from commissions.models import Commission
from customers.models import Customer
from orders.models import Order, OrderItem


def calc_commissions_for_order(order, **kwargs):
    """

    Args:
        order: order object

    Returns:list of key-value commission information per each associate

    """

    calculated_commissions = []  # check if order has any payments entered

    # check order type
    is_real_order = True
    if order.status in ('QUOT', 'E', 'X') or order.is_quote:
        is_real_order = False

    commissions_for_order = order.commissions.all()

    # protection_plan_bonus_start_dt = datetime.strptime("2017-04-01", '%Y-%m-%d')
    # protection_plan_bonus_start_dt = timezone.make_aware(protection_plan_bonus_start_dt, timezone.get_current_timezone())
    protection_plan_bonus_start_dt = datetime.date(datetime.strptime("2017-04-01", '%Y-%m-%d'))

    split_guardian_protection_plan = False
    protection_plan_price = 0.0
    if is_real_order and kwargs.get('protection_plan_bonus'):
        if order.protection_plan and order.orderitemprotectionplan_set.count():
            guardian_protection_plan = (order.orderitemprotectionplan_set.all()[:1])[0]
            protection_plan_sale_date = guardian_protection_plan.sold_date
            protection_plan_associate = guardian_protection_plan.associate
            if protection_plan_sale_date is not None and \
                            protection_plan_sale_date >= protection_plan_bonus_start_dt:
                protection_plan_price = guardian_protection_plan.plan_price
                if protection_plan_associate is None:
                    # bonus should be split between people on commissions
                    split_guardian_protection_plan = True
                else:
                    # assign protection plan bonus to associate
                    protection_plan_bonus = _get_protection_plan_bonus(protection_plan_price)
                    calculated_commissions.append({
                        'associate': protection_plan_associate.first_name,
                        'sale': 0,
                        'eligible_amount': 0,
                        'order_subtotal': order.subtotal_after_discount,
                        'order_balance_due': order.balance_due,
                        'estimated_commissions': 0,
                        'claimed_commissions': 0,
                        'commission_status': 'Guardian Bonus',
                        'protection_plan_bonus': protection_plan_bonus,
                        'protection_plan_price': protection_plan_price
                    })

    for com in commissions_for_order:
        protection_plan_bonus = 0.0
        if is_real_order:
            sales_amount = com.sale_portion

            # calculate bonus for selling protection plan
            # only for orders starting Apr 1, 2017
            # if order.order_date >= protection_plan_bonus_start_dt:

            # if associate was not selected for Protection Plan sale, split the Guardian bonus
            # event between people entered in Commissions
            if split_guardian_protection_plan:
                protection_plan_bonus = _get_protection_plan_bonus(protection_plan_price) * \
                                        com.portion_percentage / 100.0
        else:
            sales_amount = 0.0

        comm_amount = sales_amount * settings.COMMISSION_PERCENT

        if com.associate.username in 'greg':
            if com.manager_commission_flag:
                comm_amount = order.subtotal_after_discount * 0.01
            else:
                comm_amount = 0.0

        eligible_amount = sales_amount

        temp_subtotal = {
            'associate': com.associate.first_name,
            'sale': sales_amount,
            'eligible_amount': eligible_amount,
            'order_subtotal': order.subtotal_after_discount,
            'order_balance_due': order.balance_due,
            'estimated_commissions': comm_amount,
            'claimed_commissions': com.amount,
            'commission_status': com.get_status_display(),
            'protection_plan_bonus': protection_plan_bonus,
            'protection_plan_price': protection_plan_price
        }

        calculated_commissions.append(temp_subtotal)

    return calculated_commissions


def get_sales_data_from_orders(order_list, **kwargs):
    # returns a sales, commissions, and bonuses as key-val pairs by associate

    res = {}
    expand_res = {}

    # .prefetch_related(Prefetch('commissions', queryset=Commission.objects.select_related('associate'))
    order_list = order_list \
        .prefetch_related('payments') \
        .prefetch_related('ordered_items') \
        .prefetch_related(Prefetch('commissions', queryset=Commission.objects.regular().select_related('associate'))) \
        .prefetch_related('orderitemprotectionplan_set')

    for o in order_list:

        # get order subtotal and commissions
        order_commissions = calc_commissions_for_order(o, **kwargs)

        for com in order_commissions:

            # save order info into expanded result list
            if not com['associate'] in expand_res:
                expand_res[com['associate']] = []

            # get first item's title
            if o.ordered_items.exists():
                item = (o.ordered_items.all()[:1])[0].description
                if len(item) > 30:
                    item = item[:30] + "..."

                item_count = o.ordered_items.count()
                if item_count > 1:
                    item = "{0} (+{1} items)".format(item, item_count - 1)

            else:
                item = 'No items entered...'

            sales_data = {
                'order_number': o.number,
                'order_status': "{0} / {1}".format(o.get_status_display(), o.get_order_type_display()),
                'order_date': datetime.strftime(o.order_date, '%m-%d-%Y'),
                'item': item,
                'amount': com['sale'],
                'order_subtotal': com['order_subtotal'],
                'eligible_amount': com['eligible_amount'],
                'order_balance_due': com['order_balance_due'],
                'estimated_commissions': com['estimated_commissions'],
                'claimed_commissions': com['claimed_commissions'],
                'commission_status': com['commission_status'],
                'order_pk': o.pk,
                'bonus_elig_date': o.bonus_elig_date,
                'protection_plan_bonus': com['protection_plan_bonus'],
                'protection_plan_price': com['protection_plan_price'],
            }
            expand_res[com['associate']].append(sales_data)

            # count up subtotals
            if com['associate'] in res:
                res[com['associate']]['sale'] += com['sale']
                res[com['associate']]['eligible_amount'] += com['eligible_amount']
                res[com['associate']]['protection_plan_bonus'] += com['protection_plan_bonus']
            else:
                res[com['associate']] = com

    sales_list = []
    for associate, temp_subtotal in res.items():

        if 'old_bonus' in kwargs.keys() and kwargs.get('old_bonus'):
            sales_bonus = _calc_bonus_amount__old(temp_subtotal['sale'])
        else:
            sales_bonus = _calc_bonus_amount(temp_subtotal['sale'])

        protection_plan_bonus = 100 if temp_subtotal['protection_plan_bonus'] >= 100.0 else 0.0

        sales_list.append({
            'associate': associate,
            'sales': temp_subtotal['sale'],
            'eligible_amount': temp_subtotal['eligible_amount'],
            'protection_plan_bonus': temp_subtotal['protection_plan_bonus'],
            'protection_plan_extra_bonus': protection_plan_bonus,
            'bonus': sales_bonus
        })

    return sales_list, expand_res


def get_protection_plan_bonus(from_date, to_date):
    # protection_plan_bonus_start_dt = datetime.strptime("2017-04-01", '%Y-%m-%d')
    # protection_plan_bonus_start_dt = timezone.make_aware(protection_plan_bonus_start_dt,
    #                                                      timezone.get_current_timezone())
    #
    # protection_plan_qs = OrderItemProtectionPlan.objects\
    #     .filter(sold_date__gte=from_date, sold_date__lte=to_date)\
    #     .select_related('order')\
    #
    #
    # for protection_plan in protection_plan_qs:
    #     protection_plan_sale_date = protection_plan.sold_date
    #     if protection_plan_sale_date is not None and protection_plan_sale_date >= protection_plan_bonus_start_dt:
    #         protection_plan_price = protection_plan.plan_price
    #         associates_cnt = protection_plan.order.commissions_set.count()
    #         protection_plan_bonus = _get_protection_plan_bonus(protection_plan_price) / float(associates_cnt)

    from_date = datetime.date(from_date)
    to_date = datetime.date(to_date)
    protection_plan_bonus_orders = Order.objects.filter(orderitemprotectionplan__sold_date__gte=from_date,
                                                        orderitemprotectionplan__sold_date__lte=to_date) \
        .select_related('store') \
        .select_related('customer')

    bonus_list, expand_res = get_sales_data_from_orders(protection_plan_bonus_orders, protection_plan_bonus=True)

    # remove info with zero guardian plan bonus
    actual_bonus_list = []
    for i, entry in enumerate(bonus_list):
        if entry.get('protection_plan_bonus', 0.0) == 0.0:
            del expand_res[entry.get('associate')]
        else:
            actual_bonus_list.append(entry)

    return actual_bonus_list, expand_res


def _calc_bonus_amount(sales_amount):
    bonus = 0.0

    if 20000.0 <= sales_amount < 25000.0:
        bonus = 50.0
    elif 25000.0 <= sales_amount < 30000.0:
        bonus = 100.0
    elif 30000.0 <= sales_amount < 35000.0:
        bonus = 175.0
    elif 35000.0 <= sales_amount < 40000.0:
        bonus = 250.0
    elif 40000.0 <= sales_amount < 45000.0:
        bonus = 350.0
    elif 45000.0 <= sales_amount < 50000.0:
        bonus = 450.0
    elif 50000.0 <= sales_amount < 55000.0:
        bonus = 575.0
    elif 55000.0 <= sales_amount < 60000.0:
        bonus = 700.0
    elif 60000.0 <= sales_amount < 65000.0:
        bonus = 825.0
    elif 65000.0 <= sales_amount < 70000.0:
        bonus = 975.0
    elif 70000.0 <= sales_amount < 75000.0:
        bonus = 1125.0
    elif 75000.0 <= sales_amount < 80000.0:
        bonus = 1300.0
    elif 80000.0 <= sales_amount < 85000.0:
        bonus = 1475.0
    elif 85000.0 <= sales_amount < 90000.0:
        bonus = 1675.0
    elif 90000.0 <= sales_amount < 95000.0:
        bonus = 1900.0
    elif 95000.0 <= sales_amount < 100000.0:
        bonus = 2150.0
    elif 100000.0 <= sales_amount < 105000.0:
        bonus = 2500.0
    elif 105000.0 <= sales_amount < 110000.0:
        bonus = 2625.0
    elif 110000.0 <= sales_amount < 115000.0:
        bonus = 2750.0
    elif 115000.0 <= sales_amount < 120000.0:
        bonus = 2900.0
    elif 120000.0 <= sales_amount < 125000.0:
        bonus = 3100.0
    elif 125000.0 <= sales_amount < 130000.0:
        bonus = 3250.0
    elif 130000.0 <= sales_amount < 135000.0:
        bonus = 3400.0
    elif 135000.0 <= sales_amount < 140000.0:
        bonus = 3550.0
    elif 140000.0 <= sales_amount < 145000.0:
        bonus = 3700.0
    elif 145000.0 <= sales_amount < 150000.0:
        bonus = 3850.0
    elif 150000.0 <= sales_amount < 155000.0:
        bonus = 4000.0
    elif 155000.0 <= sales_amount < 160000.0:
        bonus = 4200.0
    elif 160000.0 <= sales_amount < 165000.0:
        bonus = 4400.0
    elif 165000.0 <= sales_amount < 170000.0:
        bonus = 4600.0
    elif 170000.0 <= sales_amount < 175000.0:
        bonus = 4800.0
    elif 175000.0 <= sales_amount < 180000.0:
        bonus = 5000.0
    elif 180000.0 <= sales_amount < 185000.0:
        bonus = 5200.0
    elif 185000.0 <= sales_amount < 190000.0:
        bonus = 5400.0
    elif 190000.0 <= sales_amount < 195000.0:
        bonus = 5600.0
    elif 195000.0 <= sales_amount < 200000.0:
        bonus = 5800.0
    elif sales_amount >= 200000.0:
        bonus = 6000.0
    return bonus


def _calc_bonus_amount__old(sales_amount):
    bonus = 0.0

    if 25000.0 <= sales_amount < 35000.0:
        bonus = 50.0
    elif 35000.0 <= sales_amount < 45000.0:
        bonus = 100.0
    elif 45000.0 <= sales_amount < 50000.0:
        bonus = 150.0
    elif 50000.0 <= sales_amount < 60000.0:
        bonus = 200.0
    elif 60000.0 <= sales_amount < 75000.0:
        bonus = 250.0
    elif sales_amount >= 75000.0:
        bonus = 300.0

    return bonus


def _get_protection_plan_bonus(plan_price):
    bonus = 0.0

    if plan_price <= 149.0:
        bonus = 7.0
    elif plan_price <= 199.0:
        bonus = 10.0
    elif plan_price <= 249.0:
        bonus = 15.0
    elif plan_price <= 299.0:
        bonus = 20.0

    return bonus


def is_valid_order_number(number):
    return re.match(settings.ORDER_FORMAT_REGEX, number) is not None


def is_duplicate_order_exists(number, instance):
    try:
        o = Order.objects.get(number__iexact=number)

        if instance is not None and instance.pk and o.pk == instance.pk:
            return False

        return True

    except Order.DoesNotExist:
        return False


def get_order_associates(order):
    assoc_list = []
    comm_queryset = order.commissions.select_related().regular()
    for com in comm_queryset:
        assoc_list.append(com.associate.first_name)

    return ", ".join(assoc_list)


def list_unconfirmed_orders(by_associate=None):
    launch_dt = timezone.make_aware(datetime(2016, 9, 1))

    # items that have PO# but don't have Acknowledgement #
    unconfirmed_items = OrderItem.objects \
        .exclude(po_num="") \
        .filter(ack_num="") \
        .select_related('order') \
        .prefetch_related('order__commissions__associate') \
        .filter(order__order_date__gte=launch_dt)

    if by_associate:
        # filter by specific associate
        unconfirmed_items = unconfirmed_items.filter(order__commissions__associate__exact=by_associate)

    res = set([i.order for i in unconfirmed_items if i.order.status not in ('C', 'I', 'X', 'D', 'E', 'QUOT')
               and i.order.order_type in ('stock', 'special_order')])
    res = sorted(res, key=lambda o: o.order_date)
    return res


def get_default_customer():
    default_customer, status = Customer.objects.get_or_create(first_name='N/A', last_name='N/A',
                                                              email="default@email.com")
    return default_customer


def is_order_valid_for_closure(order):
    ok_to_close = True

    # is the current order status 'Dummy' or 'Quote'
    if ok_to_close:
        if order.status in ('X', 'QUOT') or order.is_quote:
            ok_to_close = False

    # is there balance due?
    if ok_to_close:
        if order.balance_due > 1:
            ok_to_close = False

    # any unpaid commissions?
    if ok_to_close:
        if any(c for c in order.commissions.regular() if c.status != 'PAID'):
            ok_to_close = False

    # any special order items not having Delivered status
    if ok_to_close:
        if any(i for i in order.ordered_items.all() if i.status not in ('D', 'R', 'S')):
            ok_to_close = False

    return ok_to_close


def export_sales_summary_to_file(from_date, to_date, output_dir):
    lookup_kwargs = {
        '%s__gte' % 'order_date': from_date,
        '%s__lt' % 'order_date': to_date,
    }
    qs = Order.objects.filter(**lookup_kwargs)

    import csv
    import os
    file_name = os.path.join(output_dir, "orders.csv")
    with open(file_name, 'w') as csvfile:
        fieldnames = ('number', 'order_date', 'status', 'customer_fullname', 'associate', 'deposit_balance',
                      'balance_due', 'subtotal', 'grand_total')
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for o in qs:
            customer = ""
            try:
                customer = o.customer.first_name + " " + o.customer.last_name
            except Exception:
                pass
            associate = ""
            try:
                associate = ", ". join([c.associate.first_name for c in o.commissions.regular()])
            except Exception:
                pass

            row = {
                'number': o.number,
                'order_date': o.order_date.strftime("%m/%d/%Y"),
                'status': o.get_status_display(),
                'customer_fullname': customer,
                'associate': associate,
                'deposit_balance': o.balance,
                'balance_due': o.balance_due,
                'subtotal': o.subtotal_after_discount,
                'grand_total': o.grand_total,
            }
            writer.writerow(row)
