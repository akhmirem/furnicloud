from datetime import datetime, timedelta

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone

from core.utils import date_with_tz


def apply_cutoff_date_to_qs(qs, cutoff_date):
    return qs.filter(order_date__gte=cutoff_date)


class OrderManager(models.Manager):
    launch_dt = datetime(2014, 5, 1)
    if settings.USE_TZ:
        launch_dt = timezone.make_aware(launch_dt, timezone.get_current_timezone())

    def get_qs(self):
        filter_kwargs = {
            'order_type__in': ('stock', 'special_order', 'quote')
        }
        qs = super(OrderManager, self).get_queryset().filter(~Q(status='I')).filter(**filter_kwargs)
        return qs

    def get_dated_qs(self, start, end):
        lookup_kwargs = {
            '%s__gte' % 'order_date': start - timedelta(minutes=1),
            '%s__lt' % 'order_date': end,
        }
        return super(OrderManager, self).get_queryset().filter(**lookup_kwargs)

    def unplaced_orders(self):
        """ queryset that find unplaced special orders, for which there is no PO number specified """
        return self.get_qs().filter(~Q(status__in=('I', 'QUOT', 'E', 'X', 'C', 'D'))) \
            .filter(order_type='special_order') \
            .filter(~Q(ordered_items__status__in='S') & (Q(ordered_items__po_num="") | Q(ordered_items__ack_num=""))) \
            .distinct()

    def open_orders(self):
        """ Returns a queryset with open orders (for which status is not 'Closed') """
        return self.get_qs().filter(~Q(status__in=('C', 'D')))

    def special_orders(self):
        """ Returns a queryset with active special orders (status is New SO, Ordered, Received, Scheduled for Delivery, and Partially Delivered) """
        return self.get_qs().filter(
            Q(status__in=('NEW_SO', 'ORDERED', 'R', 'S', 'PARTIALLY_DELIVERED'))) 

    def financing_unactivated(self):
        """ returns a queryset for orders with financing option selected but unactivated """
        qs = apply_cutoff_date_to_qs(self.get_qs(), date_with_tz(datetime(2020, 1, 1)))
        return qs.filter(Q(financing_option=True) & Q(orderfinancing__isnull=True))

    def delivered_orders(self):
        """ returns a queryset for orders with delivered status """
        qs = self.get_qs()
        return qs.filter(status__exact='D')
