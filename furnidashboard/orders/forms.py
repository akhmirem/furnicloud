import json
import logging
from operator import is_
from secrets import choice

from ajax_select.fields import AutoCompleteSelectField
from bootstrap3_datepicker.widgets import DatePickerInput
from crispy_forms.bootstrap import AppendedText
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Div, Field, HTML
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.forms import ModelChoiceField
from django.forms.models import inlineformset_factory, modelformset_factory, BaseInlineFormSet
from django.forms.utils import ErrorList, flatatt
from django.utils.encoding import force_text
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

import core.utils as utils
import orders.utils as order_utils
from commissions.models import Commission
from core.mixins import DisabledFieldsMixin
from customers.models import Customer
from .models import Order, OrderItem, OrderDelivery, OrderAttachment, OrderIssue, OrderItemProtectionPlan, \
    OrderFinancing, OrderPayment, OrderNote

logger = logging.getLogger("furnicloud")
DATEPICKER_OPTIONS = {"format": "mm/dd/yyyy",
                      "todayHighlight": "true",
                      "todayBtn": 'linked'}
DATE_FORMAT = "%m/%d/%Y"


class NamesChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return '{firstname} {lastname}'.format(firstname=obj.first_name, lastname=obj.last_name)


class CustomDatePickerInput(DatePickerInput):
    Media = None  # unset media subclass due to errors in Django v2

    @property
    def media(self):
        js = ('bootstrap3_datepicker/js/bootstrap-datepicker.min.js',)
        css = {'all':
               ('bootstrap3_datepicker/css/bootstrap-datepicker3.min.css',), }
        return forms.Media(js=js, css=css)

    def render(self, name, value, attrs=None, renderer=None):
        if value is None:
            value = ''
        input_attrs = self.build_attrs(
            attrs, extra_attrs={'type': self.input_type, 'name': name})
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            input_attrs['value'] = force_text(self.format_value(value))
        input_attrs = dict(
            [(key, conditional_escape(val))
             for key, val in input_attrs.items()])  # python2.6 compatible
        if not self.picker_id:
            self.picker_id = input_attrs.get('id', '') + '_picker'
        self.div_attrs['id'] = self.picker_id
        picker_id = conditional_escape(self.picker_id)
        div_attrs = dict(
            [(key, conditional_escape(val))
             for key, val in self.div_attrs.items()])  # python2.6 compatible
        icon_attrs = dict([(key, conditional_escape(val))
                           for key, val in self.icon_attrs.items()])
        html = self.html_template % dict(div_attrs=flatatt(div_attrs),
                                         input_attrs=flatatt(input_attrs),
                                         icon_attrs=flatatt(icon_attrs))
        if not self.options and self.options is not None:
            js = ''
        else:
            js = self.js_template % dict(
                picker_id=picker_id,
                options=json.dumps(self.options or {}))
        return mark_safe(force_text(html + js))


class OrderItemForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):

        self.order_type = kwargs.pop('order_type', None)
        self.is_quote = kwargs.pop('is_quote', False)
        super(OrderItemForm, self).__init__(*args, **kwargs)

        is_newly_created = self.instance is None or self.instance.pk is None

        if self.order_type is None:
            if self.instance:
                self.order_type = self.instance.order.order_type

        self.fields['in_stock'].widget = forms.HiddenInput()
        # self.fields['in_stock'].widget.attrs['class'] = "order-item-in-stock"
        self.fields['description'].widget.attrs['class'] = "order-item-desc"
        self.fields['description'].required = True

        self.fields['status'].required = True
        self.fields['status'].widget.attrs['class'] = "order-item-status"
        self.fields['status'].choices = self.get_filtered_status_choices()

        if self.order_type == 'stock':
            self.fields['status'].initial = 'S'
        else:
            self.fields['status'].initial = 'P'

        self.fields['description'].widget.attrs['size'] = 80
        self.fields['po_num'].widget.attrs['class'] = "order-item-po"
        self.fields['po_num'].label = "PO #"
        self.fields['po_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['po_date'].label = "PO placed date"
        self.fields['po_date'].widget.attrs['class'] = "order-item-po-date"
        self.fields['ack_num'].widget.attrs['class'] = "order-item-ack-num"
        self.fields['ack_num'].label = "Acknowledgement #"
        self.fields['ack_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['ack_date'].label = "Ack. date"
        self.fields['ack_date'].widget.attrs['class'] = "order-item-ack-date"

        self.fields['eta'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['eta'].widget.attrs['class'] = "order-item-eta clear"
        self.fields['eta'].label = "ETA"
        
        self.fields['container_num'].label = "Container #"
        self.fields['container_num'].widget.attrs['class'] = "order-item-container-num"

        self.fields['order_last_checked_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['order_last_checked_date'].widget.attrs['class'] = "order-item-last-checked-date clear"
        self.fields['order_last_checked_date'].label = "Last Checked On"
        self.fields['special_order_tracking_desc'].label = "Current Tracking Info"
        self.fields['special_order_tracking_desc'].widget.attrs['rows'] = 3
        self.fields['special_order_tracking_desc'].widget.attrs['class'] = "order-item-tracking-desc"

        self.fields['cost'].widget.attrs['class'] = "order-item-cost"
        self.fields['price'].widget.attrs['class'] = "order-item-price"
        self.fields['category'].widget.attrs['class'] = "order-item-category"
        self.fields['category'].required = True
        self.fields['vendor'].required = True
        if self.order_type != 'cabinetry':
            self.fields['cost'].widget = forms.HiddenInput()
            self.fields['price'].widget = forms.HiddenInput()

        # mark POS item# as required for new orders
        if is_newly_created:
            self.fields['item_num'].required = True

    def get_filtered_status_choices(self):
        choices = list(OrderItem.ITEM_STATUSES)
        remove_list = ['D', 'I']

        if self.order_type == 'stock':
            remove_list.append('P')
            remove_list.append('O')
            remove_list.append('R')
        elif self.order_type == 'special_order':
            remove_list.append('S')
            remove_list.append('QUOT')
            remove_list.append('Q')
        elif self.order_type in ('quote', 'cabinetry_quote') or self.is_quote:
            remove_list.append('O')
            remove_list.append('R')

        if self.instance is not None:
            status = self.instance.status
            if status in remove_list:
                remove_list.remove(status)  # leave selected status

        choices = filter(lambda c: c[0] not in remove_list, choices)

        return tuple(choices)

    def clean(self):
        """
        By default, 'In Stock' status is auto selected for order item.
        If 'In stock' checkbox is selected, status is reset to 'In Stock'
        and special order related values are cleared and hidden. Otherwise,
        if 'Pending' is selected, in stock indicator is cleared and special
        order values are shown. 'Ordered', 'Received', or 'Delivered' are not
        available if PO number is not entered. 'Pending' is no available if
        PO number is entered.
        """
        cleaned_data = super(OrderItemForm, self).clean()
        status = cleaned_data.get("status")
        in_stock_status = cleaned_data.get("in_stock")
        if status in ('O', 'R', 'D') and not in_stock_status:  # ordered, received, or delivered
            # check that all ordered items have PO num and date
            po_num = cleaned_data.get('po_num')
            po_date = cleaned_data.get('po_date')
            if po_num is None or po_date is None:
                msg = "Specify PO# and PO Date before changing item status"
                # self.add_error('status', msg)
                self._errors['status'] = ErrorList(
                    [u'Cannot change item status.'])
                raise forms.ValidationError(msg)

        return cleaned_data

    # def get_cleaned_choices(self):
    #     choices = list(OrderItem.ITEM_STATUSES)
    #
    #     status = ''
    #     if self.instance is not None:
    #         status = self.instance.status
    #
    #     remove_list = ['D', 'I']
    #     if status in remove_list:
    #         remove_list.remove(status)  # leave selected status
    #
    #     choices = filter(lambda c: c[0] not in remove_list, choices)
    #
    #     return choices

    class Meta:
        model = OrderItem
        fields = "__all__"


class OrderForm(forms.ModelForm):
    customer = AutoCompleteSelectField('customers', required=False)

    def __init__(self, *args, **kwargs):

        self.order_type = kwargs.pop('order_type', None)
        self.is_quote = kwargs.pop('is_quote', None)
        super(OrderForm, self).__init__(*args, **kwargs)

        if self.order_type is None:
            if self.instance:
                self.order_type = self.instance.order_type
        if self.is_quote is None:
            if self.instance:
                self.is_quote = self.instance.is_quote
            else:
                self.is_quote = False

        self.fields['number'].label = "Order/Receipt #"
        self.fields['number'].required = True

        self.fields['status'].required = True
        self.fields['status'].choices = self.get_cleaned_choices()
        if self.order_type == 'stock':
            self.fields['status'].initial = 'N'
        else:
            self.fields['status'].initial = 'NEW_SO'

        # self.fields['order_date'].widget = DateTimePicker(options=DATETIMEPICKER_OPTIONS)
        self.fields['order_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['order_date'].label = "Ordered Date"
        self.fields['order_date'].widget.attrs['class'] += " order-date"

        self.fields['referral'].required = True
        # remove outdated choices for newly-created orders
        if self.instance and not self.instance.pk:
            self.fields['referral'].choices = filter(
                lambda c: c[0] not in ('RET', 'RVL'), self.fields['referral'].choices)

        # default to Sacramento store
        self.fields['store'].initial = 1

        self.fields['order_type'].widget = forms.HiddenInput()
        self.fields['delivered_date'].widget = forms.HiddenInput()
        self.fields['is_quote'].widget = forms.HiddenInput()

        if self.is_new_order() or len(self.instance.comments) == 0:
            self.fields['comments'].widget = forms.HiddenInput()

    def is_new_order(self):
        return not self.instance or not self.instance.pk

    def get_cleaned_choices(self):
        choices = list(Order.ORDER_STATUSES)
        remove_list = ['H', 'P', 'T', 'X', 'I', 'C', 'Q']

        if self.is_quote:
            remove_list = [c[0] for c in choices if c[0] != 'QUOT']
        else:
            if self.order_type == 'stock':
                remove_list.append('NEW_SO')
                remove_list.append('ORDERED')
                remove_list.append('QUOT')
                remove_list.append('R')
            elif self.order_type == 'special_order':
                remove_list.append('N')
                remove_list.append('QUOT')

        if self.instance is not None:
            status = self.instance.status
            if status in remove_list:
                remove_list.remove(status)  # leave selected status

        choices = filter(lambda c: c[0] not in remove_list, choices)

        return tuple(choices)

    def clean_number(self):
        """
        SO number validation: make sure that order
        numbers are valid according to specific format
        """
        number = self.cleaned_data.get('number')
        if not order_utils.is_valid_order_number(number):
            raise forms.ValidationError(
                "Order number should be in the following format: %s" % settings.ORDER_FORMAT_DESC)
        elif order_utils.is_duplicate_order_exists(number, self.instance):
            raise forms.ValidationError(
                "Order with the same number already exists")
        return number

    class Meta:
        model = Order
        fields = "__all__"


class CommissionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(CommissionForm, self).__init__(*args, **kwargs)

        # self.fields['paid_date'].widget = CustomDatePickerInput(**DATEPICKER_OPTIONS)
        # self.fields['paid_date'].widget.attrs['layout'] = 'inline'

        selected_associates = self.get_selected_associates_for_order()
        user_model = get_user_model()
        assoc_queryset = user_model.objects \
            .filter(Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers")) \
            .filter(Q(is_active=True) | Q(pk__in=selected_associates)) \
            .order_by('username') \
            .distinct()
        self.fields['associate'] = NamesChoiceField(
            queryset=assoc_queryset, required=True)

        # self.fields['comments'].widget.attrs['rows'] = 3

        self.fields['portion_percentage'].initial = 100.0
        self.fields['status'].initial = "NEW"

        if self.is_order_delivered() and not self.can_update_commission():
            # person can modify only certain delivery info data
            self.fields['paid_date'].widget.attrs['readonly'] = 'true'
            self.fields['paid'].widget.attrs['readonly'] = 'true'
            self.fields['status'].widget.attrs['readonly'] = 'true'
            self.fields['amount'].widget.attrs['readonly'] = 'true'
            self.fields['comments'].widget.attrs['readonly'] = 'true'
            self.fields['portion_percentage'].widget.attrs['readonly'] = 'true'

    def clean(self):
        super(CommissionForm, self).clean()
        portion_percentage = self.cleaned_data['portion_percentage']
        if not portion_percentage or portion_percentage < 0 or portion_percentage > 100:
            self._errors['portion_percentage'] = ErrorList(
                [u'Please enter a valid sales % portion'])

        if not self.cleaned_data['amount']:
            self.cleaned_data['amount'] = 0.0

        if self.is_order_delivered() and not self.can_update_commission():
            instance = getattr(self, 'instance', None)
            self.cleaned_data['paid'] = instance.paid
            self.cleaned_data['paid_date'] = instance.paid_date
            self.cleaned_data['comments'] = instance.comments
            self.cleaned_data['status'] = instance.status
        return self.cleaned_data

    def is_order_delivered(self):
        instance = getattr(self, 'instance', None)
        return instance and instance.pk and instance.order.status == 'D'

    def can_update_commission(self):
        return self.request and self.request.user.has_perm('commissions.update_commissions_payment')

    def get_selected_associates_for_order(self):
        selected_associates = []
        if self.instance and self.instance.order_id:
            order = self.instance.order
            selected_associates = [
                c.associate.pk for c in order.commissions.all()]
        return selected_associates

    class Meta:
        model = Commission
        fields = ('associate', 'paid', 'status', 'paid_date',
                  'amount', 'comments', 'portion_percentage')
        widgets = {
            'paid': forms.HiddenInput(),
            'status': forms.HiddenInput(),
            'paid_date': forms.HiddenInput(),
            'amount': forms.HiddenInput(),
            'comments': forms.HiddenInput(),
        }


class OrderDeliveryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(OrderDeliveryForm, self).__init__(*args, **kwargs)

        self.fields['scheduled_delivery_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['scheduled_delivery_date'].label = "Delivery Slip Sent Date"
        self.fields['delivered_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['pickup_from'].initial = 1

        if not (self.instance and self.instance.delivery_slip):
            del self.fields['delivery_slip']

        # Disable Robert's Furniture Delivery
        if self.instance and self.instance.delivery_type != 'RFD':
            self.fields['delivery_type'].choices = filter(lambda c: c[0] != 'RFD',
                                                          self.fields['delivery_type'].choices)

        disabled_fields = []
        self.fields_to_disable = []
        self.fields_to_remove = []

        if self.request:
            if utils.is_user_delivery_group(self.request):
                # person can modify only certain delivery info data
                visible_fields = (
                    'scheduled_delivery_date', 'delivered_date', 'comments', 'delivery_person_notes',
                    'delivery_cost', 'paid')
                disabled_fields += ['comments', 'paid']
                self.fields_to_remove = [
                    f for f in self.fields if f not in visible_fields]

                for field in self.fields_to_remove:
                    del self.fields[field]
            elif self.request.user.groups.filter(Q(name="managers") | Q(name="associates")).exists():
                disabled_fields.append('delivery_person_notes')
                disabled_fields.append('delivery_cost')

                if not self.request.user.has_perm('orders.modify_delivery_fee'):
                    disabled_fields.append('paid')

            self.fields_to_disable = [
                f for f in self.fields if f in disabled_fields]

            for field in self.fields_to_disable:
                self.fields[field].widget.attrs['disabled'] = 'disabled'

    def clean(self):
        cleaned_data = super(OrderDeliveryForm, self).clean()
        for field in self.fields_to_disable:
            val = getattr(self.instance, field)
            cleaned_data[field] = val or self.fields[field].initial

        # validation
        try:
            if cleaned_data['delivery_type'] is None and cleaned_data['delivered_date']:
                self._errors['delivery_type'] = ErrorList(
                    [u'Please specify delivery type'])

            if cleaned_data['delivery_cost'] is None:
                cleaned_data['delivery_cost'] = 0.0

            if cleaned_data['delivery_type'] == 'SELF' and cleaned_data['delivery_cost'] > 0:
                self._errors['delivery_cost'] = ErrorList(
                    [u"Cannot assign delivery cost to 'Self Pickup' orders"])

        except KeyError as e:
            pass

        return cleaned_data

    class Meta:
        model = OrderDelivery
        fields = "__all__"
        widgets = {
            'delivery_cost': forms.HiddenInput(),
            'paid': forms.HiddenInput(),
            'pickup_from': forms.HiddenInput(),
            'delivery_person_notes': forms.HiddenInput()
        }


class CustomerForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Customer
        fields = "__all__"


class CustomerDetailReadOnlyForm(DisabledFieldsMixin, CustomerForm):
    def __init__(self, *args, **kwargs):
        super(CustomerDetailReadOnlyForm, self).__init__(*args, **kwargs)


class OrderIssueForm(forms.ModelForm):
    pass


class OrderNoteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(OrderNoteForm, self).__init__(*args, **kwargs)
        self.fields['content'].widget.attrs['rows'] = 3
        if self.instance and self.instance.type != 'SYSTEM':
            self.fields['type'].choices = filter(
                lambda c: c[0] != 'SYSTEM', self.fields['type'].choices)

    class Meta:
        model = OrderNote
        fields = "__all__"
        widgets = {
            'type': forms.RadioSelect(),
        }


# class OrderTrackingForm(forms.ModelForm):
#    def __init__(self, *args, **kwargs):
#        super(OrderTrackingForm, self).__init__(*args, **kwargs)
#
#        self.fields['tracking_date'].widget = DateTimePicker(options=DATEPICKER_OPTIONS)
#        self.fields['tracking_date'].label = "Date"
#
#    class Meta:
#        model = OrderTracking
#        fields = "__all__"


class OrderPaymentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(OrderPaymentForm, self).__init__(*args, **kwargs)

        # self.fields['order_date'].widget = BootstrapDateInput()
        # self.fields['payment_date'].widget = DateTimePicker(options=DATETIMEPICKER_OPTIONS)
        self.fields['payment_date'].widget = CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS)
        self.fields['payment_date'].label = "Payment Date"
        self.fields['payment_date'].widget.attrs['class'] += " order-date"
        self.fields['payment_date'].required = True
        self.fields['payment_date'].widget.attrs['required'] = True

        # self.fields['amount'].widget.attrs['step'] = 1
        # self.fields['amount'].widget.attrs['min'] = 0
        self.fields['amount'].widget.attrs['required'] = True
        self.fields['amount'].required = True
        self.fields['amount'].initial = 0.0

        self.fields['payment_type'].required = True
        self.fields['payment_type'].widget.attrs['required'] = True

    def clean(self):
        """
        Amount is expected to be positive for any payment type.
        But if amount is negative, only "Return/Credit" payment
        is allowed
        """
        cleaned_data = super(OrderPaymentForm, self).clean()
        amount = cleaned_data.get("amount")
        payment_type = cleaned_data.get('payment_type')
        if amount is None:
            raise forms.ValidationError("Payment amount cannot be blank")
        if amount < 0 and payment_type != 'RETURN':
            msg = "For negative payment amount, only 'Return/Credit back/Reverse' payment type is allowed"
            self.add_error('payment_type', 'Invalid Payment Type.')
            raise forms.ValidationError(msg)

        return cleaned_data

    class Meta:
        model = OrderPayment
        fields = "__all__"


class DateRangeForm(forms.Form):
    REPORT_FILTER_CHOICES = (
        ("week", "This Week"),
        ("last-week", "Last Week"),
        ("month", "This month"),
        ("last-month", "Last month"),
        ("year", "Year-to-date"),
        ("last-year", "Last Year"),
        ("ytd-last-year", "YTD Last Year"),
        ("custom", "Select date range"),
    )
    DEPARTMENT_LIST = (
        ('furniture', 'Furniture'),
        ('kitchen-and-bath', 'Kitchen & Bath'),
        ('appliances', 'Appliances'),
        ('windows', 'Hunter Douglas'),
        ('flooring', 'Flooring'),
    )

    date_range = forms.ChoiceField(label="Filter by Date Range", required=False,
                                   choices=REPORT_FILTER_CHOICES)
    range_from = forms.DateField(
        label="From", required=False, widget=CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    range_to = forms.DateField(
        label="To", required=False, widget=CustomDatePickerInput(format=DATE_FORMAT, options=DATEPICKER_OPTIONS))
    department = forms.ChoiceField(label="Select Department", required=False, choices=DEPARTMENT_LIST)

    def __init__(self, *args, **kwargs):
        super(DateRangeForm, self).__init__(*args, **kwargs)
        self.initial['department'] = 'furniture'

        self.helper = FormHelper()
        self.form_tag = False
        self.disable_csrf = True
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'
        self.helper.layout = Layout(
            Div(
                HTML(
                    '<h3>Filter Orders</h3>'
                ),
                'date_range',
                'range_from',
                'range_to',
                'department',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )


class ProtectionPlanForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProtectionPlanForm, self).__init__(*args, **kwargs)

        # self.fields['sold_date'].widget = DateTimePicker(options=DATEPICKER_OPTIONS)
        self.fields['sold_date'].label = "Plan Sold Date"
        user_model = get_user_model()
        self.fields['associate'].queryset = user_model.objects.filter(
            Q(groups__name__icontains="associates") | Q(groups__name__icontains="managers"))
        self.fields['associate'].label = "Bonus Associate"
        self.helper = ProtectionPlanFormHelper()

    class Meta:
        model = OrderItemProtectionPlan
        fields = "__all__"


class ProtectionPlanFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ProtectionPlanFormHelper, self).__init__(*args, **kwargs)
        self.form_tag = False
        self.disable_csrf = True
        self.layout = Layout(
            Div(
                AppendedText(
                    'sold_date', '<span class="glyphicon-calendar glyphicon"></span>', css_class="date-input"),
                'associate',
                AppendedText('plan_price', '$', active=True),
                'approval_no',
                'details',
                css_class='item-general-fields',
            ),
            Div(
                Field('DELETE', css_class='input-small'),
            ),
        )


class BaseFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)

        super(BaseFormSet, self).__init__(*args, **kwargs)


def class_gen_with_kwarg(cls, **additionalkwargs):
    """class generator for subclasses with additional 'stored' parameters (in a closure)
     This is required to use a formset_factory with a form that need additional
     initialization parameters (see http://stackoverflow.com/questions/622982/django-passing-custom-form-parameters-to-formset)
  """

    class ClassWithKwargs(cls):
        def __init__(self, *args, **kwargs):
            kwargs.update(additionalkwargs)
            super(ClassWithKwargs, self).__init__(*args, **kwargs)

    return ClassWithKwargs


def get_ordered_items_formset(extra=1, max_num=1000):
    return inlineformset_factory(Order, OrderItem, form=OrderItemForm, fields='__all__', extra=extra, max_num=max_num)


def get_deliveries_formset(extra=1, max_num=1000, request=None):
    formset = inlineformset_factory(Order, OrderDelivery, form=class_gen_with_kwarg(OrderDeliveryForm, request=request),
                                    fields='__all__', extra=extra, max_num=max_num,
                                    can_delete=False)
    return formset


def get_commissions_formset(extra=1, max_num=1000, request=None):
    formset = inlineformset_factory(Order, Commission, form=class_gen_with_kwarg(CommissionForm, request=request),
                                    fields='__all__',
                                    extra=extra, max_num=max_num, can_delete=True)
    return formset


def get_payments_formset(extra=1, max_num=1000, request=None):
    formset = inlineformset_factory(Order, OrderPayment, form=class_gen_with_kwarg(OrderPaymentForm, request=request),
                                    fields='__all__',
                                    extra=extra, max_num=max_num,
                                    can_delete=True)
    return formset


def get_order_issues_formset(extra=1, max_num=1000):
    return inlineformset_factory(Order, OrderIssue, form=OrderIssueForm, fields='__all__', extra=extra, max_num=max_num,
                                 can_delete=True)


def get_item_formset(extra=1, max_num=100, order_type='stock', is_quote=False):
    formset = inlineformset_factory(Order, OrderItem,
                                    form=class_gen_with_kwarg(
                                        OrderItemForm, order_type=order_type, is_quote=is_quote),
                                    fields='__all__', extra=extra, max_num=max_num)
    return formset


def get_order_notes_formset(extra=1, max_num=1000):
    return inlineformset_factory(Order, OrderNote, form=OrderNoteForm, fields='__all__', extra=extra, max_num=max_num,
                                 can_delete=True)


# inline formsets
ItemFormSet = inlineformset_factory(
    Order, OrderItem, form=OrderItemForm, fields='__all__', extra=1, max_num=100)
DeliveryFormSet = inlineformset_factory(Order, OrderDelivery, form=OrderDeliveryForm, fields='__all__', extra=1,
                                        max_num=100)
CommissionFormSet = inlineformset_factory(Order, Commission, form=CommissionForm, fields='__all__', extra=1,
                                          max_num=100, can_delete=False)
OrderPaymentFormSet = inlineformset_factory(Order, OrderPayment, form=OrderPaymentForm, fields='__all__', extra=1,
                                            max_num=100, can_delete=False)
CustomerFormSet = modelformset_factory(
    Customer, form=CustomerForm, fields='__all__', extra=1, max_num=1)
OrderAttachmentFormSet = inlineformset_factory(
    Order, OrderAttachment, fields='__all__', extra=1, max_num=5)
ProtectionPlanFormSet = inlineformset_factory(Order, OrderItemProtectionPlan, form=ProtectionPlanForm, fields='__all__',
                                              extra=1, max_num=1)
OrderFinancingFormSet = inlineformset_factory(
    Order, OrderFinancing, fields='__all__', extra=1, max_num=1)
