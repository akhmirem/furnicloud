from audit_log.models.managers import AuditLog
from django.db import models
from django.urls import reverse
from django.conf import settings
from audit_log.models import AuthStampedModel
from django.db.models import CASCADE
from django_extensions.db.models import TimeStampedModel
from core.date_utils import get_prev_month_date
from customers.models import Customer
from stores.models import Store
from orders.managers import OrderManager


class Order(TimeStampedModel, AuthStampedModel):
    """
    A model class representing Order data
    """

    class Meta:
        ordering = ["-order_date"]
        db_table = "order_info"
        permissions = (
            ("view_orders", "Can View Orders"),
            ("view_sales", "Can View Sales Reports"),
            ("update_status", "Can Update Order Status"),
            ("view_cabinetry_orders", "Can View Cabinetry Orders"),
            ("update_cabinetry_orders", "Can Update Cabinetry Orders"),
        )

    ORDER_STATUSES = (
        ('N', 'New'),
        ('NEW_SO', 'New SO'),
        ('QUOT', 'Quotation'),
        ('ORDERED', 'Ordered'),
        ('Q', 'Pending'),                                    # Deprecated
        ('H', 'On Hold'),                                    # Deprecated
        ('P', 'In Production'),                              # Deprecated
        ('T', 'In Transit'),                                 # Deprecated
        ('R', 'Received/ In Warehouse'),
        ('S', 'Scheduled for Delivery'),
        ('PARTIALLY_DELIVERED', 'Partially Delivered'),
        ('D', 'Delivered'),
        ('E', 'Cancelled'),
        ('X', 'Dummy'),                                      # Deprecated
        ('I', 'Historical (Excel Import)'),                  # Deprecated
        ('C', 'Closed'),                                     # Deprecated
    )
    ORDER_ACTIVE_STATUSES = ['N', 'NEW_SO',
                             'ORDERED', 'Q', 'P', 'T', 'R', 'S', 'PARTIALLY_DELIVERED', 'D']
    REFERRAL_SOURCES = (
        ('UNK', 'Unknown'),
        ('RVL', 'Roseville Store'),  # inactive
        ('POS', 'Purchased Before'),
        ('RET', 'Returning Customer'),  # inactive
        ('COP', 'Copenhagen Customer'),
        ('GOOG', 'Google Search'),
        ('EBL', 'E-Blast'),
        ('WEB', 'Website Direct Visit'),
        ('VENDOR', 'Vendor Website Referral'),
        ('TV', 'TV'),
        ('MAG', 'Magazine'),
        ('REF', 'Referred by friends/relatives'),
        ('SOC', 'Social networks'),
        ('BILL', 'Billboard/Sign/Drive-by'),
        ('CST', 'Referred by existing Furnitalia customer'),
        ('NWP', 'Newspaper'),
        ('EXPO', 'Home & Landscape Show'),
        ('NO', 'Not Referred'),
        ('OTH', 'Other (see comments)'),
    )

    ORDER_TYPES = (
        ('stock', 'In Stock Order'),
        ('special_order', 'Special Order'),
        ('quote', 'Quote'),
        ('cabinetry', 'German Kitchen & Bath'),
        ('cabinetry_quote', 'GermanKB Quote'),
        ('appliances', 'Appliances'),
        ('windows', 'Hunter Douglas'),
        ('design', 'Design'),
        ('flooring', 'Flooring'),
    )

    number = models.CharField(max_length=50)
    order_date = models.DateTimeField(null=True)
    customer = models.ForeignKey(
        Customer, related_name='orders', on_delete=models.CASCADE)
    status = models.CharField(max_length=25, choices=ORDER_STATUSES)
    deposit_balance = models.FloatField(blank=True, default=0.0, null=True)
    subtotal_after_discount = models.FloatField(blank=True, default=0.0)
    tax = models.FloatField(blank=True, default=0.0)
    current_balance = models.FloatField(blank=True, default=0.0)
    shipping = models.FloatField(blank=True, default=0.0)
    comments = models.TextField(blank=True)
    store = models.ForeignKey(
        Store, related_name='orders', on_delete=models.CASCADE)
    referral = models.CharField(
        blank=True, null=True, max_length=50, choices=REFERRAL_SOURCES)
    protection_plan = models.BooleanField(default=False, blank=True)
    financing_option = models.BooleanField(default=False, blank=True)
    delivered_date = models.DateTimeField(null=True, blank=True, default=None)
    order_type = models.CharField(
        blank=True, null=True, max_length=25, choices=ORDER_TYPES)
    bonus_elig_date = models.DateField(null=True, blank=True, default=None)
    is_quote = models.BooleanField(null=True, blank=True, default=False)

    # track change history
    audit_log = AuditLog()

    objects = OrderManager()  # customer manager

    @property
    def not_placed(self):
        # no status        'new'                 'any item, which is not in stock, and does not have po#
        # return not self.status or self.status == 'N' or any([item for item in self.orderitem_set if not
        # item.in_stock and not item.po_num])
        if self.status not in ('I', 'X', 'E', 'QUOT'):
            return any(
                [item for item in self.ordered_items.all() if item.status not in ("S", "I") and item.po_num == ''])
        return False

    @property
    def balance_due(self):
        """ Balance due after the deposits """
        if self.status == 'X' or self.is_quote:  # Quote, Dummy -- should be no balance
            return 0.0

        return round(self.grand_total - self.balance, 2)

    @property
    def balance(self):
        """ Current order balance """
        if self.status == 'X' or self.is_quote:  # Quote, Dummy -- should be no balance
            return 0.0

        total_paid = 0.0
        for payment in self.payments.all():
            total_paid += payment.amount

        return round(total_paid, 2)

    @property
    def grand_total(self):
        """ Grand Total (Subtotal + tax + shipping) """
        grand_total = self.subtotal_after_discount + self.tax + self.shipping
        return round(grand_total, 2)

    @property
    def tracking_alert(self):
        reason = set()
        month_ago = get_prev_month_date()
        for i in self.ordered_items.all():
            if not i.in_stock:
                if i.eta is None:
                    reason.add('ETA missing')
                if i.order_last_checked_date is None or i.order_last_checked_date < month_ago.date():
                    reason.add("Tracking Date not current")
        return ', '.join(reason)

    @property
    def total_profit(self):
        """ Total Profit amount (sum of price - cost for all items) """
        total_profit = 0.0
        for i in self.ordered_items.all():
            try:
                total_profit += i.price - i.cost
            except Exception:
                pass
        return round(total_profit, 2)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        prefix = ""
        if self.is_quote:
            prefix = "Quote - "
        return "{0}#{1}".format(prefix, self.number)

    def get_absolute_url(self):
        return reverse("order_detail", kwargs={"pk": self.pk})


class CabinetryOrder(object):
    """
    A model class representing the Kitchen/Cabinetry Order data
    """

    # objects = models.Manager()      #default
    # objects = OrderManager()  # customer manager

    # track change history
    audit_log = AuditLog()

    class Meta:
        ordering = ["-order_date"]
        db_table = "cabinetry_orders"
        permissions = (
            ("view_cabinetry_orders", "Can View Kitchen/Cabinetry Order Info"),
            ("view_cabinetry_sales", "Can View Kitchen/Cabinetry Sales Reports"),
        )

    def get_absolute_url(self):
        return reverse("cabinetry_order_detail", kwargs={"pk": self.pk})


class OrderItem(TimeStampedModel, AuthStampedModel):
    """
    A model class representing Items Ordered (stock items, special order items, etc).
    """
    class Meta:
        db_table = "order_items"
        verbose_name_plural = "ordered items"

    ITEM_STATUSES = (
        ('P', 'Unordered'),
        ('O', 'Ordered'),
        ('R', 'Received'),
        ('D', 'Delivered'),
        ('S', 'In Stock'),
        ('I', 'Historical (Excel Import)'),
    )

    ITEM_CATEGORIES = (
        ('ACCESSORIES', 'Accessories'),
        ('ACCENT_TABLES', 'Accent / Coffee / Side Tables'),
        ('APPLIANCES', 'Appliances'),
        ('APP_INSTALL', 'Appliances Installation'),
        ('ARMCHAIRS', 'Armchairs / recliners'),
        ('BACKSPLASH', 'Backsplash'),
        ('BATHROOM', 'Bathroom Cabinets'),
        ('BEDROOM', 'Bedroom beds / nightstands'),
        ('CAB_INSTALL', 'Cabinets Installation'),
        ('CABINETS', 'Cabinets'),
        ('CLOSETS', 'Closets'),
        ('COUNTER', 'Counter Tops'),
        ('DESIGN', 'Design Fee'),
        ('DINING_CHAIRS', 'Dining Chairs'),
        ('FLOORING', 'Flooring'),
        ('LIGHTING', 'Lighting'),
        ('MEDIA', 'Media Cabinets / Entertainment Units'),
        ('OFFICE', 'Office Furniture'),
        ('OUTDOOR', 'Outdoor'),
        ('PILLOWS', 'Pillows'),
        ('RUGS', 'Rugs'),
        ('SOFAS', 'Sofas / sectionals / loveseats'),
        ('TABLES', 'Tables Dining / Living'),
        ('Discount', 'Discount'),
        ('VANITIES', 'Vanities'),
        ('WALL_SYSTEMS', 'Wall Systems'),
        ('WINDOWS', 'Window Treatments'),
        ('OTHER', 'Other'),
    )

    VENDORS = (
        ('ALF_ITALIA', 'Alf Italia'),
        ('ALF_DAFRE', 'Alf DaFre'),
        ('BADEA', 'Badea'),
        ('BAUFORMAT', 'Bauformat'),
        ('BDI', 'BDI'),
        ('BONTEMPI', 'Bontempi'),
        ('CALLIGARIS', 'Calligaris'),
        ('CATTELAN_ITALIA', 'Cattelan Italia'),
        ('COSENTINO', 'Cosentino'),
        ('DCS', 'DCS'),
        ('EKORNES', 'Stressless'),
        ('ELITE_MODERN', 'Elite Modern'),
        ('ESTRO_MILANO', 'Estro Milano'),
        ('EUROFASE', 'Eurofase'),
        ('FISHER_PAYKEL', 'Fisher & Paykel'),
        ('FURNITALIA', 'Furnitalia'),
        ('GARRISON', 'Garrison'),
        ('GLOBAL_VIEWS', 'Global Views'),
        ('HUNTER_DOUGLAS', 'Hunter Douglas'),
        ('IDEA_GROUP', 'IDEA Group'),
        ('INCANTO', 'Incanto'),
        ('KALORA', 'Kalora'),
        ('KOINOR', 'Koinor'),
        ('LIEBHERR', 'Liebherr'),
        ('MIELE', 'Miele'),
        ('NATUZZI_EDITIONS', 'Natuzzi Editions'),
        ('NATUZZI_ITALIA', 'Natuzzi Italia'),
        ('NOURISON', 'Nourison'),
        ('OF', 'OF Outdoor Kitchen'),
        ('PALLISER', 'Palliser'),
        ('PIANCA', 'Pianca'),
        ('PINNACLE', 'Pinnacle'),
        ('SLAMP', 'Slamp'),
        ('SMEG', 'Smeg'),
        ('SURYA', 'Surya'),
        ('GERMANKB', 'GermanKB Other'),
        ('URBAN_BONFIRE', 'Urban Bonfire'),
        ('UTTERMOST', 'Uttermost'),
        ('VONDOM', 'Vondom'),
        ('OTHER', 'Other'),
    )

    order = models.ForeignKey(
        Order, related_name='ordered_items', on_delete=CASCADE)
    status = models.CharField(
        max_length=15, choices=ITEM_STATUSES, blank=True, null=True)
    in_stock = models.BooleanField(default=False, blank=True)
    description = models.CharField(max_length=255)
    po_num = models.CharField(max_length=125, blank=True)
    po_date = models.DateField(blank=True, null=True)
    ack_num = models.CharField(max_length=125, blank=True)
    ack_date = models.DateField(blank=True, null=True)
    eta = models.DateField(blank=True, null=True)
    order_last_checked_date = models.DateField(
        blank=True, null=True, default=None)
    special_order_tracking_desc = models.TextField(blank=True, null=True)
    category = models.CharField(
        max_length=125, choices=ITEM_CATEGORIES, blank=True, default=None, null=True)
    vendor = models.CharField(
        max_length=125, choices=VENDORS, blank=True, default=None, null=True)
    cost = models.FloatField(blank=True, default=0.0, null=True)
    price = models.FloatField(blank=True, default=0.0, null=True)
    item_num = models.CharField(
        max_length=125, blank=True, null=True, verbose_name="POS Item #")
    container_num = models.CharField(max_length=125, blank=True, null=True, default=None)

    # track change history
    audit_log = AuditLog()

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        desc = self.description
        if len(desc) > 20:
            desc = desc[:20]
        return "Item: {0}, status:{1}".format(desc, self.get_status_display())

    @property
    def profit(self):
        """ Profit amount (price - cost """
        try:
            profit = self.price - self.cost
            return profit
        except Exception:
            return 0


class OrderDelivery(TimeStampedModel, AuthStampedModel):
    """
    A model class representing deliveries tracking for Orders
    """

    DELIVERY_TYPES = (
        ('SELF', 'Self Pickup/ Will Call'),
        ('MGL', 'Miguel'),
        ('FURNITALIA', 'Furnitalia'),
        ('RFD', 'Roberts Furniture Delivery'),
        ('CUSTOM', 'Other'),
    )

    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    delivery_type = models.CharField(
        max_length=25, choices=DELIVERY_TYPES, blank=True, null=True)
    scheduled_delivery_date = models.DateField(null=True, blank=True)
    delivered_date = models.DateField(null=True, blank=True)
    pickup_from = models.ForeignKey(
        Store, blank=True, null=True, on_delete=models.SET_NULL)
    delivery_slip = models.FileField(
        upload_to='deliveries/%Y/%m', blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    delivery_person_notes = models.TextField(blank=True, null=True)
    delivery_cost = models.FloatField(blank=True, default=0.0)
    paid = models.BooleanField(default=False, blank=True)

    def get_absolute_url(self):
        return reverse("delivery_detail", kwargs={"pk": self.pk})

    @property
    def delivery_slip_filename(self):
        if self.delivery_slip:
            import os
            return os.path.basename(self.delivery_slip.name)
        else:
            return ""

    class Meta:
        db_table = "deliveries"
        verbose_name_plural = "deliveries"
        permissions = (
            ("modify_delivery_fee", "Modify Delivery Fee"),
        )


class Attachment(models.Model):
    file = models.FileField(upload_to='attachments/%Y/%m')
    description = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        abstract = True

    @property
    def filename(self):
        import os
        return os.path.basename(self.file.name)

    def __unicode__(self):
        return self.description[:30]


class OrderAttachment(Attachment):
    """
    A class representing file attachments for an order
    """
    order = models.ForeignKey(Order, on_delete=CASCADE)

    class Meta:
        db_table = "order_attachments"


class OrderIssue(TimeStampedModel, AuthStampedModel):
    """
    A class representing order issues
    """
    ISSUE_STATUSES = (
        ('N', 'New'),
        ('C', 'Claim submitted'),
        ('T', 'Technician sent'),
        ('E', 'Eligible for Credit'),
        ('R', 'Resolved'),
    )

    order = models.ForeignKey(Order, on_delete=CASCADE)
    status = models.CharField(max_length=5, choices=ISSUE_STATUSES)
    comments = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "order_issues"
        ordering = ['-created']
        permissions = (
            ("update_order_issues", "Can Update Order Issues (Claims)Information"),
        )


class OrderFinancing(models.Model):
    """
    Stores financing details for sold orders
    """

    order = models.ForeignKey(Order, on_delete=CASCADE)
    approval_no = models.CharField(max_length=50, blank=False, null=False)
    details = models.TextField(blank=True, null=True)


class OrderItemProtectionPlan(models.Model):
    """
    Stores protection plan details for sold items
    """

    order = models.ForeignKey(Order, on_delete=CASCADE)
    sold_date = models.DateField(blank=True, null=True)
    associate = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, blank=True,
                                  related_name='protection_plans', on_delete=models.CASCADE)
    plan_price = models.FloatField(blank=True, default=0.0)
    approval_no = models.CharField(max_length=50, blank=False, null=False)
    details = models.TextField(blank=True, null=True)

    class Meta:
        pass
        # db_table = "order_protection_plan"
        # verbose_name_plural = "order_protection_plans"
        # permissions = (
        #     ("update_order_payments", "Can Update Order Payments"),
        # )


class OrderPayment(TimeStampedModel, AuthStampedModel):
    """
    Stores order payment history
    """

    PAYMENT_TYPES = (
        ('CASH', 'Cash'),
        ('CREDIT', 'Credit Card'),
        ('DEBIT', 'ATM/Debit Card'),
        ('CHECK', 'Check'),
        ('FINANCING', 'Financing Plan'),
        ('ACCOUNT', 'Charge to Account'),
        ('RETURN', 'Return/Credit back/Reverse'),
        ('OTHER', 'Other'),
    )

    order = models.ForeignKey(
        Order, related_name='payments', on_delete=CASCADE)
    payment_date = models.DateTimeField(null=False)
    payment_type = models.CharField(max_length=30, choices=PAYMENT_TYPES)
    amount = models.FloatField(blank=True, default=0.0)
    comments = models.TextField(blank=True, null=True)

    # track change history
    audit_log = AuditLog()

    class Meta:
        db_table = "order_payments"
        verbose_name_plural = "order_payments"
        ordering = ['-payment_date']
        # permissions = (
        #     ("update_order_payments", "Can Update Order Payments"),
        # )

    def __str__(self):
        return '<Payment> %s $%d %s, order:%s'.format(self.payment_date.strftime(settings.DATE_FORMAT_STANDARD),
                                                      self.amount, self.payment_type, self.order)

    def __unicode__(self):
        return '<Payment> %s $%d %s, order:%s'.format(self.payment_date.strftime(settings.DATE_FORMAT_STANDARD),
                                                      self.amount, self.payment_type, self.order)


class OrderNote(TimeStampedModel, AuthStampedModel):
    NOTE_TYPES = (
        ('GENERAL', 'General'),
        ('ALERT', 'Important'),
        ('SYSTEM', 'Auto-generated'),
    )

    order = models.ForeignKey(Order, on_delete=CASCADE, related_name="notes")
    type = models.CharField(max_length=25, choices=NOTE_TYPES, default="GENERAL")
    content = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "order_notes"
        ordering = ['-created']


# class OrderTracking(TimeStampedModel, AuthStampedModel):
#    """
#    This model was created for tracking special order's ETA, order correctness, etc
#    """
#    TRACKING_STATUS = (
#        ('OK', 'Order correct / On Schedule'),
#        ('ALERT', 'ALERT: Order Incorrect/ Not placed'),
#        ('FINISH', 'Order Closed'),
#        ('OTHER', 'OTHER: see comments'),
#    )
#    order = models.ForeignKey(Order, related_name='tracking_log')
#    tracking_date = models.DateTimeField(null=False)
#    tracking_status = models.CharField(max_length=5, choices=TRACKING_STATUS)
#    comments = models.TextField(blank=True, null=True)
#
#    class Meta:
#        db_table = "order_tracking"
#        ordering = ['-tracking_date']
#
#    def __str__(self):
#        return '<Tracking Log> %s %s, order:%s'.format(self.tracking_date.strftime(settings.DATE_FORMAT_STANDARD),
#                                                       self.tracking_status, self.order)
#
#    def __unicode__(self):
#        return '<Tracking Log> %s %s, order:%s'.format(self.tracking_date.strftime(settings.DATE_FORMAT_STANDARD),
#                                                       self.tracking_status, self.order)
