from customers.models import Customer
from orders.models import Order, OrderItem
from rest_framework.relations import PrimaryKeyRelatedField, StringRelatedField
from rest_framework import serializers
from warehouse.models import IncomingShipment, IncomingShipmentItem, IncomingShipmentFile
import logging

logger = logging.getLogger('furnicloud')


class OrderItemSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='get_status_display')
    so_num = serializers.CharField(source='order.number', read_only=True)
    order_id = serializers.IntegerField(read_only=True)

    class Meta:
        model = OrderItem
        fields = ('pk', 'order_id', 'so_num', 'status', 'description', 'po_num', 'po_date', 'ack_num', 'ack_date')


class OrderSerializer(serializers.ModelSerializer):
    customer_name = StringRelatedField(read_only=True, source='customer')
    customer = PrimaryKeyRelatedField(many=False,
                                      queryset=Customer.objects.all(),
                                      required=True)
    store = StringRelatedField()
    status_label = serializers.CharField(read_only=True, source='get_status_display')
    balance_due = serializers.FloatField(read_only=True)

    class Meta:
        model = Order
        # fields = ('pk', 'number', 'order_date', 'customer', 'status', 'store', 'subtotal_after_discount')
        fields = '__all__'

    def get_status(self, obj):
        return obj.get_status_display()