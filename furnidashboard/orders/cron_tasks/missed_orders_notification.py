from datetime import datetime

from django.template.loader import render_to_string

import orders.cron_tasks.utils as cron_utils
from orders.models import Order
from stores.models import Store


DELETED_ORDER_NUMS = [13965, 14020, 14089, 14090,
                      14137, 14138, 14166, 14210, 14311, 14319, 14711]
QUOTATION_ORDER_NUMS = [14000, 14006, 14010, 14013, 14026, 14043, 14046, 14084, 14291, 14292, 14302, 14303, 14322, 14332, 14338, 14341, 14354, 14356, 14361, 14365, 14368, 14370, 14379, 14380, 14384, 14386, 14401, 14421, 14427, 14428,
                        14431, 14432, 14434, 14435, 14441, 14442, 14464, 14469, 14470, 14471, 14473, 14489, 14493, 14501, 14502, 14506, 14515, 14518, 14523, 14524, 14537, 14539, 14540, 14545, 14558, 14565, 14571, 14577, 14580, 14585, 14588, 14590, 14612]
ORDER_NUMS_TO_EXCLUDE = DELETED_ORDER_NUMS + QUOTATION_ORDER_NUMS


def run_missed_orders_cron():
    orders_missing = get_skipped_orders()
    if not orders_missing:
        return

    msg = render_to_string("core/cron_messages_email_template.html", {'orders_missing': orders_missing,
                                                                      'orders_missing_count': len(orders_missing),
                                                                      'current_date': datetime.now()})
    # send email notifications
    cron_utils.send_emails(message=msg)


def get_skipped_orders():
    sac_orders = Order.objects.filter(number__istartswith="SO-1")\
        .filter(store=Store.objects.get(name="Sacramento"))\
        .order_by("-number")[:1000]

    sac_order_nums = sorted(map(int, [o.number[-5:] for o in sac_orders]))

    return __find_skipped_order_nums(sac_order_nums, "SO-")


def __find_skipped_order_nums(order_nums, prefix):
    res = []
    err_msg = "#{0}{1:05d}"

    if order_nums:
        first = order_nums[0]
        expected = first + 1
        for num in order_nums[1:]:
            if num != expected and expected not in ORDER_NUMS_TO_EXCLUDE:
                res.append(err_msg.format(prefix, expected))
                expected = expected + 1
                while expected < num:
                    res.append(err_msg.format(prefix, expected))
                    expected = expected + 1
                if expected <= num:
                    expected = expected + 1
            else:
                expected = num + 1

    return res
