import collections
from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.template.loader import render_to_string

import orders.cron_tasks.utils as cron_utils
from orders.models import Order


def run_unplaced_orders_cron():
    unplaced_orders = Order.objects.unplaced_orders()
    if not len(unplaced_orders):
        return

    msg = generate_unplaced_orders_email_contents(unplaced_orders)

    # send email notifications
    if msg:
        cron_utils.send_emails(
            to=settings.CRON_UNPLACED_ORDERS_NOTIFICATION_LIST,
            subject="FurniCloud: Order Alerts",
            message=msg
        )


def run_unplaced_orders_by_assoc_cron():
    user_model = get_user_model()
    associates = user_model.objects.filter(
        Q(is_active=True) & Q(groups__name__icontains="associates"))

    for associate in associates:
        if len(associate.email) and associate.email in settings.CRON_EMAIL_NOTIFICATION_LIST:

            unplaced_orders = Order.objects.unplaced_orders().filter(commissions__associate__exact=associate)
            if not len(unplaced_orders):
                continue

            msg = generate_unplaced_orders_email_contents(unplaced_orders)

            if msg:
                assoc_email = associate.email
                cron_utils.send_emails(
                    to=[assoc_email],
                    subject="FurniCloud: Your Order Alerts",
                    message=msg
                )


def generate_unplaced_orders_email_contents(unplaced_orders):
    unplaced_orders_with_summary = []
    for order in unplaced_orders:
        vendor_summary = collections.Counter([i.get_vendor_display() + "/" + i.get_status_display() for i in order.ordered_items.all()])
        unplaced_orders_with_summary.append({
            'order': order,
            'item_summary': ["{vendor}: {count} item(s)".format(vendor=v, count=cnt)
                             for v, cnt in vendor_summary.most_common()]
        })
    msg = render_to_string("core/cron_messages_email_template.html",
                           {
                               'orders_unplaced': unplaced_orders_with_summary,
                               'orders_unplaced_count': len(unplaced_orders),
                               'current_date': datetime.now()
                           })
    return msg
