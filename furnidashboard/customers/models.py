from django.db import models
from django.urls import reverse
from taggit.managers import TaggableManager
import os


class Customer(models.Model):
    """
    A model class representing customer information
    """

    first_name = models.CharField(max_length=200, blank=False)
    last_name = models.CharField(max_length=200, blank=True)
    phone = models.CharField(max_length=30, blank=True, null=True, verbose_name="Home Phone #")
    email = models.EmailField(blank=True, null=True)
    shipping_address = models.TextField(blank=True, null=True, verbose_name="Full Shipping Address")
    billing_address = models.TextField(blank=True, null=True, verbose_name="Full Billing Address")

    company = models.CharField(max_length=100, blank=True, null=True, verbose_name="Company Name")
    mobile = models.CharField(max_length=30, blank=True, null=True, verbose_name="Mobile Phone #")

    # bill to address
    bill_addr_str = models.CharField(max_length=100, blank=True, null=True, verbose_name="Billing Address 1")
    bill_addr_str2 = models.CharField(max_length=100, blank=True, null=True, verbose_name="Billing Address 2")
    bill_addr_city = models.CharField(max_length=50, blank=True, null=True, verbose_name="Bill To City")
    bill_addr_state = models.CharField(max_length=30, blank=True, null=True, verbose_name="Bill To State")
    bill_addr_zip = models.CharField(max_length=10, blank=True, null=True, verbose_name="Bill To ZIP")

    # ship to address
    ship_addr_str = models.CharField(max_length=100, blank=True, null=True, verbose_name="Shipping Address 1")
    ship_addr_str2 = models.CharField(max_length=100, blank=True, null=True, verbose_name="Shipping Address 2")
    ship_addr_city = models.CharField(max_length=50, blank=True, null=True, verbose_name="Ship To City")
    ship_addr_state = models.CharField(max_length=30, blank=True, null=True, verbose_name="Ship To State")
    ship_addr_zip = models.CharField(max_length=10, blank=True, null=True, verbose_name="Ship To ZIP")

    notes = models.TextField(blank=True, null=True, verbose_name="Customer Notes")

    tags = TaggableManager(blank=True)

    # override model save method
    def save(self, *args, **kwargs):
        self.update_billing_address()
        self.update_shipping_address()
        super().save(*args, **kwargs)

    def update_billing_address(self):
        self.billing_address = format_full_address(self.bill_addr_str, self.bill_addr_str2, self.bill_addr_city,
                                                   self.bill_addr_state, self.bill_addr_zip)

    def update_shipping_address(self):
        self.shipping_address = format_full_address(self.ship_addr_str, self.ship_addr_str2, self.ship_addr_city,
                                                    self.ship_addr_state, self.ship_addr_zip)

    @property
    def full_name(self):
        return self.first_name + " " + self.last_name

    def get_absolute_url(self):
        return reverse('customer_detail', kwargs={"pk": self.pk})

    class Meta:
        db_table = "customers"
        permissions = (
            ("view_customers", "Can View Orders"),
        )

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


def format_full_address(street=None, street2=None, city=None, state=None, zip=None):
    full_address = ''
    if street and len(street):
        full_address = street
        if street2 and len(street2):
            full_address += " %s" % street2

    if city and len(city):
        if len(full_address):
            full_address += ", " + os.linesep
        full_address += city

    if state and len(state):
        if len(full_address):
            full_address += ", "
        full_address += state

    if zip and len(zip):
        if len(full_address):
            full_address += " "
        full_address += zip

    return full_address if len(full_address) > 0 else None
