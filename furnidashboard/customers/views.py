from django.views.generic import DetailView, UpdateView
from django.views.generic.edit import CreateView
from django_filters.rest_framework import DjangoFilterBackend
from django_tables2 import RequestConfig, SingleTableView
from rest_framework import views, status, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from core.mixins import LoginRequiredMixin
from orders.forms import CustomerForm, CustomerDetailReadOnlyForm
from orders.tables import OrderTable
from customers.models import Customer
from .serializers import CustomerSerializer, MergeRecordsSerializer
from .tables import CustomersTable


class CustomerUpdateView(LoginRequiredMixin, UpdateView):
    model = Customer
    context_object_name = "customer"
    template_name = "customers/customer_update.html"
    form_class = CustomerForm


class CustomerCreateView(LoginRequiredMixin, CreateView):
    model = Customer
    context_object_name = "customer"
    template_name = "customers/customer_update.html"
    form_class = CustomerForm


class CustomerDetailView(LoginRequiredMixin, DetailView):
    model = Customer
    context_object_name = "customer"
    template_name = "customers/customer_detail.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerDetailView, self).get_context_data(**kwargs)
        context['customer_details_form'] = CustomerDetailReadOnlyForm(instance=context['object'])

        customer_orders = context['object'].orders.all()
        customer_orders_table = OrderTable(customer_orders)
        RequestConfig(self.request).configure(customer_orders_table)
        context['customer_orders_table'] = customer_orders_table
        return context


class CustomerTableView(LoginRequiredMixin, SingleTableView):
    model = Customer
    table_class = CustomersTable
    template_name = "customers/customer_table.html"


class CustomerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows customers to be viewed or edited.
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = (IsAuthenticated,)
    lookup_field = 'pk'
    lookup_url_kwarg = 'pk'
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['first_name', 'last_name', 'email', 'phone', 'mobile', 'bill_addr_city', 'bill_addr_state',]
    

class MergeRecordsView(views.APIView):
    serializer_class = MergeRecordsSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            primary_record_id = serializer.validated_data['primary']
            duplicate_record_ids = serializer.validated_data['duplicates']

            # Implement your logic to merge records with primary_record_id and duplicate_record_ids

            # Example logic: Merging by updating the primary record and deleting the duplicates
            # Implement your specific merge logic here

            return Response({'detail': 'Records merged successfully'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
