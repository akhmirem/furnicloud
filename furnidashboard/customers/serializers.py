from rest_framework import serializers
from taggit.serializers import TagListSerializerField

from customers.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    tags = TagListSerializerField()

    class Meta:
        model = Customer
        fields = ('pk', 'first_name', 'last_name', 'email', 'phone', 'mobile', 'bill_addr_str', 'bill_addr_str2',
                  'bill_addr_city', 'bill_addr_state', 'bill_addr_zip', 'ship_addr_str', 'ship_addr_str2',
                  'ship_addr_city', 'ship_addr_state', 'ship_addr_zip', 'notes', 'tags')
        read_only_fields = ('pk', 'created_at', 'updated_at')

class MergeRecordsSerializer(serializers.Serializer):
    primary = serializers.IntegerField()
    duplicates = serializers.ListField(child=serializers.IntegerField())
