# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200, blank=True)),
                ('phone', models.CharField(max_length=30, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('shipping_address', models.TextField(null=True, blank=True)),
                ('billing_address', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'customers',
                'permissions': (('view_customers', 'Can View Orders'),),
            },
        ),
    ]
