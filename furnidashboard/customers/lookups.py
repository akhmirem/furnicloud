from ajax_select import LookupChannel, register
from customers.models import Customer
from django.db.models import Q


@register('customers')
class CustomerLookup(LookupChannel):

  model = Customer

  def get_query(self, q, request):
      """ return a query set.  you also have access to request.user if needed """
      return self.model.objects.filter(Q(first_name__istartswith=q) | Q(last_name__istartswith=q) | Q(email__icontains=q))\
               .order_by('last_name')[:50]

  def format_item_display(self, customer):
      return str(customer)

  def check_auth(self, request):
      return request.user.is_active
