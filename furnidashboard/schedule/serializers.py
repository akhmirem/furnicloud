from associates.models import Associate
from rest_framework import serializers
from schedule.models import ScheduleEntry


class AssociateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Associate
        fields = 'first_name'


class ScheduleSerializer(serializers.ModelSerializer):
    start = serializers.DateTimeField(format='%m/%d/%Y %H:%M:%S')

    class Meta:
        model = ScheduleEntry
        fields = ('pk', 'associate', 'start', 'end', 'event_type', 'store', 'location', 'description')
