from datetime import datetime
import django_filters as filters
from core.utils import get_current_month_date_range
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit
from django import forms
from django.forms.fields import SplitDateTimeField
from django_filters.fields import DateTimeRangeField
from schedule.models import ScheduleEntry
from store_traffic.filters import FurnDateRangeFilter
from store_traffic.models import StoreTrafficEntry


class ScheduleEntryFilter(filters.FilterSet):
    start_range = FurnDateRangeFilter(
        name='start',
    )

    def __init__(self, *args, **kwargs):
        super(ScheduleEntryFilter, self).__init__(*args, **kwargs)

        # default_from_dt = datetime(2016, 8, 31)
        # default_from_dt = datetime.now()
        # default_to_dt = datetime.now()
        default_from_dt, default_to_dt = get_current_month_date_range()
        self.filters['start_range'].field.initial = (default_from_dt, default_to_dt)

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.include_media = True
        self.helper.layout = Layout(
            Div(
                'start_range',
                'associate',
                'event_type',
                Submit('submit', 'Filter', css_class='btn-default'),
                css_class='well'
            )
        )

    class Meta:
        model = ScheduleEntry
        fields = ['associate', 'start', 'event_type']
