from django.utils import timezone
from django.db.models import Count, Sum
from rest_framework import viewsets, filters, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from schedule.filters import ScheduleEntryFilter
from schedule.models import ScheduleEntry
from schedule.serializers import ScheduleSerializer
from store_traffic.filters import StoreTrafficEntryFilter
from store_traffic.models import StoreTrafficEntry
from store_traffic.serializers import StoreTrafficSerializer
from datetime import datetime, timedelta


class ScheduleViewSet(viewsets.ModelViewSet):
    """
    API endpoint for Schedule Entries
    """
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    queryset = ScheduleEntry.objects.all()
    serializer_class = ScheduleSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = ScheduleEntryFilter

