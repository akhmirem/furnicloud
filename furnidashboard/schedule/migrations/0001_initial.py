# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('stores', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ScheduleEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField(null=True, blank=True)),
                ('event_type', models.CharField(blank=True, max_length=25, null=True, choices=[(b'schedule_entry', b'Schedule Entry'), (b'alert_reminder', b'Reminder/Alert/Notification'), (b'holiday', b'Holiday'), (b'vacation', b'Vacation'), (b'other', b'Other')])),
                ('other_location', models.CharField(default=None, max_length=125, null=True, blank=True)),
                ('description', models.CharField(max_length=250, null=True, blank=True)),
                ('associate', models.ForeignKey(related_name='schedule_entries', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)),
                ('store', models.ForeignKey(related_name='store_schedule', default=None, blank=True, to='stores.Store', null=True, on_delete=models.SET_NULL)),
            ],
            options={
                'db_table': 'schedule',
            },
        ),
    ]
