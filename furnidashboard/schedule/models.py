from associates.models import Associate
from django.conf import settings
from django.db import models
from stores.models import Store

EVENT_TYPES = (
    ('schedule_entry', 'Schedule Entry'),
    ('alert_reminder', 'Reminder/Alert/Notification'),
    ('holiday', 'Holiday'),
    ('vacation', 'Vacation'),
    ('other', 'Other'),
)


class ScheduleEntry(models.Model):
    associate = models.ForeignKey(settings.AUTH_USER_MODEL, default=None, null=True, blank=True,
                                  related_name='schedule_entries', on_delete=models.CASCADE)
    start = models.DateTimeField(null=False, blank=False)
    end = models.DateTimeField(null=True, blank=True)
    event_type = models.CharField(blank=True, null=True, max_length=25, choices=EVENT_TYPES)
    store = models.ForeignKey(Store, default=None, null=True, blank=True,
                              related_name='store_schedule', on_delete=models.SET_NULL)
    other_location = models.CharField(blank=True, null=True, max_length=125, default=None)
    description = models.CharField(blank=True, null=True, max_length=250)

    class Meta:
        db_table = "schedule"
